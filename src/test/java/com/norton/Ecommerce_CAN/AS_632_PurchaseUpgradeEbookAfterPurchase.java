package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
@Listeners({ TestListener.class })
public class AS_632_PurchaseUpgradeEbookAfterPurchase extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID, totalPrice, dlpTitleName,itemTotal,invoiceLabel,accountName,accountEmail,itemcurrency,invoiceNumber,ordercompText,itemPrice,itemCurr;
	Canadiandistributor cand;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 632- TC_EC-84 LOGIN: Purchase Upgrade to an Ebook after initial Purchase")
	@Story("AS 632- TC_EC-84 LOGIN: Purchase Upgrade to an Ebook after initial Purchase")
	@Test
	public void purchaseUpgradeEbook() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		RP.clickpurchaseOptionButton();
		dlpTitleName = RP.selectOtherCheckbox();
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "College Francois-Xavier-Garneau, Quebec");
		 RP.clickContinueButton();
		totalPrice = RP.getTotalPrice();
		Assert.assertNotNull(RP.isTotalPrice());
		Assert.assertTrue(RP.issecureButton());
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		accountName = cand.getNortonAccountDetails().get(0);
		LogUtil.log(accountName);
		accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getorderSummaryDetails());
		itemTotal = cand.itemTotal().get(0);
		itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		// Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor,
		// Login Canada.",
		// cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals("Enter your email or mobile number to get started.", cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n" + "Please save this invoice for your records.");
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		//RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		dlpTitleName=RP.getISBNName();
		RP.clickCloseButton();
		ncia.clickDLPTile("Ebook");
		ncia.clickeBookChapterTitle(3);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		WebElement iframe = driver.findElement(By.xpath("//iframe[@id='section_iframe']"));
		driver.switchTo().frame(iframe);
		Assert.assertTrue(RP.isUpGradeFullAccessbutton());
		RP.clickRegisterCodePurchaseAccessButton();
		driver.switchTo().defaultContent();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC",
				"College Francois-Xavier-Garneau, Quebec");
		RP.Continue_Button.click();
		RP.selectCheckbox();
		totalPrice = RP.getTotalPrice();
		Assert.assertNotNull(RP.isTotalPrice());
		Assert.assertTrue(RP.issecureButton());
		
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		accountName = cand.getNortonAccountDetails().get(0);
		LogUtil.log(accountName);
		accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getorderSummaryDetails());
		itemTotal = cand.itemTotal().get(0);
		itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		Thread.sleep(5000);
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		itemPrice = cand.getItemPrice();
		itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n"
						+ "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		RP.clickCloseButton();
		//Click the DLP Tile Name 
		ncia.clickDLPTile("Ebook");
		ncia.clickeBookChapterTitle(3);
		ReusableMethods.switchToNewWindow(3, driver);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		WebElement iframe1 = driver.findElement(By.xpath("//iframe[@id='section_iframe']"));
		driver.switchTo().frame(iframe1);
		Assert.assertFalse(RP.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1,driver);
		ncia.UserSignOut();
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
