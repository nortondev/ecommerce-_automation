package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.CustomAssertion;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ TestListener.class })
public class AS_604_SupportStaffPurchasedEntitlementsPCAT extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;
	PCATPage pcat;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;
	CustomAssertion ca;
	String parentWindow;
	Object obj;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 604 - TC_01- EC-30:-Verify that, As a Support Staff, user is able to see the entitlements purchased by the Canadian Users from W.W. Norton on a digital landing page in NERT.")
	@Story("AS 604 - TC_01- EC-30:-Verify that, As a Support Staff, user is able to see the entitlements purchased by the Canadian Users from W.W. Norton on a digital landing page in NERT.")
	@Test
	public void canEntPurchaseSupportStaff() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		ca = new CustomAssertion();
		RP.clickpurchaseOptionButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();
		// Compare two List

		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "Bishop's College School, Sherbrooke");
		RP.clickContinueButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		System.out.println(dlppriceList);
		// Assert.assertTrue(dlppriceList.containsAll(pcatcanPrice));
		// pcatcanPrice
		totalPrice = RP.getTotalPrice();
		Assert.assertNotNull(RP.isTotalPrice());
		Assert.assertTrue(RP.issecureButton());

		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountName = cand.getNortonAccountDetails().get(0);
		LogUtil.log(accountName);
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotal = cand.itemTotal().get(0);
		String itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		// Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor,
		// Login Canada.",
		// cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals("Enter your email or mobile number to get started.", cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		String ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n" + "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		LogUtil.log(totalPrice);
		cand.clickLink("Help Desk");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.helpPage(), "W. W. Norton Help");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickLink("Return Policy");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.returnPolicy(), "Returns");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		// RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		RP.clickCloseButton();
		ncia.UserSignOut();
		List<String> tabs = ReusableMethods.switchToNewTAB(driver);
		parentWindow = tabs.get(0);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.clickgearMenu();
		pcat.clickNCIAEntMenu();
		Thread.sleep(3000);
		pcat.setUserName(emailID.trim());
		pcat.uncheck();
		pcat.clickGo();
		pcat.clickUsernamelink();
		String role = pcat.getuserRole();
		String studentrole = ReusableMethods.getStringparenthesis(role);
		Assert.assertEquals("Student", studentrole);
		// Verify that below fields should be displayed under 'Entitlement' Sections:
		// Product Code/Description,ISBN..etc
		Assert.assertNotNull(pcat.getProdCodeDesc());
		Assert.assertNotNull(pcat.getISBN());
		Assert.assertEquals("purchase", pcat.getpurchasePriceData().get(0));
		Assert.assertEquals(totalPrice, pcat.getpurchasePriceData().get(1).trim() + "CAD");
		Assert.assertNotNull(pcat.getDateValue());
		// Click details lnk
		pcat.clickDetailslink();
		Thread.sleep(2000);
		String getjsonData = pcat.getEntlDetails();
		obj = ReusableMethods.getPCATJSONData(getjsonData, "entitlement_method", null);
		Assert.assertEquals("purchase", obj);
		obj = ReusableMethods.getPCATJSONData(getjsonData, "info", "loginOrderId");
		Assert.assertEquals(invoiceNumber, obj);
		obj = ReusableMethods.getPCATJSONData(getjsonData, "info", "price");
		Assert.assertEquals(itemPrice.replace("$", " ").trim(), obj);
		obj = ReusableMethods.getPCATJSONData(getjsonData, "info", "currency");
		Assert.assertEquals(itemCurr, obj);
		obj = ReusableMethods.getPCATJSONData(getjsonData, "product_code", null);
		Assert.assertEquals("chem6", obj);
		ReusableMethods.scrollToBottom(driver);
		pcat.clickEntitlementOKbutton();
		Thread.sleep(2000);
		pcat.clickOKbutton();
		ReusableMethods.scrollByY(driver, -350);
		pcat.SignOut_Pcat();
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
