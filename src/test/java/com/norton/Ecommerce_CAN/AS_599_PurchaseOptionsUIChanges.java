package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class AS_599_PurchaseOptionsUIChanges extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID;
	List<String> dlppriceList,pcatPrice,pcatDesc,pcatcanPrice,dlpDesc,schoolName,countryName;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("AS 599 - TC 27-29-Verify Purchase Options UI for Canadian fulfillment process")
	@Story("AS 599 - TC 27-29-Verify Purchase Options UI for Canadian fulfillment process")
	@Test
	public void purchaseOptionCandaian() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP =new RegisterPurchasePage();
		//Verify when user accesses the DLP and selects 'Purchase Options', Then user sees (See Attachment Purchase_Options): The list of options available for purchase along with the access duration
		RP.clickpurchaseOptionButton();
		List<String> tabs =ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		//String childWindow = tabs.get(1);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		//PCAT Entitlement/Purchase Configuration for Standard Product
		pcatPrice =pcat.capturePrice();
		pcatDesc =pcat.captureisbndetails();
		pcatcanPrice = pcat.captureCADPrice();
		System.out.println(pcatcanPrice);
		pcat.logOutPCAT();
		driver.close();
		driver.switchTo().window(parentWindow);
		dlppriceList =RP.capturePurchaseoptionsPrice();		
		dlpDesc =RP.capturePurchaseoptionsDesc();
		//Compare two List 
		
	    Assert.assertTrue(pcatPrice.containsAll(dlppriceList));
	    Assert.assertTrue(pcatDesc.containsAll(dlpDesc));
	    //Verify when user chooses an purchase option/ options and selects ‘Purchase or Register for Access', the following are displayed - 'Register, Purchase, or Sign Up for Trial Access’ overlay.
	    RP.selectCheckbox();
	    RP.clickRegisterAccessButton();
	    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
	    wait.until(ExpectedConditions.textToBePresentInElement(RP.registrationcodeText, "I have a registration code:"));
	    boolean isLoginDialog = driver.findElements(By.id("login_dialog")).size() > 0;
	    if(isLoginDialog==true){
	    String regCodeText=	RP.registrationcodeText.getAttribute("innerText");
	    Assert.assertEquals("I have a registration code:", regCodeText );
	    String purchaseAccessText=RP.purchaseaccessText.getAttribute("innerText");
	    Assert.assertEquals("I want to purchase access\n(All titles available in the US and its territories. Most titles available in Canada.)", purchaseAccessText);
	    String trailAccessText=RP.trialaccessText.getAttribute("innerText");
	    Assert.assertEquals("I want to sign up for 21 days of trial access", trailAccessText);
	    }
	    
	   emailID = RP.createUser();
	   RP.clickShowPurchaseButton();
	   Thread.sleep(5000);
	   RP.confirm_Registration(emailID.toLowerCase());
	   Thread.sleep(3000);
	   RP.selectCheckboxTermofUse();
	   Assert.assertEquals("a College/University student", RP.schoolDropdownDefault());
	   Assert.assertEquals("Select Country", RP.countrySelectedDefault());
	   schoolName =  new ArrayList<String>();
	   schoolName.add("a College/University student");
	   schoolName.add("a High School student");
	   schoolName.add("not a student");
	   Assert.assertTrue(RP.schoolDropdownlist().containsAll(schoolName));
	   countryName =  new ArrayList<String>();
	   countryName.add("Select Country");
	   countryName.add("the United States");
	   countryName.add("Canada");
	   Assert.assertTrue(RP.countryDropdownlist().containsAll(countryName));
	   RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
	   RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB", "Alberta College of Art & Design, Calgary");
	   Thread.sleep(3000);
	   RP.selectCheckboxTermofUse();
	   Thread.sleep(2000);
	   RP.selectCheckboxTermofUse();
	   RP.selectCountryDropDown("HIGH_SCHOOL", "USA", "USA_AZ", "Air War College, Thatcher");
	   RP.selectCountryDropDown("HIGH_SCHOOL", "CN", "CN_MB", "Canadian Mennonite University, Winnipeg");
	   Assert.assertFalse(RP.selectNotaStudent("NEITHER", "USA"));
	   Assert.assertFalse(RP.selectNotaStudent("NEITHER", "CN"));
	   driver.manage().deleteAllCookies();
	   RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "College Francois-Xavier-Garneau, Quebec");
	   RP.clickContinueButton();
	   //Verify Canadian CAD info
	   dlppriceList =RP.capturePurchaseoptionsPrice();
	   System.out.println(dlppriceList);
	   Assert.assertTrue(dlppriceList.containsAll(pcatcanPrice));
	 	   //pcatcanPrice
	   Assert.assertNotNull(RP.isTotalPrice());
	   Assert.assertTrue(RP.issecureButton());
	}
	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
	
}
