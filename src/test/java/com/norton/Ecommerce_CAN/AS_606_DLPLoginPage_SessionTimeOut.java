package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.time.Duration;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_606_DLPLoginPage_SessionTimeOut extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID;
	Canadiandistributor cand;
	List<String> dlppriceList,pcatPrice,pcatDesc,pcatcanPrice,dlpDesc,schoolName,countryName,tabs;
	String totalPrice;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("AS 606 - TC_02-EC-62:- Verify that after clicking on the Exit and Return to Norton button after 120 min, user is navigated back to DLP screen with Get Started popup.")
	@Story("AS 606 - TC_02-EC-62:- Verify that after clicking on the Exit and Return to Norton button after 120 min, user is navigated back to DLP screen with Get Started popup.")
	@Test
	public void sessionTimeOutDLpLoginPage() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP =new RegisterPurchasePage();
		cand = new Canadiandistributor();
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createTrialUser();
		Thread.sleep(8000);
		tabs =ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		ncia.navigateNCIADLP("");
	    String uName = ncia.verify_Username();
	    Assert.assertEquals(uName.toLowerCase(), emailID.toLowerCase());
	    RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB",
				"Alberta College of Art & Design, Calgary");
		RP.clickContinueButton();
		// RP.selectCheckbox();
		totalPrice = RP.getTotalPrice();
		Assert.assertTrue(RP.issecureButton());
		Thread.sleep(20000);
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(6000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		cand.clickPayPal();
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(8000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		String ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n"
						+ "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		//Navigate to Back to Parent Window and Clear the Cookies
		driver.switchTo().window(parentWindow);
		driver.manage().deleteAllCookies();
		Thread.sleep(8000);
		ReusableMethods.switchToNewWindow(2, driver);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		Assert.assertTrue(ncia.isSigninPopUp(), "SignIn Page is displayed");
		ReusableMethods.closeCurrentTab(1, driver);
		driver.navigate().refresh();
		Assert.assertTrue(ncia.loginButton.isDisplayed(), "SignIn Page is displayed");
		
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
