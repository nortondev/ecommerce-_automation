package com.norton.Ecommerce_CAN;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import junit.framework.Assert;
@Listeners({ TestListener.class })
public class AS_642_ErrorEntlInvalidinLoginCanada extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID, totalPrice, dlpTitleName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("bionow2");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 642- TC_01-EC-67 - Verify that I'm not able to purchase Entitlement which do not have sales territory right so sell in candada ")
	@Story("AS 642- TC_01-EC-67 - Verify that I'm not able to purchase Entitlement which do not have sales territory right so sell in candada ")
	@Test
	public void errorMessageInvalidEntitlement() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		RP.clickpurchaseOptionButton();
		dlpTitleName = RP.selectInvalidISBNCheckbox("test 67");
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "College Francois-Xavier-Garneau, Quebec");
		RP.clickContinueButton();
		RP.clickSecureCheckOutButton();
		String Errortext = RP.getErrorentlCanada();
		Assert.assertEquals(Errortext, "This title is not available for purchase in Canada.");
		RP.clickBackButton();
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
