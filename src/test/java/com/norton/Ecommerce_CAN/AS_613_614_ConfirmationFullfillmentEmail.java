package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.MailDrop;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.CustomAssertion;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_613_614_ConfirmationFullfillmentEmail extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID;
	Canadiandistributor cand;
	MailDrop maildrop;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc,
			schoolName, countryName, tabs;
	String totalPrice, parentWindow;
	WebElement getOrderConfEmail = null;
	WebElement getOrderFullEmail = null;
	CustomAssertion cas;
	
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 613_614 - CAN : TC_01-EC-31:- Verify Purchase Notification Emails: Order Confirmation Email,TC_01-EC-70:- Verify Purchase Notification Emails: Order Fulfillment Email")
	@Story("AS 613_614 - CAN : TC_01-EC-31:- Verify Purchase Notification Emails: Order Confirmation Email,TC_01-EC-70:- Verify Purchase Notification Emails: Order Fulfillment Email")
	@Test
	public void confirmationFullfillmentEmail() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		maildrop = new MailDrop();
		cand = new Canadiandistributor();
		cas = new CustomAssertion();
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createUser();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB",
				"Alberta College of Art & Design, Calgary");
		RP.clickContinueButton();
		RP.selectCheckbox();
		totalPrice = RP.getTotalPrice();
		Assert.assertTrue(RP.issecureButton());
		Thread.sleep(20000);
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(6000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		ReusableMethods.scrollToBottom(driver);
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Map<String, String> map = cand.getBillingInfo();
		String addressvalue  =map.get("Address");
		LogUtil.log(addressvalue);
		String namevalue = map.get("Name");
		LogUtil.log(namevalue);
		String emailvalue = map.get("Email");
		LogUtil.log(emailvalue);
		Assert.assertNotNull(addressvalue);
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String gstPrice = cand.getGSTItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(8000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		/*
		 * driver.switchTo().window(parentWindow);
		 * ReusableMethods.switchToNewWindow(2, driver);
		 */
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		//ReusableMethods.closeCurrentTab(1, driver);
		tabs = ReusableMethods.switchToNewTAB(driver);
		parentWindow = tabs.get(0);
		maildrop.navigateMailDrop();
		String[] userFLName = emailID.split("@");
		maildrop.clickViewInboxbutton(userFLName[0]);
		//Check For Order Confirmation Email
		Thread.sleep(4000);
		String emailSubject = "Login Canada Order Confirmation";
		int counter = 0;
		ReusableMethods.checkPageIsReady(driver);
		maildrop.clickReloadButton();
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
		while (getOrderConfEmail==null && counter != 15) {
			try {
				getOrderConfEmail =wait.until(ExpectedConditions.presenceOfElementLocated(
								By.xpath("//div[contains(@class,'fade-container')]//div[contains(@class,'messagelist-subject')]/span[contains(.,'Login Canada Order Confirmation')]")));
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				System.out.println("Not loaded yet, continuing ");
				maildrop.clickReloadButton();
				Thread.sleep(7000);
				counter++;
			}
			
		}
		getOrderConfEmail = driver
				.findElement(By
						.xpath("//div[contains(@class,'fade-container')]//div[contains(@class,'messagelist-subject')]/span[contains(.,'"
								+ emailSubject + "')]"));

		String getcourseInvifText = getOrderConfEmail.getText().trim();
		
		cas.assertContains(getcourseInvifText,emailSubject);
		getOrderConfEmail.click();

		// Email Content Verification
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='messagedata-iframe']")));
		
		String mailDropEmail = maildrop.getOrderConfirmationContents("Email");
		String[] str = mailDropEmail.split(":");
		Assert.assertEquals(accountEmail.toLowerCase(), str[1].toString().trim());
		// Account Number Compare
		String mailDropAccount = maildrop.getOrderConfirmationContents("Account");
		String[] astr = mailDropAccount.split(":");
		LogUtil.log(astr[1]);
		String mailDropInvoice = maildrop.getOrderConfirmationContents("Order#");
		String[] orderstr = mailDropInvoice.split(":");
		Assert.assertEquals(invoiceNumber.trim(), orderstr[1].toString().trim());
		// Sub Total
		String mailDropSubTotal = maildrop
				.getOrderConfirmationContents("Sub Total");
		String[] subTotlstr = mailDropSubTotal.split(":");
		Assert.assertEquals(itemPrice+" " +itemCurr, subTotlstr[1].trim().toString());
		String mailDropgstPrice = maildrop
				.getOrderConfirmationContents("Tax Charges");
		String[] gstPricestr = mailDropgstPrice.split(":");
		Assert.assertEquals(gstPrice+" "+itemCurr, gstPricestr[1].trim().toString());
		String address = maildrop.getBillingAddress();
		String emailAddress = address.toString().replaceAll(",", "").replaceAll("\\[", "").replaceAll("\\]","");
		String emailAddressonCanada= namevalue.toString().replaceAll(",", "")+" "+addressvalue.toString().replaceAll(",", "");
		Assert.assertEquals(emailAddress, emailAddressonCanada.replaceAll("\\n", " "));
		driver.switchTo().defaultContent();
		maildrop.clickClose();
		
		String emailSubFulFillMent = "Login Canada Order Fulfillment";
		while (getOrderConfEmail==null && counter != 15) {
			try {
				getOrderConfEmail =wait.until(ExpectedConditions.presenceOfElementLocated(
								By.xpath("//div[contains(@class,'fade-container')]//div[contains(@class,'messagelist-subject')]/span[contains(.,'Login Canada Order Fulfillment')]")));
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				System.out.println("Not loaded yet, continuing ");
				maildrop.clickReloadButton();
				Thread.sleep(7000);
				counter++;
			}
			
		}
		
		getOrderFullEmail = driver
				.findElement(By
						.xpath("//div[contains(@class,'fade-container')]//div[contains(@class,'messagelist-subject')]/span[contains(.,'"
								+ emailSubFulFillMent + "')]"));

		String getcoursefillmentText = getOrderFullEmail.getText().trim();
		cas.assertContains(getcoursefillmentText,emailSubFulFillMent);
		getOrderFullEmail.click();
		
		// Email Content Verification
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='messagedata-iframe']")));
				
				String mailDropEmailent = maildrop.getOrderConfirmationContents("EMAIL");
				String[] strent= mailDropEmailent.split(":");
				Assert.assertEquals(accountEmail.toLowerCase(), strent[1].toString().trim());
				// Account Number Compare
				String mailDropAccountent = maildrop.getOrderConfirmationContents("ACCOUNT");
				String[] astrent = mailDropAccountent.split(":");
				LogUtil.log(astrent);
				String mailDropOrder = maildrop.getOrderNumber("ORDER #");
				String[] orderid = mailDropOrder.split(":");				
				Assert.assertEquals(orderid[1].trim(), invoiceNumber.trim());
				driver.switchTo().defaultContent();
				maildrop.clickClose();
				driver.close();
				ReusableMethods.switchToNewWindow(1, driver);
				ncia.UserSignOut();
	}
	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
