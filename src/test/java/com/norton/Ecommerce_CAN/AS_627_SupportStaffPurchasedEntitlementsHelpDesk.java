package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.HelpDeskTool;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.CustomAssertion;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_627_SupportStaffPurchasedEntitlementsHelpDesk extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;
	PCATPage pcat;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc,
	schoolName, countryName;
	CustomAssertion ca;
	HelpDeskTool hdtool;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("AS 627 - TC_01-EC-80: Verify that As a Support Staff, user is able to see the entitlements purchased by the Canadian Users from W.W. Norton on a digital landing page in Helpdesk.")
	@Story("AS 627 - TC_01-EC-80: Verify that As a Support Staff, user is able to see the entitlements purchased by the Canadian Users from W.W. Norton on a digital landing page in Helpdesk.")
	@Test
	public void canEntPurchaseSupportStaffHelpDesk() throws Exception{
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		ca= new CustomAssertion();
		hdtool = new HelpDeskTool();
		RP.clickpurchaseOptionButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();
		// Compare two List
		
	    RP.selectCheckbox();
	    RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC",
				"Ecole Polytechnique of Montreal, Montreal");
		 RP.clickContinueButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		System.out.println(dlppriceList);
		//Assert.assertTrue(dlppriceList.containsAll(pcatcanPrice));
		// pcatcanPrice
		totalPrice = RP.getTotalPrice();
		Assert.assertNotNull(RP.isTotalPrice());
		Assert.assertTrue(RP.issecureButton());
		
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountName = cand.getNortonAccountDetails().get(0);
		LogUtil.log(accountName);
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotal = cand.itemTotal().get(0);
		String itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		// Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
		// cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		String ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n"
						+ "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		System.out.println(totalPrice);
		cand.clickLink("Help Desk");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.helpPage(), "W. W. Norton Help");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickLink("Return Policy");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.returnPolicy(), "Returns");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		//RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		RP.clickCloseButton();
		ncia.UserSignOut();
		List<String> tabs =ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		hdtool.navigateHDTool();
		hdtool.hdtoolLogin();
		hdtool.selectEmailOption();
		hdtool.searchEmail(accountEmail);
		hdtool.clickEmaillink();
		String userRole =hdtool.getUserRole();
		Assert.assertEquals("Student-1", userRole);
		Assert.assertEquals(emailID.toLowerCase(), hdtool.getUserEmailID());
		Assert.assertNotNull(hdtool.getProductCodeDesc());
		Assert.assertNotNull(hdtool.getISBNNumber());
		Assert.assertEquals("purchase", hdtool.getpurchaseText().trim());
		List<String> orderHistoryhdata = hdtool.getOrderHistory();
		String ohOrder =orderHistoryhdata.get(0).trim();
		Assert.assertEquals(invoiceNumber, ohOrder);
		String ohDate =orderHistoryhdata.get(1);
		Assert.assertNotNull(ohDate);
		String ohShipmentMethod =orderHistoryhdata.get(2).trim();
		Assert.assertEquals("Electronic Delivery", ohShipmentMethod);
		String ohProductTotal =orderHistoryhdata.get(3).replace(" ", "");
		Assert.assertEquals(totalPrice, ohProductTotal);
		String ohOrderType =orderHistoryhdata.get(7).trim();
		Assert.assertEquals("Canada Order Processing", ohOrderType);
		hdtool.signOutHDtool();
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	
	  @AfterTest 
	  public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest();
	  
	  }
	 
}
