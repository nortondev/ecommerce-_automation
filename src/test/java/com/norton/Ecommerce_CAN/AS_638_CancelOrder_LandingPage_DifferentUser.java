package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_638_CancelOrder_LandingPage_DifferentUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice, trialUserEmail;
	Canadiandistributor cand;
	List<String> tabs;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 638 -TC_03-EC-39 - Verify when I'm routed back to DLP sign in page by clicking on either Back to W.W Norton button or Cancel Order button  on LOGIN side after DLP session timed out and logged in  with different  user then the DLP landing page is displayed.")
	@Story("AS 638 -TC_03-EC-39 - Verify when I'm routed back to DLP sign in page by clicking on either Back to W.W Norton button or Cancel Order button  on LOGIN side after DLP session timed out and logged in  with different  user then the DLP landing page is displayed.")
	@Test
	public void cancelOrderButtonDLPSignInPageDiffUser() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		// Create Trial User
		trialUserEmail = RP.createTrialAccessNewUser();
		// Sign Out Trial User
		ReusableMethods.checkPageIsReady(driver);
		ncia.UserSignOut();
		Thread.sleep(5000);
		// Create a purchase User
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		/*
		 * RP.clickpurchaseOptionButton(); RP.selectCheckbox();
		 * RP.clickRegisterAccessButton();
		 */
		emailID = RP.createUser();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		 RP.clickContinueButton();
		Thread.sleep(3000);
		RP.selectOtherCheckbox();
		RP.creditcardButton();
		RP.FirstName_LastName.sendKeys("John" + " " + "Simpson");
		RP.CreditCardNumber.sendKeys("1234-5678-9012-1150");
		RP.securityCode.sendKeys("344");
		RP.expdate.sendKeys("12/2024");
		RP.billingInfo();
		RP.reviewOrderbutton();
		Thread.sleep(4000);
		RP.completePurchase();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(RP.Get_Started));
		RP.Get_Started.click();
		// Open a New Tab
		Thread.sleep(8000);
		tabs = ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		ncia.navigateNCIADLP("");
		Thread.sleep(8000);
		String uName = ncia.verify_Username();
		Assert.assertEquals(uName.toLowerCase(), emailID.toLowerCase());
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB", "Alberta College of Art & Design, Calgary");
		RP.Continue_Button.click();
		Thread.sleep(3000);
		RP.clickSecureCheckOutButton();
		Thread.sleep(5000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		driver.switchTo().window(parentWindow);
		driver.manage().deleteAllCookies();
		Thread.sleep(8000);
		ReusableMethods.switchToNewWindow(2, driver);
		cand.clickCancelOrder();
		Thread.sleep(10000);
		// ReusableMethods.checkPageIsReady(driver);

		Assert.assertTrue(ncia.isSigninPopUp(), "SignIn Page is displayed");
		Assert.assertTrue(ncia.loginButton.isDisplayed(), "SignIn Page is displayed");
		ncia.loginNCIA(trialUserEmail, "Tabletop1");
		Thread.sleep(5000);
		Assert.assertTrue(ncia.isDLPTilesPage(), "DLP Page is displayed");
		ncia.UserSignOut();
		Thread.sleep(5000);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
