package com.norton.Ecommerce_CAN;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ TestListener.class })
public class AS_641_distributordatabaseErrorCanada extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID, totalPrice, dlpTitleName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("bionow2");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 641- TC_01-EC-98:- Verify that if the entitlements are not present into Database then,user will get the error message.")
	@Story("AS 641- TC_01-EC-98:- Verify that if the entitlements are not present into Database then,user will get the error message.")
	@Test
	public void errordistributordatabaseCanada() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		RP.clickpurchaseOptionButton();
		dlpTitleName = RP.selectInvalidISBNCheckbox("Smartwork");
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "College Francois-Xavier-Garneau, Quebec");
		RP.clickContinueButton();
		RP.clickSecureCheckOutButton();
        Thread.sleep(10000);
		String Errortext = RP.getdistrDatabaseError().replaceAll("\\n", "");
		System.out.println(Errortext);
		Assert.assertEquals(Errortext,
				"The selected items were not found in distributor's database.You may request help from our support team.");
		RP.clicksupportTeamLink();
		ReusableMethods.switchToNewWindow(2, driver);
		WebElement headerText = driver
				.findElement(By.xpath("//header[@class='bannerContainer']//h1[contains(.,'Support Ticket Request')]"));
		String headerTextva = headerText.getText();
		Assert.assertEquals(headerTextva, "Support Ticket Request");
		ReusableMethods.closeCurrentWindow(2, driver);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
