package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_623_PurchaseOtherthanEbook extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID, totalPrice,dlpTitleName;
	Canadiandistributor cand;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc,
			schoolName, countryName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 623- TC_01- EC-37: Verify that user is able to purchase entitlement other than Ebook")
	@Story("AS 623- TC_01- EC-37: Verify that user is able to purchase entitlement other than Ebook")
	@Test
	public void purchaseOtherThanEbook() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		// Verify when user accesses the DLP and selects 'Purchase Options',
		// Then user sees (See Attachment Purchase_Options): The list of options
		// available for purchase along with the access duration
		RP.clickpurchaseOptionButton();
		List<String> tabs = ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		// String childWindow = tabs.get(1);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		// PCAT Entitlement/Purchase Configuration for Standard Product
		pcatPrice = pcat.capturePrice();
		pcatDesc = pcat.captureisbndetails();
		pcatcanPrice = pcat.captureCADPrice();
		System.out.println(pcatcanPrice);
		pcat.logOutPCAT();
		driver.close();
		driver.switchTo().window(parentWindow);
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();
		// Compare two List
		Assert.assertTrue(pcatPrice.containsAll(dlppriceList));
		Assert.assertTrue(pcatDesc.containsAll(dlpDesc));
		//Select Check Box Other than Ebook
	    dlpTitleName =RP.selectOtherCheckbox();
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC",
				"College Francois-Xavier-Garneau, Quebec");
		RP.clickContinueButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		System.out.println(dlppriceList);
		Assert.assertTrue(dlppriceList.containsAll(pcatcanPrice));
		// pcatcanPrice
		totalPrice = RP.getTotalPrice();
		Assert.assertNotNull(RP.isTotalPrice());
		Assert.assertTrue(RP.issecureButton());
		
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountName = cand.getNortonAccountDetails().get(0);
		LogUtil.log(accountName);
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotal = cand.itemTotal().get(0);
		String itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		// Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
		// cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		String ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n"
						+ "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickLink("Help Desk");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.helpPage(), "W. W. Norton Help");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickLink("Return Policy");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.returnPolicy(), "Returns");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		//RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		dlpTitleName=RP.getISBNName();
		RP.clickCloseButton();
		//Click the DLP Tile Name 
		ncia.clickDLPTile(dlpTitleName);
		Assert.assertFalse(RP.isUpGradeFullAccessbutton());
		ncia.clickdigitalResourceBackButton();
		ncia.clickDLPTile("Ebook");
		ncia.clickeBookChapterTitle(3);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		WebElement iframe = driver.findElement(By.xpath("//iframe[@id='section_iframe']"));
		driver.switchTo().frame(iframe);
		Assert.assertTrue(RP.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1,driver);
		ncia.UserSignOut();
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}
}
