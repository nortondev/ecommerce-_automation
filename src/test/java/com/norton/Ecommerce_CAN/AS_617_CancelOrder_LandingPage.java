package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_617_CancelOrder_LandingPage extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 617 -TC_01-EC-39 - Verify that when I'm on the Login side and click on either Back to W.W Norton button or Cancel order button then redirected to the DLP Sign in page.")
	@Story("AS 617 -TC_01-EC-39 - Verify that when I'm on the Login side and click on either Back to W.W Norton button or Cancel order button then redirected to the DLP Sign in page.")
	@Test
	public void cancelOrderButtonDLPSignInPage() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB",
				"Alberta College of Art & Design, Calgary");
		RP.clickContinueButton();
		// RP.selectCheckbox();
		Assert.assertTrue(RP.issecureButton());
		Thread.sleep(5000);
		RP.clickSecureCheckOutButton();
		Thread.sleep(5000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		cand.clickPayPal();
		Thread.sleep(5000);
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		driver.manage().deleteAllCookies();
		cand.clickCancelOrder();		
		Thread.sleep(10000);
		//ReusableMethods.checkPageIsReady(driver);
		Assert.assertTrue(RP.isPurchaseOptionButton());
		Assert.assertEquals(ncia.verify_Username().toLowerCase(),
				emailID.toLowerCase());
	}

	
	  @AfterTest 
	  public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest(); }
	 
}
