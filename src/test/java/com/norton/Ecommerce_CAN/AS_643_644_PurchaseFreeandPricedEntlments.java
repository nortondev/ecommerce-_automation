package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
@Listeners({ TestListener.class })
public class AS_643_644_PurchaseFreeandPricedEntlments
extends PropertiesFile
{

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;
	
	ReadUIJsonFile readUserData = new ReadUIJsonFile();
	JsonObject obj = readUserData.readUIJson();
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("americanlit9pre1865");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 644 - TC_01- EC-25: Verify that user is able to purchase Free Items along with a Priced Entitlement.")
	@Story("AS 644 - TC_01- EC-25: Verify that user is able to purchase Free Items along with a Priced Entitlement.")
	@Test
	public void purchaseFreePricedEntlment() throws Exception {
		driver = getDriver();
		driver.manage().deleteAllCookies();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();		
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createUser();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB",
				"Alberta College of Art & Design, Calgary");
		RP.clickContinueButton();
		RP.selectCheckboxFREEOption();
		RP.clickOKButton();
		RP.selectallCheckbox();
		totalPrice = RP.getTotalPrice();
		RP.clickGetFreeItemsButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(6000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		ReusableMethods.scrollToBottom(driver);
		String freePriceorderSummary = cand.getFreeItemPrice();		
		Assert.assertEquals(freePriceorderSummary, "$0.00 CAD");
		List<String> itemsPrice = cand.geteachItemPrice();
		float totalitemsPrice = 0;
		String totalItems;
		int items = itemsPrice.size();
		for(int i=0; i<items; i++) {
			totalItems =itemsPrice.get(i);
			totalitemsPrice +=ReusableMethods.getDecimalDigits(totalItems);
		}
		Assert.assertEquals(totalPrice, "$"+totalitemsPrice+"CAD");
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());		
		String itemPrice = cand.getItemPrice();
		String gstPrice = cand.getGSTItemPrice();
		String itemCurr = cand.getItemCurrency();
		LogUtil.log(itemPrice +""+ gstPrice +"" +itemCurr );
		cand.clickSubmitOrder();
		Thread.sleep(8000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(6000);		
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		List<String> purchasedList =RP.getPurchasedName();
		LogUtil.log(purchasedList);
		totalPrice = RP.getUSATotalPrice();
		Assert.assertEquals(totalPrice, "$0.00 USD");
		RP.clickCloseButton();
		//List Contains 		
		List<String> nciaProductCode = ncia.getProductCodeNames();
		List<Integer> index  = nciaProductCode.stream().filter(dlp -> dlp.contains("Post")).map(dlp -> nciaProductCode.indexOf(dlp)).collect(Collectors.toList());
		String str = index.toString();
		int indexValue = Integer.parseInt(str.replaceAll("(^\\[|\\]$)",""));
		ncia.clickTile(indexValue);		
		ncia.clickeBookChapterTitle(3);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		WebElement iframe1 = driver.findElement(By.xpath("//iframe[@id='section_iframe']"));
		driver.switchTo().frame(iframe1);
		Assert.assertFalse(RP.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1,driver);
		ncia.UserSignOut();
		ReusableMethods.checkPageIsReady(driver);
		String postURL = obj.getAsJsonObject("AmercanLitPostURL").get("URL").getAsString();
		System.out.println(postURL);
		driver.get(postURL);
		ncia.loginNCIA(emailID, "Tabletop1");
		Thread.sleep(6000);		
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
	}
	
	  @AfterTest 
	  public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest(); }
	 
}
