package com.norton.Ecommerce_CAN;

import java.time.Duration;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ TestListener.class })
public class AS_600_PayPalIntegration  extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand; 
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}
	@Severity(SeverityLevel.NORMAL)
	@Description("AS 600 - TC 59-60-Verify Paypal Integration and flow in Login and Submit Order")
	@Story("AS 600 - TC 59-60-Verify Paypal Integration and flow in Login and Submit Order")
	@Test
	public void payPalIntegration() throws Exception {
		driver = getDriver();
		// driver.manage().deleteAllCookies();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createTrialUser();
		String[] email = emailID.split("[@.]");
		firstlastName = email[0].replace("_", " ");

		// logout the User and Login Again
		Thread.sleep(5000);
		ncia.UserSignOut();
		// firstlastName = RP.getFLName();
		ncia.clickSignIn();
		ncia.loginNCIA(emailID, "Tabletop1");
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB",
				"Alberta College of Art & Design, Calgary");
		RP.clickContinueButton();
		// RP.selectCheckbox();
		totalPrice = RP.getTotalPrice();
		Assert.assertTrue(RP.issecureButton());
		Thread.sleep(20000);
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		Assert.assertEquals(
				"You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String accountName = cand.getNortonAccountDetails().get(0);
		String accountEmail = cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertEquals(accountName, firstlastName);
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotal = cand.itemTotal().get(0);
		String itemcurrency = cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal + itemcurrency);
		cand.clickPayPal();
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada",
				cand.loginCanadatitle());
		// Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
		// cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals(
				"Enter your email or mobile number to get started.",
				cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail.toLowerCase(), emailID.toLowerCase());
		Assert.assertEquals(accountName, firstlastName);
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice = cand.getItemPrice();
		String itemCurr = cand.getItemCurrency();
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickSubmitOrder();
		Thread.sleep(10000);
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel = cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber = cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull(invoiceLabel + " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber);
		String ordercompText = cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText,
				"You will receive a confirmation email shortly." + "\n"
						+ "Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice + itemCurr);
		cand.clickLink("Help Desk");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.helpPage(), "W. W. Norton Help");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickLink("Return Policy");
		ReusableMethods.switchToNewWindow(2, driver);
		Assert.assertEquals(cand.returnPolicy(), "Returns");
		ReusableMethods.closeCurrentTab(1, driver);
		cand.clickExitBacktoNortonButton();
		Thread.sleep(10000);
		RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		RP.clickCloseButton();
		ncia.UserSignOut();
	}
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
