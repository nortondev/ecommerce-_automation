package com.norton.Ecommerce_CAN;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
@Listeners({ TestListener.class })
public class AS_637_purchaseOveralyBackToNorton extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, trialUserEmail;
	Canadiandistributor cand;
	List<String> tabs;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 637 -TC_01-EC-57: Verify Routing the User from the Login Shopping Cart back to DLP Purchase Options Overlay")
	@Story("AS 637 -TC_01-EC-57: Verify Routing the User from the Login Shopping Cart back to DLP Purchase Options Overlay")
	@Test
	public void purchaseOverlayWWnortonBackButton() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		// Create Trial User
		trialUserEmail = RP.createTrialAccessNewUser();
		// Sign Out Trial User
		ReusableMethods.checkPageIsReady(driver);
		tabs = ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		ncia.navigateNCIADLP("");
		Thread.sleep(8000);
		String uName = ncia.verify_Username();
		Assert.assertEquals(uName.toLowerCase(), trialUserEmail.toLowerCase());
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		Thread.sleep(8000);		
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(trialUserEmail.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_AB", "Alberta College of Art & Design, Calgary");
		 RP.clickContinueButton();
		// RP.selectCheckbox();
		Assert.assertTrue(RP.issecureButton());
		Thread.sleep(5000);
		RP.clickSecureCheckOutButton();
		Thread.sleep(5000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		driver.switchTo().window(parentWindow);
		//driver.manage().deleteCookieNamed(uName);
		Thread.sleep(8000);
		ReusableMethods.switchToNewWindow(2, driver);
		cand.clickNortonlink();
		Thread.sleep(10000);
		//ReusableMethods.checkPageIsReady(driver);
		//DLP Purchase Options Overlay is displayed 
		Assert.assertTrue(RP.isPurchaseOption());
		RP.clickCloseButton();
		Assert.assertEquals(ncia.verify_Username().toLowerCase(), trialUserEmail.toLowerCase());
	}

	
	  @AfterTest public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest(); }
	 

}
