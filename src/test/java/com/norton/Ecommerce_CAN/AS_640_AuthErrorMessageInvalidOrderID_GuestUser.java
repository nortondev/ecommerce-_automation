package com.norton.Ecommerce_CAN;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.Canadiandistributor;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
@Listeners({ TestListener.class })
public class AS_640_AuthErrorMessageInvalidOrderID_GuestUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;
	List<String> tabs;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 640 - TC_02-EC-63 - Verify as a PayPal  guest user , Error message should be displayed  on the Shopping cart page when the PayPal authorization gets failed")
	@Story("AS 640 - TC_02-EC-63 - Verify as a PayPal  guest user , Error message should be displayed  on the Shopping cart page when the PayPal authorization gets failed")
	@Test
	public void errorMessageInvalidOrderIDGuestUser() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		emailID = RP.createUser();
		RP.clickShowPurchaseButton();
		Thread.sleep(5000);
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC", "Bishop's College School, Sherbrooke");
		 RP.clickContinueButton();
		Thread.sleep(3000);
		RP.clickSecureCheckOutButton();
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(1));
		Thread.sleep(10000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.",
				cand.getCanadadisText());
		String getURL = driver.getCurrentUrl();
		getURL = getURL.substring(3, getURL.length() - 3);
		String newURL = getURL.concat("000");
		newURL = getURL.replace(getURL, newURL);
		/*
		 * JavascriptExecutor js = (JavascriptExecutor) driver;
		 * js.executeScript("window.open('');"); ReusableMethods.switchToNewWindow(2,
		 * driver);
		 */
		cand.appendURL("htt" + newURL);
		ReusableMethods.clickENTERkey(driver);
		cand.clickPayPal();
		Thread.sleep(5000);
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals("Enter your email or mobile number to get started.", cand.emailSubTag.getText());
		cand.clickCreateAccountGuestUser();
		//Assert.assertNotNull(cand.getCardinfo());
		cand.setBillingInfoGuestUser();
		Thread.sleep(5000);
		WebElement errorEle = driver.findElement(By.xpath("//div[@class='error_cont']/span[@class='error_text']"));

		Assert.assertEquals(
				"PayPal authorization has failed. Please check your payment details or try a different credit card.",
				errorEle.getText());
		//ReusableMethods.closeCurrentWindow(1, driver);
	}

	
	  @AfterTest public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest();
	  
	  }
	 
}
