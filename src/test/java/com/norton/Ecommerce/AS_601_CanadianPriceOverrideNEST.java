package com.norton.Ecommerce;

import java.math.BigDecimal;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;

public class AS_601_CanadianPriceOverrideNEST extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;
	PCATPage pcat;
	List<String> dlppriceList,pcatDesc,dlpDesc,schoolName,countryName,pcatOveridePrice;
	String pcatcanPrice;
	List<String> tabs;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 601 - TC_01-EC-74:- Verify Adding Canadian Price Override for DLP Field in NEST")
	@Story("AS 601 - TC_01-EC-74:- Verify Adding Canadian Price Override for DLP Field in NEST")
	@Test
	public void canadianPriceOveride() throws Exception {
		driver = getDriver();
		driver.manage().deleteAllCookies();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP =new RegisterPurchasePage();
		cand = new Canadiandistributor();
		//Verify when user accesses the DLP and selects 'Purchase Options', Then user sees (See Attachment Purchase_Options): The list of options available for purchase along with the access duration
		tabs =ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		//String childWindow = tabs.get(1);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		pcatcanPrice = pcat.originalCADPrice();
		System.out.println(pcatcanPrice);
		pcatOveridePrice =pcat.OveridePriceCAD("110.50");
		System.out.println(pcatOveridePrice);
		pcat.logOutPCAT();
		driver.close();
		driver.switchTo().window(parentWindow);
		
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createUser();
		firstlastName = RP.getFLName();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC",
				"Alberta College of Art & Design, Calgary");
		RP.Continue_Button.click();
		RP.selectCheckbox();
		totalPrice =RP.getTotalPrice();
		Assert.assertTrue(RP.issecureButton());
		RP.clickSecureCheckOutButton();
		Thread.sleep(5000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.", cand.getCanadadisText());
		String accountName= cand.getNortonAccountDetails().get(0);
		String accountEmail= cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail, emailID);
		Assert.assertEquals(accountName, firstlastName);
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotalP= cand.itemTotal().get(0).replace("$", "");
		double value = Double.parseDouble(itemTotalP);
		BigDecimal stripedVal = new BigDecimal(value).stripTrailingZeros();
		Assert.assertEquals("$"+stripedVal+"CAD", pcatOveridePrice.toString().replace("[", "").replace("]", ""));
		cand.clickNortonlink();
		//LogOut DLP
		ncia.UserSignOut();
		//Navigate to PCAT and Reset the Price.
		tabs =ReusableMethods.switchToNewTAB(driver);
		String parentWindow1 = tabs.get(0);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		pcatOveridePrice =pcat.OveridePriceCAD(pcatcanPrice);
		pcat.logOutPCAT();
		driver.close();
		driver.switchTo().window(parentWindow1);
	}

}
