package com.norton.Ecommerce;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities.LogUtil;
import Utilities.PropertiesFile;

public class AS_600_PayPalIntegration extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String emailID, firstlastName, totalPrice;
	Canadiandistributor cand;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 600 - TC 59-60-Verify Paypal Integration and flow in Login and Submit Order")
	@Story("AS 600 - TC 59-60-Verify Paypal Integration and flow in Login and Submit Order")
	@Test
	public void payPalIntegration() throws Exception {
		driver = getDriver();
		driver.manage().deleteAllCookies();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		cand = new Canadiandistributor();
		ncia.clickSignIn();
		ncia.clickPurchaseSignUpRegisterButton();
		emailID = RP.createUser();
		firstlastName = RP.getFLName();
		RP.selectPurchaseOption();
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID.toLowerCase());
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		RP.selectCountryDropDown("COLLEGE", "CN", "CN_QC",
				"Alberta College of Art & Design, Calgary");
		RP.Continue_Button.click();
		RP.selectCheckbox();
		totalPrice =RP.getTotalPrice();
		Assert.assertTrue(RP.issecureButton());
		RP.clickSecureCheckOutButton();
		Thread.sleep(5000);
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.", cand.getCanadadisText());
		String accountName= cand.getNortonAccountDetails().get(0);
		String accountEmail= cand.getNortonAccountDetails().get(1);
		Assert.assertEquals(accountEmail, emailID);
		Assert.assertEquals(accountName, firstlastName);
		Assert.assertNotNull(cand.getorderSummaryDetails());
		String itemTotal= cand.itemTotal().get(0);
		String itemcurrency =cand.itemTotal().get(1);
		Assert.assertEquals(totalPrice, itemTotal+itemcurrency);
		cand.clickPayPal();
		cand.clickCancelLink();
		Assert.assertEquals("Fulfillment by Login Canada", cand.loginCanadatitle());
		//Assert.assertEquals("You’ve been redirected to Norton’s Canadian distributor, Login Canada.", cand.getCanadadisText());
		cand.clickPayPal();
		Assert.assertEquals("Pay with PayPal", cand.headerText.getText());
		Assert.assertEquals("Enter your email or mobile number to get started.", cand.emailSubTag.getText());
		cand.setPayPalUser();
		Assert.assertNotNull(cand.getCardinfo());
		cand.clickContinueButton();
		Assert.assertEquals(accountEmail, emailID);
		Assert.assertEquals(accountName, firstlastName);
		Assert.assertNotNull(cand.getBillingInfo());
		Assert.assertNotNull(cand.getBookName());
		String itemPrice =cand.getItemPrice();		
		String itemCurr= cand.getItemCurrency();		
		Assert.assertEquals(totalPrice, itemPrice+itemCurr);
		cand.clickSubmitOrder();
		Assert.assertEquals(cand.getOrderCompleteText(), "Order Complete");
		String invoiceLabel=cand.getOrderCompleteInvoice().get(0);
		String invoiceNumber=cand.getOrderCompleteInvoice().get(1);
		Assert.assertNotNull( invoiceLabel+ " " + invoiceNumber);
		LogUtil.log(invoiceLabel + " " + invoiceNumber );
		String ordercompText= cand.getOrderCompleteEmailText().toString();
		Assert.assertEquals(ordercompText, "You will receive a confirmation email shortly."+"\n"+"Please save this invoice for your records.");
		Assert.assertEquals(totalPrice, itemPrice+itemCurr);
		cand.clickExitBacktoNortonButton();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		
	}
}
