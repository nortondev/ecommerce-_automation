package com.norton.Ecommerce_US;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
import Utilities.urlDataProvider;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class AS_626_EC400PurchaseIndividualTiles extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	String tileName, emailID = null;
	Set<String> windowHandles;
	List<String> windowHandlesList;
	List<String> tileNamesList;
	PCATPage pcat;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);

	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 626 - EC-400 Purchase Digital Product Entitlements from Individual Tiles/Activities")
	@Story("AS 626 -EC-400 Purchase Digital Product Entitlements from Individual Tiles/Activities")
	@Test(dataProvider = "ECPURLs", dataProviderClass = urlDataProvider.class, priority = 0)
	public void ecpPurchaseEntitlements(String testURLs) throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		RP = new RegisterPurchasePage();
		pcat = new PCATPage();
		driver.navigate().to(testURLs);
		ReusableMethods.checkPageIsReady(driver);
		emailID = RP.createTrialAccessNewUser();
			Thread.sleep(10000);	
		
		boolean isUserName  = driver.findElements(By.xpath("//button[@id='gear_button']//span[@id='gear_button_username'][contains(.,'"+emailID.toLowerCase()+"')]")).size() >0;
		if(isUserName==true) {
		ncia.UserSignOut();
		}
		// get the Tile Name and Pass in to the Method
		ReusableMethods.checkPageIsReady(driver);
		tileNamesList = ncia.getTileName();
		for (int i = 0; i < tileNamesList.size(); i++) {
			tileName = tileNamesList.get(i).toString();
			if (tileName.contains("Smartwork5") || (tileName.contains("Getting Started"))
					|| (tileName.contains("Student Solutions Manual"))) {
				continue;
			} else {
				ncia.clickDLPTile(tileName);
				ReusableMethods.checkPageIsReady(driver);
				windowHandles = driver.getWindowHandles();
				windowHandlesList = new ArrayList<>(windowHandles);
				if (windowHandlesList.size() > 1) {
					driver.switchTo().window(windowHandlesList.get(1));
					Assert.assertTrue(ncia.loginDialog());
					ncia.loginNCIA(emailID, "Tabletop1");
					ReusableMethods.checkPageIsReady(driver);
					String trialText = ncia.getTrialAccessPopUp();
					Assert.assertEquals(trialText,
							"You currently have trial access only. Would you like to upgrade to full access now by purchasing or redeeming a registration code?");
					driver.close();
					driver.switchTo().window(windowHandlesList.get(0));
					ncia.clickdigitalResourceBackButton();
					ncia.UserSignOut();
					continue;
				}
				boolean isChapterTable = driver
						.findElements(By.xpath("//table[contains(@class,'demo_activity_list_table')]/tbody/tr/td/a"))
						.size() > 0;
				if (isChapterTable == true) {
					ncia.clickChapters(2);
					windowHandles = driver.getWindowHandles();
					windowHandlesList = new ArrayList<>(windowHandles);

					if (windowHandlesList.size() > 1) {
						driver.switchTo().window(windowHandlesList.get(1));
						// check its inside the frame
						boolean isiframe = driver
								.findElements(By.xpath("//iframe[contains(@class,'contenttable_iframe')]")).size() > 0;
						if (isiframe == true) {
							WebElement ifameEle = driver
									.findElement(By.xpath("//iframe[contains(@class,'contenttable_iframe')]"));
							driver.switchTo().frame(ifameEle);
							Assert.assertTrue(ncia.loginDialog());
							ncia.loginNCIA(emailID, "Tabletop1");
							ReusableMethods.checkPageIsReady(driver);
							String trialText = ncia.getTrialAccessPopUp();
							Assert.assertEquals(trialText,
									"You currently have trial access only. Would you like to upgrade to full access now by purchasing or redeeming a registration code?");
							driver.switchTo().defaultContent();
							ncia.UserSignOut();

						} else {
							Assert.assertTrue(ncia.loginDialog());
							ncia.loginNCIA(emailID, "Tabletop1");
							ReusableMethods.checkPageIsReady(driver);
							String trialText = ncia.getTrialAccessPopUp();
							Assert.assertEquals(trialText,
									"You currently have trial access only. Would you like to upgrade to full access now by purchasing or redeeming a registration code?");
							driver.close();
							driver.switchTo().window(windowHandlesList.get(0));
							ncia.clickdigitalResourceBackButton();
							ncia.UserSignOut();
						}
					}
				} else {
					boolean isiframe = driver.findElements(By.xpath("//iframe[contains(@class,'contenttable_iframe')]"))
							.size() > 0;
					if (isiframe == true) {
						WebElement ifameEle = driver
								.findElement(By.xpath("//iframe[contains(@class,'contenttable_iframe')]"));
						driver.switchTo().frame(ifameEle);
						Assert.assertTrue(ncia.loginDialog());
						ncia.loginNCIA(emailID, "Tabletop1");
						ReusableMethods.checkPageIsReady(driver);
						String trialText = ncia.getTrialAccessPopUp();
						Assert.assertEquals(trialText,
								"You currently have trial access only. Would you like to upgrade to full access now by purchasing or redeeming a registration code?");
						driver.switchTo().defaultContent();
						ncia.clickdigitalResourceBackButton();
						ncia.UserSignOut();
					} else {
						Assert.assertTrue(ncia.loginDialog());
						ncia.loginNCIA(emailID, "Tabletop1");
						ReusableMethods.checkPageIsReady(driver);
						String trialText = ncia.getTrialAccessPopUp();
						Assert.assertEquals(trialText,
								"You currently have trial access only. Would you like to upgrade to full access now by purchasing or redeeming a registration code?");
						ncia.clickdigitalResourceBackButton();
						ncia.UserSignOut();
					}
				}
			}

		}
		Thread.sleep(8000);
		// Click Purchase Option
		ncia.loginNCIA(emailID, "Tabletop1");
		RP.clickpurchaseOptionButton();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		Thread.sleep(5000);
		RP.clickShowPurchaseButton();
		RP.confirm_Registration(emailID);
		RP.termprivacypolicypopupPurchaseOptions();
		RP.creditcardButton();
		RP.verifyPayment();
		RP.clickCompletePurchase();
		RP.getStarted_popup();
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		// Completed the Purchase
		RP.clickCloseButton();
		tileNamesList = ncia.getTileName();
		for (int i = 0; i < tileNamesList.size(); i++) {
			tileName = tileNamesList.get(i).toString();
			if (tileName.contains("Smartwork5") || (tileName.contains("Getting Started"))
					|| (tileName.contains("Student Solutions Manual"))) {
				continue;
			} else {
				ncia.clickDLPTile(tileName);
				ReusableMethods.checkPageIsReady(driver);
				windowHandles = driver.getWindowHandles();
				windowHandlesList = new ArrayList<>(windowHandles);
				if (windowHandlesList.size() > 1) {
					driver.switchTo().window(windowHandlesList.get(1));
					Assert.assertFalse(ncia.loginDialog());
					ReusableMethods.checkPageIsReady(driver);
					Assert.assertFalse(ncia.isTrialAccessPopUp());
					driver.close();
					driver.switchTo().window(windowHandlesList.get(0));
					ncia.clickdigitalResourceBackButton();
					ncia.UserSignOut();
					continue;
				}
				boolean isChapterTable = driver
						.findElements(By.xpath("//table[contains(@id,'activity_list_table')]/tbody/tr/td/a"))
						.size() > 0;
				if (isChapterTable == true) {
					ncia.clickeBookChapterTitle(2);
					windowHandles = driver.getWindowHandles();
					windowHandlesList = new ArrayList<>(windowHandles);

					if (windowHandlesList.size() > 1) {
						driver.switchTo().window(windowHandlesList.get(1));
						// check its inside the frame
						boolean isiframe = driver
								.findElements(By.xpath("//iframe[contains(@id,'section_iframe')]")).size() > 0;
						if (isiframe == true) {
							WebElement ifameEle = driver
									.findElement(By.xpath("//iframe[contains(@id,'section_iframe')]"));
							driver.switchTo().frame(ifameEle);
							Assert.assertFalse(RP.isUpGradeFullAccessbutton());
							ReusableMethods.checkPageIsReady(driver);
							driver.switchTo().defaultContent();
							driver.close();
							driver.switchTo().window(windowHandlesList.get(0));
							ncia.clickdigitalResourceBackButton();

						} else {
							Assert.assertFalse(RP.isUpGradeFullAccessbutton());
							ReusableMethods.checkPageIsReady(driver);
						    driver.close();
							driver.switchTo().window(windowHandlesList.get(0));
							ncia.clickdigitalResourceBackButton();

						}
					}
				} else {
					boolean isiframe = driver.findElements(By.xpath("//iframe[contains(@id,'section_iframe')]"))
							.size() > 0;
					if (isiframe == true) {
						WebElement ifameEle = driver
								.findElement(By.xpath("//iframe[contains(@id,'section_iframe')]"));
						driver.switchTo().frame(ifameEle);
						Assert.assertFalse(RP.isUpGradeFullAccessbutton());
						ReusableMethods.checkPageIsReady(driver);
						driver.switchTo().defaultContent();
						driver.switchTo().window(windowHandlesList.get(0));
						ncia.clickdigitalResourceBackButton();
					} else {
						Assert.assertFalse(RP.isUpGradeFullAccessbutton());
						ncia.clickdigitalResourceBackButton();

					}
				}
			}
		}
		ncia.UserSignOut();
		Thread.sleep(3000);
	}
}
