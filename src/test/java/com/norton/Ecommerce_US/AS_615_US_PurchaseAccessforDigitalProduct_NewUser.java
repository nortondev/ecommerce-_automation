package com.norton.Ecommerce_US;

import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
@Listeners({TestListener.class})
public class AS_615_US_PurchaseAccessforDigitalProduct_NewUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID;
	String emailid;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 615- Verify Purchase access for digital product as a New User")
	@Story("AS 615 - TC-EC-391:- Verify Purchase access for digital product as a New User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();

		// Verify when user accesses the DLP and selects 'Purchase Options', Then user
		// sees (See Attachment Purchase_Options): The list of options available for
		// purchase along with the access duration

		List<String> tabs = ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		pcatPrice = pcat.capturePrice();
		pcatDesc = pcat.captureisbndetails();
		pcat.logOutPCAT();
		driver.close();
		driver.switchTo().window(parentWindow);

		// Login as a new user
		RP.signInRegister();
		RP.createPurchaseAccessNewUser();
		ReusableMethods.checkPageIsReady(driver);
		/// Thread.sleep(2000);
		
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();

		// Assert dlp pricelist matches with the details present in PCAT
		Assert.assertTrue(pcatPrice.containsAll(dlppriceList));
		Assert.assertTrue(pcatDesc.containsAll(dlpDesc));

		// Verify entitlement selection
		RP.selectOtherCheckbox();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();
		/// Thread.sleep(5000);
		// dlppriceList = RP.capturePurchaseoptionsPrice();

		// Verify Payment Page
		RP.verifyPayment();
		RP.VerifyPurchaseData();

		// Verify - Selected appropriate ‘Sales Tax’ and 'Total Price’ for that purchase
		// should be displayed on ' Review Your Order' Page.
		RP.verifySalexTaxTotalPrice();

		// Verify Complete Purchase Option
		RP.clickCompletePurchase();
		RP.getStarted_popup();

		// Verify if user is able to access the tiles purchased
		ncia.verifyEbookAccessible();
		Assert.assertFalse(ncia.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
		ncia.clickdigitalResourceBackButton();
		ncia.accessNciaAnimation();

		// Verify when user click on Purchase Option button again, purchased options
		// should be displayed as "Already Purchased"
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		RP.clickCloseButton();
		ncia.UserSignOut();

	}

	
	  @AfterTest 
	  public void closeTest() throws Exception {
	  PropertiesFile.tearDownTest();
	  
	  }
	 

}
