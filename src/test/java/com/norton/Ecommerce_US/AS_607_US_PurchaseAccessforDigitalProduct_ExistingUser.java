package com.norton.Ecommerce_US;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
@Listeners({TestListener.class})
public class AS_607_US_PurchaseAccessforDigitalProduct_ExistingUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String email_id;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;
	String childWindow,parentWindow; 

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 607 - Verify Purchase access for digital product as an Existing United States User")
	@Story("AS 607 - TC-EC-390:- Verify Purchase access for digital product as an Existing United States User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		String UserName = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("email").getAsString();
		String Password = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("password").getAsString();

		// Verify the PCAT Entitlement/Purchase Configuration for Standard Product in
		// NEST
		List<String> tabs = ReusableMethods.switchToNewTAB(driver);
		parentWindow = tabs.get(0);
		childWindow = tabs.get(1);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());
		pcatPrice = pcat.capturePrice();
		pcatDesc = pcat.captureisbndetails();
		pcat.logOutPCAT();
		driver.close();

		// Verify details in DLP
		driver.switchTo().window(parentWindow);
		ncia.loginNCIA(UserName, Password);
		ReusableMethods.checkPageIsReady(driver);
		RP.clickpurchaseOptionButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();

		// Verify Price and Desc present in DLP matches with details present in NEST
		Assert.assertTrue(pcatPrice.containsAll(dlppriceList));
		Assert.assertTrue(pcatDesc.retainAll(dlpDesc));

		// Verify Checkbox Selection
		RP.selectOtherCheckbox();
		RP.selectOtherCheckbox();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();

		// Verify all the three purchase/trial options are available
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.textToBePresentInElement(RP.registrationcodeText, "I have a registration code:"));
		boolean isLoginDialog = driver.findElements(By.id("login_dialog")).size() > 0;
		if (isLoginDialog == true) {
			String regCodeText = RP.registrationcodeText.getAttribute("innerText");
			Assert.assertEquals("I have a registration code:", regCodeText);
			String purchaseAccessText = RP.purchaseaccessText.getAttribute("innerText");
			Assert.assertEquals(
					"I want to purchase access\n(All titles available in the US and its territories. Most titles available in Canada.)",
					purchaseAccessText);
			String trailAccessText = RP.trialaccessText.getAttribute("innerText");
			Assert.assertEquals("I want to sign up for 21 days of trial access", trailAccessText);
		}

		RP.clickShowPurchaseButton();
		RP.confirm_Registration(UserName);
		Thread.sleep(2000);
		RP.selectCheckboxTermofUse();

		// Verify as School/College Student, Country, State and School Name
		Assert.assertEquals("a College/University student", RP.schoolDropdownDefault());
		Assert.assertEquals("Select Country", RP.countrySelectedDefault());

		schoolName = new ArrayList<String>();
		schoolName.add("a College/University student");
		schoolName.add("a High School student");
		schoolName.add("not a student");
		Assert.assertTrue(RP.schoolDropdownlist().containsAll(schoolName));

		countryName = new ArrayList<String>();
		countryName.add("Select Country");
		countryName.add("the United States");
		countryName.add("Canada");
		Assert.assertTrue(RP.countryDropdownlist().containsAll(countryName));
		Thread.sleep(3000);
		RP.selectCountryDropDown("HIGH_SCHOOL", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		Thread.sleep(3000);
		RP.selectCheckboxTermofUse();
		Thread.sleep(2000);

		// Verify as NOT A STUDENT
		RP.selectCheckboxTermofUse();
		RP.selectNotaStudent("NEITHER", "USA");
		// Assert.assertFalse(RP.selectNotaStudent("NEITHER", "USA"));
		RP.clickContinueButton();
		RP.clickBackUpButton();
		RP.clickShowPurchaseButton();
		Thread.sleep(2000);

		// Continuing as College Student
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		RP.clickContinueButton();
		Thread.sleep(5000);
		dlppriceList = RP.capturePurchaseoptionsPrice();

		// Verify Payment Page
		RP.verifyPayment();
		RP.VerifyPurchaseData();

		// Verify - Selected appropriate ‘Sales Tax’ and 'Total Price’ for that purchase
		// should be displayed on ' Review Your Order' Page.
		RP.verifySalexTaxTotalPrice();
		
		// Verify Complete Purchase Option
		RP.clickCompletePurchase();
		RP.getStarted_popup();

		// Verify is user is able to access the purchased tile
		ncia.verifyEbookAccessible();
		Assert.assertFalse(ncia.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
		ncia.clickdigitalResourceBackButton();
		ncia.accessNciaAnimation();											
		
		//Verify when user click on Purchase Option button again, purchased options should be displayed as "Already Purchased"
		RP.clickpurchaseOptionButton();
		RP.VerifyPurchaseData();
		
	}

	@AfterTest
	public void closeTest() throws Exception {
	PropertiesFile.tearDownTest();
	}


}
