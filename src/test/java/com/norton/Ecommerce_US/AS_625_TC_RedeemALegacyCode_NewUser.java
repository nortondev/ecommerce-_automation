package com.norton.Ecommerce_US;

import java.util.Iterator;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.Mailinator;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
@Listeners({TestListener.class})
public class AS_625_TC_RedeemALegacyCode_NewUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String email_id ;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("Jazz2");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 625_RedeemALegacyCode_New_User")
	@Story("AS 625- TC_EC-399 QA Only: Redeem a Legacy code to access a digital product on DLP as an New User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		
		RP.Sign_in_or_Register.click();
		RP.SignInButton.click();
		String incorrect_access_code = PropertiesFile.getInvalidRegistrationcode();
		String access_code = PropertiesFile.getValidRegistrationcode();
		String used_access_code =PropertiesFile.getUsedRegistrationcode();
		String legacycode = PropertiesFile.getvalid_legacy_code();
		
		// Sign In As New User with Invalid Code
		email_id =RP.register_Access_InValidCode(incorrect_access_code);
		Thread.sleep(5000);
		String incorrect_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		System.out.println(incorrect_code_errormsg);
		String expected_invalidcode_errmsg = "Error: The access code you entered is not valid. Most access codes have 8 or 9 alphanumeric characters, optionally separated by one or more dashes.";
		System.out.println(expected_invalidcode_errmsg);
		Assert.assertTrue(incorrect_code_errormsg.contains(expected_invalidcode_errmsg));
		

		// When user provide a Used Code
		Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(used_access_code);
		RP.click_register_My_code_Button();
		Thread.sleep(5000);
		//RP.emailConfirm.sendKeys(email_id);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		//RP.click_confirmButton();
		RP.clickContinueButton();
		Thread.sleep(5000);
		String used_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		System.out.println(used_code_errormsg);
		String expected_usedcode_msg = "Error: The access code you entered has already been used.";
		System.out.println(expected_usedcode_msg);
		Assert.assertTrue(used_code_errormsg.contains(expected_usedcode_msg));

		// Verify successful purchase when user provides a Valid Registration Code
		Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(legacycode);
		RP.click_register_My_code_Button();
		Thread.sleep(2000);
		RP.clickBackUpButton();
		Thread.sleep(2000);
		RP.click_register_My_code_Button();
		Thread.sleep(2000);
		//RP.emailConfirm.sendKeys(email_id);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AZ", "Benedictine University, Mesa");
		//RP.click_confirmButton();
		RP.clickContinueButton();

		// **Add Assertion for Get Started Page - Registration Code, Name and Email ID
		Thread.sleep(2000);
		String gtg_registration_code = RP.gtg_reg_id.getAttribute("innerText");
		String gtg_name = RP.gtg_name.getAttribute("innerText");
		String gtg_email = RP.gtg_email.getAttribute("innerText");
		String expected_gtg_name = RP.first_name+" " + RP.last_name;
		String expected_gtg_email = email_id;
		Assert.assertEquals(gtg_name, expected_gtg_name);
		Assert.assertEquals(gtg_email.toUpperCase(), expected_gtg_email.toUpperCase());
		Assert.assertEquals(gtg_registration_code, access_code);
		RP.getStarted_popup();
		
	
	// Legacy Code Dependency
							
//		// Verify User is not able to access ebook chapters 
//		ncia.clickEbookTile();
//		Thread.sleep(2000);
//		ncia.ebook_psychsc6.click();
//		Thread.sleep(2000);
//		ReusableMethods.switchToNewWindow(2, driver);
//		Thread.sleep(5000);
//		driver.switchTo().frame("section_iframe");
//		String error_message_ebook_chapter = RP.ebook_unaccessible_errormessage_new.getAttribute("innerText");
//		String expected_ebook_errmsg = "This section of the e-book is only available to registered users.";
//		Assert.assertTrue(error_message_ebook_chapter.contains(expected_ebook_errmsg));
//		ReusableMethods.switchToNewWindow(1, driver);
//		ncia.clickReturnArrow();
//		Thread.sleep(2000);
							
						
		// Verify when user click on Purchase Option Button then user can see my
		// purchased options are disabled with label 'Already Purchased’
		// and can continue with the purchase or redemption of codes to get access for
		// the other entitlement as well - Cover in 622
		RP.purchaseOptionsButton.click();
		RP.VerifyPurchaseData();

		// Verification in Mailinator
		Mailinator checkMail = new Mailinator(driver);
		checkMail.SearchEmail(email_id);
		List<String> MailSubjects = checkMail.emailNotifications();
		Iterator<String> itrSubjects = MailSubjects.iterator();
		boolean found = false;
		while (itrSubjects.hasNext()) {
			String temp = itrSubjects.next().toString();
			if (temp.equals(PropertiesFile.getEmailText()))
				found = true;
		}
		Assert.assertEquals(found, found);
	}

	@AfterTest
	public void closeTest() throws Exception {
	PropertiesFile.tearDownTest();

	}

}
