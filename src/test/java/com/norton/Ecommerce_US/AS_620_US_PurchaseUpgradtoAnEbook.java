package com.norton.Ecommerce_US;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.PropertiesFile;
import Utilities.ReusableMethods;
@Listeners({TestListener.class})
public class AS_620_US_PurchaseUpgradtoAnEbook extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 620- Verify User is able to Upgrade to an Ebook after initial Purchase From the DLP")
	@Story("AS 620 - TC-EC-413 Purchase Upgrade to an Ebook after initial Purchase From the DLP")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();		
		
		//Login as new user and purchase addons 
		RP.signInRegister();
		emailID =RP.createPurchaseAccessNewUser();
		ReusableMethods.checkPageIsReady(driver);
		RP.selectOtherCheckbox();
		RP.clickRegisterAccessButton(); 
		
		//Make Payment 
		RP.verifyPayment();
		RP.VerifyPurchaseData();
		RP.CompleteYourPurchase.click();
		RP.getStarted_popup();
		
		//Step 2 (Verify if already purchased should be greyed out and the user is unable to make the selection for that Purchase option.)
		
		Thread.sleep(5000);
		ReusableMethods.scrollIntoView(driver, RP.purchaseOptionsButton);
		RP.clickpurchaseOptionButton();
	    RP.VerifyPurchaseData();
		
		//Step 3 (User should be able to purchase EBook
		RP.clickCheckbox_Ebook();
		ReusableMethods.scrollIntoView(driver, RP.PurchaseRegisterAccess);
		RP.clickRegisterAccessButton();
		Thread.sleep(2000);
		RP.clickShowPurchaseButton();
		Thread.sleep(2000);
		//Email Id to be updated here 
		//String new_user_email = RP.new_user_emailID;
		RP.emailConfirm.sendKeys(emailID.toLowerCase());
		RP.click_confirmButton();
		Thread.sleep(2000);
		RP.selectCheckboxTermofUse();
		Thread.sleep(2000);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		RP.clickContinueButton();
		
		//Make Payment
		RP.verifyPayment();
		RP.VerifyPurchaseData();
		
		//Assertion - Verify ‘Sales Tax’ and  'Total Price’ for that purchase  displayed  on ' Review Your Order' Page is as expected
		String CapturedSalesTax = RP.captureSalesTax();
		String SubStringSalexTax = CapturedSalesTax.substring(14);
		String expected_ebook_salestax = PropertiesFile.ebook_salestax;
		Assert.assertEquals(SubStringSalexTax, expected_ebook_salestax);
		String TotalPrice = RP.captureTotalPrice();
		String expected_ebook_total_price = PropertiesFile.ebook_totalprice;
		Assert.assertEquals(TotalPrice, expected_ebook_total_price);
		
		//Complete Purchase
		RP.clickCompletePurchase();
		RP.getStarted_popup();
		
		// Verify is user is able to access the purchased tile
		ncia.verifyEbookAccessible();
		Assert.assertFalse(ncia.isUpGradeFullAccessbutton());
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
		ncia.clickdigitalResourceBackButton();
		ncia.accessNciaAnimation();
	     
}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
