package com.norton.Ecommerce_US;

import java.time.Duration;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;

import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
import Utilities.TestListener;

@Listeners({ TestListener.class })
public class AS_605_US_InitialWorkFlowonDLP extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String emailID;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 605- Verify Initial Workflow in a Digital Landing Page as an Existing User")
	@Story("AS 605 - TC_01-EC-388:-Verify Initial Workflow in a Digital Landing Page as an Existing User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		String UserName = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("email").getAsString();
		String Password = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("password").getAsString();

		// Verify the PCAT Entitlement/Purchase Configuration for Standard Product in
		// NEST
		List<String> tabs = ReusableMethods.switchToNewTAB(driver);
		String parentWindow = tabs.get(0);
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		pcat.selectDiscpline(PropertiesFile.disciplineValue());
		pcat.selectProduct(PropertiesFile.productCode());

		// PCAT Entitlement/Purchase Configuration for Standard Product
		pcatPrice = pcat.capturePrice();
		pcatDesc = pcat.captureisbndetails();
		pcat.logOutPCAT();
		driver.close();

		// Verify details in DLP
		driver.switchTo().window(parentWindow);
		//RP.signInRegister();
		ncia.clickSignIn();
		ncia.loginNCIA(UserName, Password);
		//RP.StudentEmail.sendKeys(UserName);
		//RP.Password.sendKeys(Password);
		//RP.click_SignInButton();
		ReusableMethods.checkPageIsReady(driver);
		RP.clickpurchaseOptionButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();

		// Verify Price and Desc present in DLP matches with details present in NEST
		Assert.assertTrue(pcatPrice.containsAll(dlppriceList));
		Assert.assertTrue(pcatDesc.containsAll(dlpDesc));

		// Verify User is able to see the Purchase Options

		// Verify Checkbox Selection
		RP.selectOtherCheckbox();
		RP.selectOtherCheckbox();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();

		// Verify all the three purchase/trial options are present
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.textToBePresentInElement(RP.registrationcodeText, "I have a registration code:"));

		boolean isLoginDialog = driver.findElements(By.id("login_dialog")).size() > 0;
		if (isLoginDialog == true) {
			String regCodeText = RP.registrationcodeText.getAttribute("innerText");
			Assert.assertEquals("I have a registration code:", regCodeText);
			String purchaseAccessText = RP.purchaseaccessText.getAttribute("innerText");
			Assert.assertEquals(
					"I want to purchase access\n(All titles available in the US and its territories. Most titles available in Canada.)",
					purchaseAccessText);
			String trailAccessText = RP.trialaccessText.getAttribute("innerText");
			Assert.assertEquals("I want to sign up for 21 days of trial access", trailAccessText);
		}
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
