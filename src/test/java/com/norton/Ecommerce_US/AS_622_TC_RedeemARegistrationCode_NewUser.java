package com.norton.Ecommerce_US;

import java.time.Duration;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;
import Utilities.LogUtil;
import Utilities.Mailinator;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;

@Listeners({ TestListener.class })
public class AS_622_TC_RedeemARegistrationCode_NewUser extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String email_id;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;
	WebElement getEmailcourseInvi = null;
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("psychsci6");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 622_RedeemARegistrationCode_New_User")
	@Story("AS 622- TC_EC-397 QA Only: Redeem a Registration code to access a digital product on DLP as an New User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		String getaccesscode = getAccessCodeNEST("Psychology", "psychsci6: Psychological Science");
		
		driver.navigate().refresh();
		ncia.navigateNCIADLP("psychsci6");
		RP.Sign_in_or_Register.click();
		RP.SignInButton.click();
		String incorrect_access_code = PropertiesFile.getInvalidRegistrationcode();
		//String access_code = PropertiesFile.getValidRegistrationcode();
		String used_access_code = PropertiesFile.getUsedRegistrationcode();

//		String access_code =pcat.getAccessCodeNEST("Psychology", "psychsci6: Psychological Science");

		// Sign In As New User with Invalid Code
		email_id =RP.register_Access_InValidCode(incorrect_access_code);
		Thread.sleep(5000);
		String incorrect_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		LogUtil.log(incorrect_code_errormsg);
		String expected_invalidcode_errmsg = "Error: The access code you entered is not valid. Most access codes have 8 or 9 alphanumeric characters, optionally separated by one or more dashes.";
		LogUtil.log(expected_invalidcode_errmsg);
		Assert.assertTrue(incorrect_code_errormsg.contains(expected_invalidcode_errmsg));

		// When user provide a Used Code
		Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(used_access_code);
		RP.click_register_My_code_Button();
		Thread.sleep(2000);
		//RP.emailConfirm.sendKeys(email_id);
		//Thread.sleep(2000);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		//RP.click_confirmButton();
		RP.clickContinueButton();
		Thread.sleep(5000);
		String used_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		LogUtil.log(used_code_errormsg);
		String expected_usedcode_msg = "Error: The access code you entered has already been used.";
		LogUtil.log(expected_usedcode_msg);
		Assert.assertTrue(used_code_errormsg.contains(expected_usedcode_msg));

		// Verify successful purchase when user provides a Valid Registration Code
		Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(getaccesscode);
		RP.click_register_My_code_Button();
		Thread.sleep(2000);
		//RP.emailConfirm.sendKeys(email_id);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		//RP.click_confirmButton();
		RP.clickContinueButton();

		// **Add Assertion for Get Started Page - Registration Code, Name and Email ID
		Thread.sleep(2000);
		String gtg_registration_code = RP.gtg_reg_id.getAttribute("innerText");
		String gtg_name = RP.gtg_name.getAttribute("innerText");
		String gtg_email = RP.gtg_email.getAttribute("innerText");
		String expected_gtg_name = RP.first_name + " " + RP.last_name;
		String expected_gtg_email = RP.EmailID;
		Assert.assertEquals(gtg_name, expected_gtg_name);
		Assert.assertEquals(gtg_email.toUpperCase(), expected_gtg_email.toUpperCase());
		Assert.assertEquals(gtg_registration_code, getaccesscode);
		RP.getStarted_popup();

		// Registration Code provided for Psychsc6 Code used here provides access to
		// Inquisitive Tile only

		// Verify User is not able to access ebook chapters
		ncia.clickDLPTile("Ebook");
		Thread.sleep(2000);
		ncia.ebook_psychsc6.click();
		Thread.sleep(2000);
		ReusableMethods.switchToNewWindow(2, driver);
		Thread.sleep(5000);
		driver.switchTo().frame("section_iframe");
		String error_message_ebook_chapter = RP.ebook_unaccessible_errormessage_new.getAttribute("innerText");
		String expected_ebook_errmsg = "This section of the e-book is only available to registered users.";
		Assert.assertTrue(error_message_ebook_chapter.contains(expected_ebook_errmsg));
		ReusableMethods.switchToNewWindow(1, driver);
		ncia.clickdigitalResourceBackButton();
		Thread.sleep(2000);

		// Verify user is able to access Inquisitive Tile
		ncia.verifyInquisitiveAccessible();

		// Verify when user click on Purchase Option Button then user can see my
		// purchased options are disabled with label 'Already Purchased’
		// and can continue with the purchase or redemption of codes to get access for
		// the other entitlement as well - Cover in 622
		RP.purchaseOptionsButton.click();
		RP.VerifyPurchaseData();

		// Verification in Mailinator
		Mailinator checkMail = new Mailinator(driver);
		checkMail.SearchEmail(RP.EmailID);
		getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		Thread.sleep(4000);
		String getEmailSubjectCourseInvi = "Account Confirmation";		
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		int counter = 0; //optional, just to cut off infinite waiting 
		while( getEmailcourseInvi == null && counter != 15 ){
		    try{
		    	getEmailcourseInvi = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[contains(@class,'jambo_table')]/tbody/tr/td[contains(.,'Account Confirmation')]")));
		    } catch(TimeoutException te) {
		        System.out.println("Not loaded yet, continuing");
		        counter++;
		        driver.navigate().refresh();
		    }
		}

		getEmailcourseInvi = driver
				.findElement(By
						.xpath("//table[contains(@class,'jambo_table')]/tbody/tr/td[contains(.,'Account Confirmation')]"));

		String getcourseInvifText = getEmailcourseInvi.getText().trim();
		Assert.assertTrue(getcourseInvifText.contains(getEmailSubjectCourseInvi));
		
		/*
		 * List<String> MailSubjects = checkMail.emailNotifications(); Iterator<String>
		 * itrSubjects = MailSubjects.iterator(); boolean found = false; while
		 * (itrSubjects.hasNext()) { String temp = itrSubjects.next().toString(); if
		 * (temp.equals(PropertiesFile.getEmailText())) found = true; }
		 * Assert.assertEquals(found, found);
		 */
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

	public String getAccessCodeNEST(String discpline, String product) throws InterruptedException {
		pcat.navigatePCAT();
		pcat.loginto_Pcat();
		ReusableMethods.checkPageIsReady(driver);
		// pcat.clickgearMenu();
		// pcat.clickNCIAEntMenu();
		pcat.selectDiscpline(discpline);
		pcat.selectProduct(product);
		pcat.generateAccessCodebasedTileName("inquizitive");
		String getaccesscode = pcat.getAccessCode();
		pcat.clickgearMenu();
		pcat.SignOut_Pcat();
		return getaccesscode;

	}
}
