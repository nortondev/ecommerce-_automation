package com.norton.Ecommerce_US;

import java.util.Iterator;
import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.Mailinator;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;


@Listeners({ TestListener.class })
public class AS_616_US_PrintUpsell extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;
    String emailID;
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 616- US PrintUpsell")
	@Story("AS 616 - TC-EC-406:- US PrintUpsell")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();

		// String UserName =
		// jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("email").getAsString();
		// String Password =
		// jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("password").getAsString();
		// Login as a new user
		RP.signInRegister();
		emailID =RP.createPurchaseAccessNewUser();
		ReusableMethods.checkPageIsReady(driver);
		RP.selectCheckboxBasedonName("Ebook");
		// Sign in to DLP
		RP.clickRegisterAccessButton();
		RP.verifyPayment();
		RP.clickCompletePurchase();
		RP.getStarted_popup();
		
		/*
		 * ReusableMethods.scrollIntoView(driver, ncia.gear_button_username);
		 * RP.VerifyPurchaseData(); RP.clickCloseButton();
		 */
		Thread.sleep(5000);
		ncia.clickDLPTile("Ebook");
		Thread.sleep(2000);
		ncia.clickeBookChapterTitle(1);

		// Switching to new window
		ReusableMethods.switchToNewWindow(2, driver);
		ncia.clickSettingsButton();
		ncia.clickPrintCopyLink();
		ncia.clickShowPrintPurchase();
		ncia.clickPrintPurchaseCheckbox();
		// Verify Payment Page
		RP.verifyPayment();
		Thread.sleep(2000);
		// Verify and Assert Total Price of the printed Ebook
		RP.verifyTotalPricePrinted();
		// Complete Purchase
		RP.clickCompletePurchase();
		RP.click_PurchaseCompletePopUp();

		// Assert when user tried to purchase the printed book again - Option to
		// purchase should not be available
		ncia.clickSettingsButton();

		//
		boolean found1 = false;
		List<WebElement> List = driver.findElements(By.xpath(
				"//div[@class=\"item_group\"]//button[@class=\"menu_item account_menu_menu_item text-button button\"]//span[@class =\"icon-with-text\"]"));
		Iterator<WebElement> itr = List.iterator();
		while (itr.hasNext()) {
			String temp1 = itr.next().getAttribute("innerText");
			if (temp1.contains("Purchase")) {
				found1 = true;
			}
		}
		Assert.assertEquals(found1, false);

		// Verification in Mailinator
		Mailinator checkMail = new Mailinator(driver);
		checkMail.SearchEmail(emailID);
		List<String> MailSubjects = checkMail.emailNotifications();
		Iterator<String> itrSubjects = MailSubjects.iterator();
		boolean found = false;
		while (itrSubjects.hasNext()) {
			String temp = itrSubjects.next().toString();
			if (temp.equals(PropertiesFile.getEmailText()))
				found = true;
		}
		Assert.assertEquals(found, found);

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();

	}

}
