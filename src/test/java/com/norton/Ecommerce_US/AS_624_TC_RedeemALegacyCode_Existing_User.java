package com.norton.Ecommerce_US;

import java.util.List;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.norton.Ecommerce.NCIA;
import com.norton.Ecommerce.PCATPage;
import com.norton.Ecommerce.RegisterPurchasePage;
import Utilities.TestListener;

import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
@Listeners({TestListener.class})
public class AS_624_TC_RedeemALegacyCode_Existing_User extends PropertiesFile {

	NCIA ncia;
	RegisterPurchasePage RP;
	PCATPage pcat;
	String email_id, access_code, incorrect_access_code, used_access_code;
	List<String> dlppriceList, pcatPrice, pcatDesc, pcatcanPrice, dlpDesc, schoolName, countryName;

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL("");
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("AS 624_RedeemALegacyCode_Existing_User")
	@Story("AS 624- TC_EC-398 QA Only: Redeem a Legacy code to access a digital product on DLP as an Existing User")
	@Test
	public void purchaseOptionUS() throws Exception {
		driver = getDriver();
		ncia = new NCIA();
		pcat = new PCATPage();
		RP = new RegisterPurchasePage();
		driver.manage().deleteAllCookies();
		RP.clickpurchaseOptionButton();
		RP.clickCloseButton();
		
		ncia.navigateNCIADLP("jazz2");

		String UserName = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("email").getAsString();
		String Password = jsonobject.getAsJsonObject("EXISTINGCREDENTIALS").get("password").getAsString();

		String incorrect_access_code = PropertiesFile.getInvalidRegistrationcode();
		String access_code = PropertiesFile.getValidRegistrationcode();
		String used_access_code = PropertiesFile.getUsedRegistrationcode();
		String legacycode = PropertiesFile.getvalid_legacy_code();
		String firstname = PropertiesFile.getExistingUserFirstName();
		String lastname = PropertiesFile.getExistingUserLastName();
		
		// Sign In As Existing User
		ncia.loginNCIA(UserName, Password);
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		RP.clickpurchaseOptionButton();
		dlppriceList = RP.capturePurchaseoptionsPrice();
		dlpDesc = RP.capturePurchaseoptionsDesc();

		// Verify Checkbox Selection
		RP.selectOtherCheckbox();
		RP.selectOtherCheckbox();
		RP.selectCheckbox();
		RP.clickRegisterAccessButton();

//		// Assertions:
//		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
//		wait.until(ExpectedConditions.textToBePresentInElement(RP.registrationcodeText, "I have a registration code:"));
//		boolean isLoginDialog = driver.findElements(By.id("login_dialog")).size() > 0;
//		if (isLoginDialog == true) {
//			String regCodeText = RP.registrationcodeText.getAttribute("innerText");
//			Assert.assertEquals("I have a registration code:", regCodeText);
//			String purchaseAccessText = RP.purchaseaccessText.getAttribute("innerText");
//			Assert.assertEquals(
//					"I want to purchase access\n(All titles available in the US and its territories. Most titles available in Canada.)",
//					purchaseAccessText);
//			String trailAccessText = RP.trialaccessText.getAttribute("innerText");
//			Assert.assertEquals("I want to sign up for 21 days of trial access", trailAccessText);
//		}

		// When user provide Incorrect Code
		RP.click_registration_code();
		Thread.sleep(2000);
		RP.access_code.sendKeys(incorrect_access_code);
		RP.click_register_My_code_Button();
		Thread.sleep(5000);
		RP.StudentEmail.sendKeys(UserName);
		RP.click_confirmButton();
		RP.selectCheckboxTermofUse();
		Thread.sleep(5000);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		Thread.sleep(5000);
		RP.clickContinueButton();
		
		//Verify and Assert Error message
		Thread.sleep(2000);
		String incorrect_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		String expected_invalidcode_errmsg = "Error: The access code you entered is not valid. Most access codes have 8 or 9 alphanumeric characters, optionally separated by one or more dashes.";
		Assert.assertTrue(incorrect_code_errormsg.contains(expected_invalidcode_errmsg));

		// When user provide a Used Code
		// RP.click_registration_code();
		Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(used_access_code);
		RP.click_register_My_code_Button();
		Thread.sleep(2000);
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		Thread.sleep(2000);
		RP.clickContinueButton();
		
		//Verify and Assert error mesaage 
		Thread.sleep(2000);
		String used_code_errormsg = RP.Error_Msg.getAttribute("innerText");
		String expected_usedcode_msg = "Error: The access code you entered has already been used.";
		Assert.assertTrue(used_code_errormsg.contains(expected_usedcode_msg));

		// Verify successful purchase when user provides a Valid Registration Code
		// Thread.sleep(2000);
		RP.access_code.clear();
		RP.access_code.sendKeys(legacycode);
		RP.click_register_My_code_Button();
		RP.selectCountryDropDown("COLLEGE", "USA", "USA_AL", "Air Force Cyber College, Maxwell AFB");
		Thread.sleep(2000);
		RP.clickContinueButton();
		Thread.sleep(5000);

		// **Add Assertion for Get Started Page - Registration Code, Name and Email ID
		
		String gtg_registration_code = RP.gtg_reg_id.getAttribute("innerText");
		String gtg_name = RP.gtg_name.getAttribute("innerText");
		String gtg_email = RP.gtg_email.getAttribute("innerText");
		String expected_gtg_name = firstname+" "+ lastname;
		String expected_gtg_email = UserName;
		Assert.assertEquals(gtg_name, expected_gtg_name);
		Assert.assertEquals(gtg_email.toUpperCase(), expected_gtg_email.toUpperCase());
		Assert.assertEquals(gtg_registration_code, access_code);
		RP.getStarted_popup();

		// Legacy Code Dependency

		// Verify User is not able to access ebook chapters
//		ncia.clickEbookTile();
//		Thread.sleep(2000);
//		ncia.ebook_psychsc6.click();
//		Thread.sleep(2000);
//		ReusableMethods.switchToNewWindow(2, driver);
//		Thread.sleep(5000);
//		driver.switchTo().frame("section_iframe");
//		String error_message_ebook_chapter = RP.ebook_unaccessible_errormessage_new.getAttribute("innerText");
//		String expected_ebook_errmsg = "This section of the e-book is only available to registered users.";
//		Assert.assertTrue(error_message_ebook_chapter.contains(expected_ebook_errmsg));
//		ReusableMethods.switchToNewWindow(1, driver);
//		ncia.clickReturnArrow();
//		Thread.sleep(2000);

		

		// Verify when user click on Purchase Option Button then user can see my
		// purchased options are disabled with label 'Already Purchased’
		// and can continue with the purchase or redemption of codes to get access for
		// the other entitlement as well - Cover in 622

		RP.purchaseOptionsButton.click();
		RP.VerifyPurchaseData();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}

}
