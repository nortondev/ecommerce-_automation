package Utilities;

import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.DataProvider;


public class urlDataProvider {
	public static String UserDir = System.getProperty("user.dir");
		
	@DataProvider(name = "ECPURLs")
	public static Object[][] readDataProviderEntlJsonecp() throws IOException,
			ParseException {

		JSONParser jsonparser = new JSONParser();
		FileReader reader = new FileReader(UserDir + "//resources//ECPURL.json");
		JSONObject obj = (JSONObject) jsonparser.parse(reader);
		JSONArray productCodelist = (JSONArray) obj.get("URLList");
		Object[][] returnValue = new Object[productCodelist.size()][1];
		int index = 0;
		for (Object[] url : returnValue) {
			url[0] = productCodelist.get(index++).toString();
		}
		return returnValue;

	}
	

}
