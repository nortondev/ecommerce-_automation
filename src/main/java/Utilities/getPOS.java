package Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class getPOS {
	public static String UserDir = System.getProperty("user.dir");

	static ArrayList<String> searchKeys = new  ArrayList<String>();
	static String sentence;
	
	/*public static void main(String[] args) throws Exception {
		
		sentence ="Where did the first peoples to the Americas come from?";
		String word = new getPOS().givenPOSModel_whenPOSTagging_thenPOSAreDetected(sentence,searchKeys);
		System.out.println(word);
	}*/
	
	
	public String givenPOSModel_whenPOSTagging_thenPOSAreDetected(
		String sentence, ArrayList<String> searchKeys) throws Exception {
		String outPutWord = null;
		String word = "";
		
		// String perm = "rwxrwx---";
		// File dir = new File("/models");
		// setAsExecutable(UserDir +"\\src\\main\\java\\models");
		InputStream posModelInputStream = new FileInputStream("en-pos-maxent.bin");
				
				//getClass().getResourceAsStream();

		//System.out.println(inputStreamPOSTagger);
		POSModel posModel = new POSModel(posModelInputStream);
		POSTaggerME posTagger = new POSTaggerME(posModel);

		// Tokenizing the sentence using WhitespaceTokenizer class
		WhitespaceTokenizer whitespaceTokenizer = WhitespaceTokenizer.INSTANCE;
		String outputText =removeStopWord.whenRemoveStopwordsManually_thenSuccess(sentence);
		
		String[] tokens = whitespaceTokenizer.tokenize(outputText);
		
		String[] tags = posTagger.tag(tokens);
		// Instantiating the POSSample class
		POSSample sample = new POSSample(tokens, tags);
		String key = null;

		String outputString = sample.toString();
		String[] outputTags = outputString.split("\\s+");

		for (int i = 0; i < outputTags.length; i++) {
			

			if (outputTags[i].contains("JJ") && (!searchKeys.contains(word))) {
				key = "_JJ";
			}else if (outputTags[i].contains("DT")
						&& (!searchKeys.contains(word))) {
					key = "_DT";
			}else if (outputTags[i].contains("WRB")
					&& (!searchKeys.contains(word))) {
				key = "_WRB";
			}else if (outputTags[i].contains("NNPS")
					&& (!searchKeys.contains(word))) {
				key = "_NNPS";
			}else if (outputTags[i].contains("NNS")
					&& (!searchKeys.contains(word))) {
				key = "_NNS";
			} else if (outputTags[i].contains("NNP")
					&& (!searchKeys.contains(word))) {
				key = "_NNP";
			}else if (outputTags[i].contains("VBD")
					&& (!searchKeys.contains(word))) {
				key = "_VBD";
			}else if (outputTags[i].contains("TO")
					&& (!searchKeys.contains(word))) {
				key = "_TO";
			}else if (outputTags[i].contains("_NN")
					&& (!searchKeys.contains(word))) {
				key = "_NN";
			}else if (outputTags[i].contains("_VBP")
				&& (!searchKeys.contains(word))) {
			key = "_VBP";
		}else if (outputTags[i].contains("_IN")
				&& (!searchKeys.contains(word))) {
			key = "_IN";
		}
			if (key != null) {
				outPutWord = outputTags[i];
				int index = outPutWord.indexOf(key);
				word = outPutWord.substring(0, index);
				
				if(word.equalsIgnoreCase("is")){
					continue;				
				}else  {
				// Check if not present in searchkeys array
				break;
				}
			} else
				continue;
		}
			
		
		return word;
	
		
	}

	public static void setAsExecutable(String filePath) throws IOException {
		File file = new File(filePath);

		// set application user permissions to 455
		file.setExecutable(false);
		file.setReadable(false);
		file.setWritable(true);

		// change permission to 777 for all the users
		// no option for group and others
		file.setExecutable(true, false);
		file.setReadable(true, false);
		file.setWritable(true, false);
		// using PosixFilePermission to set file permissions 755
		Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
		// add owners permission
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		perms.add(PosixFilePermission.OWNER_EXECUTE);
		// add group permissions
		perms.add(PosixFilePermission.GROUP_READ);
		perms.add(PosixFilePermission.GROUP_WRITE);
		perms.add(PosixFilePermission.GROUP_EXECUTE);
		// add others permissions
		perms.add(PosixFilePermission.OTHERS_READ);
		perms.add(PosixFilePermission.OTHERS_WRITE);
		perms.add(PosixFilePermission.OTHERS_EXECUTE);

		Files.setPosixFilePermissions(Paths.get(filePath), perms);
		// logger.info("Modified as executable " + filePath);

	}
	
	/*public static String[] stopWordsofwordnet = {
		"without", "see", "unless", "due", "also", "must", "might", "like", "]", "[", "}", "{", "<", ">", "?", "\"", "\\", "/", ")", "(", "?","will", "may", "can", "much", "every", "the", "in", "other", "this", "the", "many", "any", "an", "or", "for", "in", "an", "an ", "is", "a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren’t", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can’t", "cannot", "could",
		"couldn’t", "did", "didn’t", "do", "does", "doesn’t", "doing", "don’t", "down", "during", "each", "few", "for", "from", "further", "had", "hadn’t", "has", "hasn’t", "have", "haven’t", "having",
		"he", "he’d", "he’ll", "he’s", "her", "here", "here’s", "hers", "herself", "him", "himself", "his", "how", "how’s", "i ", " i", "i’d", "i’ll", "i’m", "i’ve", "if", "in", "into", "is",
		"isn’t", "it", "it’s", "its", "itself", "let’s", "me", "more", "most", "mustn’t", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "ought", "our", "ours", "ourselves",
		"out", "over", "own", "same", "shan’t", "she", "she’d", "she’ll", "she’s", "should", "shouldn’t", "so", "some", "such", "than",
		"that", "that’s", "their", "theirs", "them", "themselves", "then", "there", "there’s", "these", "they", "they’d", "they’ll", "they’re", "they’ve",
		"this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn’t", "we", "we’d", "we’ll", "we’re", "we’ve",
		"were", "weren’t", "what", "what’s", "when", "when’s", "where", "where’s", "which", "while", "who", "who’s", "whom",
		"why", "why’s", "with", "won’t", "would", "wouldn’t", "you", "you’d", "you’ll", "you’re", "you’ve", "your", "yours", "yourself", "yourselves",
		"Without", "See", "Unless", "Due", "Also", "Must", "Might", "Like", "Will", "May", "Can", "Much", "Every", "The", "In", "Other", "This", "The", "Many", "Any", "An", "Or", "For", "In", "An", "An ", "Is", "A", "About", "Above", "After", "Again", "Against", "All", "Am", "An", "And", "Any", "Are", "Aren’t", "As", "At", "Be", "Because", "Been", "Before", "Being", "Below", "Between", "Both", "But", "By", "Can’t", "Cannot", "Could",
		"Couldn’t", "Did", "Didn’t", "Do", "Does", "Doesn’t", "Doing", "Don’t", "Down", "During", "Each", "Few", "For", "From", "Further", "Had", "Hadn’t", "Has", "Hasn’t", "Have", "Haven’t", "Having",
		"He", "He’d", "He’ll", "He’s", "Her", "Here", "Here’s", "Hers", "Herself", "Him", "Himself", "His", "How", "How’s", "I ", " I", "I’d", "I’ll", "I’m", "I’ve", "If", "In", "Into", "Is",
		"Isn’t", "It", "It’s", "Its", "Itself", "Let’s", "Me", "More", "Most", "Mustn’t", "My", "Myself", "No", "Nor", "Not", "Of", "Off", "On", "Once", "Only", "Ought", "Our", "Ours", "Ourselves",
		"Out", "Over", "Own", "Same", "Shan’t", "She", "She’d", "She’ll", "She’s", "Should", "Shouldn’t", "So", "Some", "Such", "Than",
		"That", "That’s", "Their", "Theirs", "Them", "Themselves", "Then", "There", "There’s", "These", "They", "They’d", "They’ll", "They’re", "They’ve",
		"This", "Those", "Through", "To", "Too", "Under", "Until", "Up", "Very", "Was", "Wasn’t", "We", "We’d", "We’ll", "We’re", "We’ve",
		"Were", "Weren’t", "What", "What’s", "When", "When’s", "Where", "Where’s", "Which", "While", "Who", "Who’s", "Whom",
		"Why", "Why’s", "With", "Won’t", "Would", "Wouldn’t", "You", "You’d", "You’ll", "You’re", "You’ve", "Your", "Yours", "Yourself", "Yourselves"
		};*/
	
	
}