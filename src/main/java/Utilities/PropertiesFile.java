package Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.support.ui.WebDriverWait;

public class PropertiesFile extends BaseDriver {

	public static WebDriverWait wait;
	public static String UserDir = System.getProperty("user.dir");
	public static String browser;
	public static String url, dlpName, websiteURL, LMSURL,pcatUrl,accesscodeadminTool,hdToolURL,emailID, ccnumber, ccexpirydate;
	public static String DriverPath;
	//SS Strings  Definitions
	public static String salesTax, totalpriceUS, totalpriceprinted,ebook_salestax, firstname, lastname,ebook_totalprice;
	public static String invalid_registrationcode, valid_registrationcode, used_registrationcode, invalid_legacy_code, valid_legacy_code;
	public static String EmailText;
	static Properties prop = new Properties();

	// Read Browser name, Test url and Driver file path from config.properties.

	public static void loadProperties() throws IOException {
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir
					+ "\\resources\\config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop.load(input);
	}

	public static void readPropertiesFile() throws Exception {

		loadProperties();
		url = prop.getProperty("ecpURL");
		dlpName = prop.getProperty("dlpName");
		DriverPath = UserDir + "\\Drivers\\";
		websiteURL =prop.getProperty("websiteurl");
		pcatUrl =prop.getProperty("pcatURL");
		accesscodeadminTool = prop.getProperty("accesscodeAdmintool");
		hdToolURL = prop.getProperty("hdTool");
       //SS called the property values 
		emailID = prop.getProperty("emailid");
		ccnumber = prop.getProperty("ccnumber");
		ccexpirydate = prop.getProperty("ccexpirydate");
		EmailText = prop.getProperty("EmailSubject");
		salesTax = prop.getProperty("SalesTax");
		ebook_salestax = prop.getProperty("EbookSalesTax");
		ebook_totalprice = prop.getProperty("EbookTotalPrice");
		invalid_registrationcode = prop.getProperty("InvalidRegistrationCode");
		valid_registrationcode  = prop.getProperty("ValidRegistrationCode");
		used_registrationcode = prop.getProperty("UsedRegistrationCode");
		invalid_legacy_code = prop.getProperty("InvalidLegacyCode");
		valid_legacy_code = prop.getProperty("ValidLegacyCode");
		firstname = prop.getProperty("FirstName");
		lastname = prop.getProperty("LastName");
		
	}

	// Set Browser configurations by comparing Browser name and Diver file path.
	// @Parameters({"browser"})
	public static void setBrowserConfig(String browser)
			throws IllegalAccessException, IOException {
		loadProperties();
		browser = prop.getProperty("browsername");
		BaseDriver bd = new BaseDriver();
		bd.init_driver(browser);

	}

	// Set Test URL based on config.properties file.

	public static void setURL(String dlpTaxonomyName) throws InterruptedException {
		if (dlpTaxonomyName.isEmpty()) {
			getDriver().manage().deleteAllCookies();
			getDriver().manage().timeouts().getPageLoadTimeout();
					
			getDriver().get(url + dlpName);
			ReusableMethods.checkPageIsReady(getDriver());
		} else {
			getDriver().manage().deleteAllCookies();
			getDriver().manage().timeouts().getPageLoadTimeout();
					
			getDriver().get(url + dlpTaxonomyName);
			ReusableMethods.checkPageIsReady(getDriver());
		}
	}
	
	public static String disciplineValue() throws InterruptedException {	
			String disciplineName = prop.getProperty("disciplineValue");
			return disciplineName;
		
	}
	public static String productCode() throws InterruptedException {	
		String productName = prop.getProperty("ProductCode");
		return productName;
	
}

	// get WebSite URL
	public static String webSiteURL() {
		return websiteURL;
	}
	public static String nciaURL() {
		return url;
	}
	// get LMS URL
	public static void setlmsURL() {

		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().getPageLoadTimeout();
		getDriver().get(LMSURL);
	}

	// Close the driver after running test script.

	public static void tearDownTest() throws InterruptedException {

		getDriver().manage().timeouts().getPageLoadTimeout();
		getDriver().manage().deleteAllCookies();
		getDriver().quit();

	}
	public static void setPcatURL() {
		// driver.manage().window().maximize();
		getDriver().get(pcatUrl);

	}
	public static void setCanNewURL(String newURL) {
		// driver.manage().window().maximize();
		getDriver().get(newURL);

	}
	
	public static void setHDToolURL() {
		// driver.manage().window().maximize();
		getDriver().get(hdToolURL);

	}
	public static void setAccessCodeAdminToolURL() {
		// driver.manage().window().maximize();
		getDriver().get(accesscodeadminTool);

	}
	
	public static void setmailDropURL() {
		// driver.manage().window().maximize();
		getDriver().navigate().to("https://maildrop.cc/");

	}
	//SS Methods
	//SS
		public static String getExistingUserEmail() { 
			// driver.manage().window().maximize();
			//getDriver().get(emailID);
				return emailID;
		}
		
		//SS
		public static String getSalesTax() { 
		
					return salesTax;
		}
		
		//SS -totalpriceUS, totalpriceprinted;
		public static String getTotalPrice() {
			totalpriceUS = prop.getProperty("TotalPrice");
			return totalpriceUS;
			
		}
		
		//SS -totalpriceUS, totalpriceprinted;
		public static String getTotalPricePrinted() {
			totalpriceprinted = prop.getProperty("TotalPricePrinted");
			return totalpriceprinted;
			
		}
		
		
			//SS
		public static String getcreditecardnumber() {
			return ccnumber;
			}
		
		//SS
		public static String getCCexpirydate() {
			return ccexpirydate;
		}
		
		public static String getEmailText() {
					return EmailText; 
		}

		public static String getEbookSalestax() {
			return ebook_salestax; 
	}
		
		public static String getEbookTotalprice() {
			return ebook_totalprice; 
	}

			public static String getValidRegistrationcode() {
			return valid_registrationcode;
			}
		
		//SS
		public static String getInvalidRegistrationcode() {
			return invalid_registrationcode ;
		}
		
		public static String getUsedRegistrationcode() {
					return used_registrationcode ; 
		}

		public static String getinvalid_legacy_code () {
			return invalid_legacy_code ; 
	}
		
		public static String getvalid_legacy_code () {
			return valid_legacy_code ; 
	}
		
		public static String getExistingUserFirstName () {
			return firstname ; 
	}
		
		public static String getExistingUserLastName () {
			return lastname ; 
	}
}
