package Utilities;

import java.util.Random;

public class GetRandomId {
	
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	public static String randomAlphaNumeric(int count) {
	StringBuilder builder = new StringBuilder();
	while (count-- != 0) {
	int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
	builder.append(ALPHA_NUMERIC_STRING.charAt(character));
	}
	return builder.toString();
	}
	
	public static int getRandom(int max){
		int min;
		Random r = new Random();
		min =(int) max/2;
		int result =r.nextInt(max-min) + min;
		return result;
		
	}

}
