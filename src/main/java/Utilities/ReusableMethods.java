package Utilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.google.gson.JsonObject;




public class ReusableMethods {
	private static final String HTML_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
	private static Pattern pattern = Pattern.compile(HTML_PATTERN);

	public static void checkPageIsReady(WebDriver driver) throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		int i = 0;
		while (i != 10) {
			String state = (String) js.executeScript("return document.readyState");
			if (state.equals("complete")) {
				Thread.sleep(5000);
				break;
			} else {
				Thread.sleep(5000);
				i++;
			}
		}
		// waitForJQueryProcessing(driver,100);
	}

	public static boolean waitForJQueryProcessing(WebDriver driver, int timeOutInSeconds) {
		boolean jQcondition = false;
		try {
			new WebDriverWait(driver, Duration.ofSeconds(timeOutInSeconds)) {
			}.until(new ExpectedCondition<Boolean>() {

				@Override
				public Boolean apply(WebDriver driverObject) {
					return (Boolean) ((JavascriptExecutor) driverObject).executeScript("return jQuery.active == 0");
				}
			});
			jQcondition = (Boolean) ((JavascriptExecutor) driver)
					.executeScript("return window.jQuery != undefined && jQuery.active == 0");
			return jQcondition;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jQcondition;
	}

	public static boolean elementExist(WebDriver driver, String xpath) {
		try {
			driver.findElement(By.xpath(xpath));

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	public static boolean elementExist(WebDriver driver, By by) {
		try {
			driver.findElement(by);

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	public static void scrollToBottom(WebDriver driver) {
		try {
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToBottomEle(WebDriver driver, WebElement element) {
		try {
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToElement(WebDriver driver, By by) {
		try {
			WebElement element = driver.findElement(by);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public static void scrollIntoView(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoViewByWindowHeight(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true); window.scrollBy(0, -window.innerHeight / 2);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoViewTOC(WebDriver driver, WebElement webElement) {
		driver.findElement(By.id("controls-panel")).click();
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoViewListElements(WebDriver driver, List<WebElement> webElement) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByXY(WebDriver driver, int xOffset, int yOffset) {
		try {
			String script = "scroll(" + xOffset + ", " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByX(WebDriver driver, int xOffset) {
		try {
			String script = "scroll(" + xOffset + ", 0)";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollByY(WebDriver driver, int yOffset) {
		try {
			String script = "scroll(0, " + yOffset + ")";
			((JavascriptExecutor) driver).executeScript(script);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void waitForPageLoad(WebDriver driver) {
		JavascriptExecutor j = (JavascriptExecutor) driver;
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		for (int i = 0; i < 25; i++) {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
			wait.until(pageLoadCondition);
			if (j.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
	}

	public static void WaitVisibilityOfElement(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void WaitElementClickable(WebDriver driver, By by) {
		WebDriverWait Wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		Wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void clickElement(WebDriver driver, WebElement Element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", Element);
	}

	public static void loadingScreenWaitDisapper(WebDriver driver, By by) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		WebElement loader = driver.findElement(
				By.xpath("//div[contains(@class,'loading-container anim')]/div[contains(@class,'loading-copy')]"));
		wait.until(ExpectedConditions.visibilityOf(loader)); // wait for loader
																// to appear
		wait.until(ExpectedConditions.invisibilityOf(loader)); // wait for
																// loader to
																// disappear
	}

	public static void spinnerWaitDisapper(WebDriver driver) {
		boolean isspinner = driver
				.findElements(By.xpath("//div[starts-with(@class,'frame-container')]/div[@class='spinner']"))
				.size() > 0;
		if (isspinner == true) {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1000L));
			WebElement loader = driver
					.findElement(By.xpath("//div[starts-with(@class,'frame-container')]/div[@class='spinner']"));
			try {
				wait.until(ExpectedConditions.visibilityOf(loader)); // wait for
																		// loader
																		// to
																		// appear
				wait.until(ExpectedConditions.invisibilityOf(loader)); // wait
																		// for
																		// loader
																		// to
																		// disappear
			} catch (StaleElementReferenceException e) {
				// TODO Auto-generated catch block
				wait.until(ExpectedConditions.visibilityOf(loader)); // wait for
																		// loader
																		// appear
				wait.until(ExpectedConditions.invisibilityOf(loader)); // wait
																		// for
																		// loader
																		// to
																		// disappear
			}

		}
	}

	// using StringBuilder.append()
	public static String convertArrayToStringMethod(String[] strArray) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < strArray.length; i++) {
			stringBuilder.append(strArray[i]);
		}
		return stringBuilder.toString();
	}

	// Using String.join() method
	public static String convertArrayToStringUsingStreamAPI(String[] strArray) {
		String joinedString = String.join(" ", strArray);
		String replaced = joinedString.replace("[", "").replace("]", "");
		System.out.println(replaced);
		return replaced;
	}

	public static boolean scroll_Page(WebDriver driver, WebElement webelement, int scrollPoints) {
		try {
			Actions dragger = new Actions(driver);
			// drag downwards
			int numberOfPixelsToDragTheScrollbarDown = 10;
			for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown) {
				dragger.moveToElement(webelement).clickAndHold().moveByOffset(0, numberOfPixelsToDragTheScrollbarDown)
						.release(webelement).build().perform();
			}
			Thread.sleep(500);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void scrollSlowly(WebDriver driver, WebElement element) {
		for (int i = 0; i < 1000; i++) {
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,1)", "");
			if (element.isDisplayed()) {
				break;
			} else {
				continue;
			}

		}
	}

	public static String waitTillTextPresent(WebDriver driver, By by, String text) {

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
		return text;

	}

	// get HTML tags from a String

	public static boolean hasHTMLTags(String text) {
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}

	public static void pressKeyTab(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).build().perform();
		action.sendKeys(Keys.RETURN).build().perform();
	}

	public static void switchToNewWindow(int windowNumber, WebDriver driver) {
		Set<String> s = driver.getWindowHandles();
		Iterator<String> ite = s.iterator();
		int i = 1;
		while (ite.hasNext() && i < 10) {
			String popupHandle = ite.next().toString();
			driver.switchTo().window(popupHandle);
			LogUtil.log("Window title is : " + driver.getTitle());
			if (i == windowNumber)
				break;
			i++;
		}
	}

	public static List<String> switchToNewTAB(WebDriver driver) {
		driver.switchTo().newWindow(WindowType.TAB);
		Set<String> handles = driver.getWindowHandles();
		List<String> ls = new ArrayList<>(handles);
		return ls;
	}

	public static void getWindowHandle(WebDriver driver) throws InterruptedException {
		Set<String> originalWindow = driver.getWindowHandles();
		List<String> browserstab = new ArrayList<String>(originalWindow);
		int windowCount = originalWindow.size();
		if (windowCount == 2) {
			driver.switchTo().window(browserstab.get(1));
			Thread.sleep(3000);
			driver.close();
			driver.switchTo().window(browserstab.get(0));
		} else {
			driver.switchTo().window(browserstab.get(0));
			driver.navigate().back();
		}

	}

	public static void closeCurrentWindow(int windowNumber, WebDriver driver) {
		Set<String> s = driver.getWindowHandles();
		Iterator<String> ite = s.iterator();
		int i = 1;
		while (ite.hasNext() && i < 10) {
			String popupHandle = ite.next().toString();
			driver.switchTo().window(popupHandle);
			LogUtil.log("Window title is : " + driver.getTitle());
			if (i == windowNumber) {
				Actions action = new Actions(driver);
				action.sendKeys(Keys.chord(Keys.CONTROL, "w")).build().perform();
				i++;
			}
		}
	}
	
	
	public static void closeCurrentWindowTab(String newWindow, WebDriver driver) {
		Set<String> handles = driver.getWindowHandles();
		for(String cWindow : handles ) {
			if(!cWindow.equalsIgnoreCase(newWindow)) {
				driver.switchTo().window(newWindow);
				driver.close();
				driver.switchTo().window(cWindow);
			}
		}
	}

	public static void closeCurrentTab(int windowNumber, WebDriver driver) {
		Set<String> s = driver.getWindowHandles();
		List<String> browserstab = new ArrayList<String>(s);
		driver.switchTo().window(browserstab.get(windowNumber));
		driver.close();
		driver.switchTo().window(browserstab.get(0));
	}

	public static String replacesquarebrackets(String str) {
		return str = str.replaceAll("\\[", "").replaceAll("\\]", "");
	}

	public static void switchToFrame(WebDriver driver, String frameName) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameName));
		driver.switchTo().frame(frameName);

	}

	public static void switchToFrameByIndex(WebDriver driver, int index) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(index));
		driver.switchTo().frame(index);

	}

	public static void switchToFramebyElement(WebDriver driver, WebElement element) {
		driver.switchTo().frame(element);

	}

	public static void switchToDefaultContent(WebDriver driver) {
		driver.switchTo().defaultContent();

	}

	public static String removeAsciiChar(String str) {
		return str = str.replaceAll("[^\\p{ASCII}]", "");

	}

	// state element exception
	public static boolean retryElementClick(By by, WebDriver driver) {
		int attempts = 0;
		boolean result = false;
		while (attempts < 2) {
			try {
				driver.findElement(by).click();
				return true;
			} catch (StaleElementReferenceException e) {

			}
			attempts++;

		}
		return result;

	}

	// Robot class to Click Tab Key
	public static void clickTABKeyRobot() throws AWTException {
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_TAB);
		r.keyRelease(KeyEvent.VK_TAB);
	}

	public static void clickTABkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).build().perform();
	}

	public static void clickENTERkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
	}

	public static void clickTABSHIFTkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).sendKeys(Keys.SHIFT).build().perform();
	}

	public static void clickSHIFTTABkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
	}

	public static void clickCtrlAkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0061')).build().perform();
	}

	public static void clickCtrlCkey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL).sendKeys(String.valueOf('\u0043')).build().perform();
	}

	public static void clickArrowUpKey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_UP).build().perform();
	}

	public static void clickArrowDownKey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_DOWN).build().perform();
	}

	public static void clickArrowRightKey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_RIGHT).build().perform();
	}

	public static void clickArrowLeftKey(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ARROW_LEFT).build().perform();
	}

	public static void clickSpaceBar(WebDriver driver) {
		Actions action = new Actions(driver);
		action.sendKeys(Keys.SPACE).build().perform();
	}

	public static void openNewTab(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank');");
	}

	public static void closeDialog(WebDriver driver) throws AWTException {
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ESCAPE);
		r.keyRelease(KeyEvent.VK_ESCAPE);
	}

	public static JsonObject jsondataReader() {
		ReadUIJsonFile readJasonObject = new ReadUIJsonFile();
		JsonObject jsonobject = readJasonObject.readUIJson();
		return jsonobject;
	}

	public static int getDigits(String text) {
		String str = text.replaceAll("[^0-9]", "");
		int v = Integer.parseInt(str);
		return v;

	}

	public static float getDecimalDigits(String text) {
		String str = text.replaceAll("[^\\d.]+|\\.(?!\\d)", "");
		float v = Float.parseFloat(str);
		return v;

	}

	public static String getNNumberofWords(String str, int n) {
		String firstStrs = null;
		String[] sArry = str.split(" ");
		for (int i = 3; i < n; i++) {
			firstStrs += sArry[i] + " ";
		}
		return firstStrs.trim();
	}

	public static String getNortonianNumberofWords(String str) {
		String firstStrs = null;
		String[] sArry = str.split(" ");
		for (int i = 3; i < sArry.length; i++) {
			firstStrs += sArry[i] + " ";
		}
		return firstStrs.trim();
	}

	// Open Notepad
	public static String openNotePad() throws IOException {
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec("C:\\Windows\\notepad.exe");
		return process.toString();
	}

	public static int countWords(String str) {
		int count = 1;

		for (int i = 0; i < str.length() - 1; i++) {
			if ((str.charAt(i) == ' ') && (str.charAt(i + 1) != ' ')) {
				count++;
			}
		}
		return count;
	}

	public static WebElement expandRootElement(WebElement element, WebDriver driver) {
		WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",
				element);
		return ele;
	}

	public static void createUser(int i, String emailID) {
		if (i > 0) {
			emailID = "John" + "_" + "Simpson" + GetRandomId.randomAlphaNumeric(30).toLowerCase() + "@mailinator.com";
		} else {
			emailID = "John" + "_" + "Simpson" + GetRandomId.randomAlphaNumeric(3).toLowerCase() + "@mailinator.com";
		}

	}

	public static String getStringparenthesis(String value) {
		String matcherValue = null;
		Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(value);
		while (m.find()) {
			LogUtil.log(m.group(1));
			matcherValue = m.group(1);
		}
		return matcherValue;

	}

    public static Object getPCATJSONData(String json, String key1, String key2) throws ParseException {
    	String sjobj = null;
    	JSONParser parser = new JSONParser();
    	JSONObject jobj = null;
    	jobj = (JSONObject) parser.parse(json);
    	Object objValue  = jobj.get(key1);
    	if(objValue instanceof JSONObject && key2!=null) {
    		sjobj =((JSONObject) objValue).get(key2).toString();
        	System.out.println(sjobj);
        	return sjobj;
        	
    	} 
    	
		return objValue;
    	
    }
	


	public static String[] getSysDate() {
		DateFormat df = new SimpleDateFormat("MM yy"); // Just the year, with 2 digits
		String formattedDate = df.format(Calendar.getInstance().getTime());
		String[] myStr = formattedDate.split(" ");
		return myStr;

	}

	public static String[] getUSASysDate() {
		DateFormat df = new SimpleDateFormat("MM yyyy"); // Just the year, with 2 digits
		String formattedDate = df.format(Calendar.getInstance().getTime());
		String[] myStr = formattedDate.split(" ");
		return myStr;

	}
	
	
	
}