package Utilities;

import org.testng.asserts.SoftAssert;

public class CustomAssertion extends SoftAssert{
	
	public void assertContains(String actual, String expected){
		if(actual.contains(expected)){
			LogUtil.log("The Assertion is passed" );
		} else {
			throw new AssertionError("Text Contains NOT matches, Assertion Failed");
		}
	}

}
