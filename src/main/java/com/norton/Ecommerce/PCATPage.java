package com.norton.Ecommerce;

import io.qameta.allure.Step;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;

import Utilities.BaseDriver;
import Utilities.GetRandomId;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;

public class PCATPage {

	WebDriver driver;
	public static String Inst_Email;
	public static String Inst_Password;
		

	@FindBy(how = How.ID, using = "register_login_choice_login")
	public WebElement SignIn_Pcat;
	
	@FindBy(how = How.NAME, using = "register_login_choice")
	public List<WebElement> oRadiobutton;

	@FindBy(how = How.ID, using = "username")
	public WebElement username_Pcat;

	@FindBy(how = How.ID, using = "password")
	public WebElement password_Pcat;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton_Pcat;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']")
	public WebElement gearButton_Username;

	@FindBy(how = How.XPATH, using = "//ul[@id='gear_menu']/li/a[contains(text(),'Authorize an Instructor Account')]")
	public WebElement Authorize_Instructor_Account;

	@FindBy(how = How.ID, using = "authorize_instructor_first_name")
	public WebElement Authorize_Instructor_FirstName;

	@FindBy(how = How.ID, using = "authorize_instructor_last_name")
	public WebElement Authorize_Instructor_LastName;

	@FindBy(how = How.ID, using = "authorize_instructor_email")
	public WebElement Authorize_Instructor_Email;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Authorize Instructor')]")
	public WebElement Authorize_Instructor_Button;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-content ui-widget-content']/b[1]")
	public WebElement Instructor_Email;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-content ui-widget-content']/b[2]")
	public WebElement Instructor_Password;

	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-content ui-widget-content']/br[1]")
	public WebElement Instructor_authorization_text;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'Done')]")
	public WebElement Instructor_Done_Button;

	@FindBy(how = How.XPATH, using = "//li[@role='presentation']/a[@role='menuitem']/b[contains(text(),'Sign Out')]")
	public WebElement PcatSignOut_link;
	
	@FindBy(how = How.XPATH, using = "//ul[@id='gear_menu']/li/a/b[contains(text(),'Sign Out')]")
	public WebElement signoutAdminpage;

	@FindBy(how = How.XPATH, using = "//ul[@id='gear_menu']/li/a[starts-with(text(),'Go to NCIA Entitlements')]")
	public WebElement NciaEntitlementsMenu;

	@FindBy(how = How.ID, using = "filter_student")
	public WebElement userNameTextbox;

	@FindBy(how = How.ID, using = "search_legacy")
	public WebElement searchlegacycheckbox;

	@FindBy(how = How.ID, using = "exclude_test_accounts")
	public WebElement excludetestaccountscheckbox;

	@FindBy(how = How.XPATH, using = "//button[@id='run_report_button']/span[contains(text(),'GO!')]")
	public WebElement GoButton;

	@FindBy(how = How.XPATH, using = "//table[@id='pert_table']/tbody/tr/td/div/a")
	public WebElement Usernamelink;

	@FindBy(how = How.XPATH, using = "//button[@title='close']")
	public WebElement closeUserDetailsection;
	
	@FindBy(how = How.ID, using = "collections_0_select")
	public WebElement selectDiscipline;
	
	@FindBy(how = How.ID, using = "collections_1_select")
	public WebElement selectProduct;
	
	@FindBy(how = How.ID, using = "ec_row_edit_notes")
	public WebElement NotesTextbox;
	
	@FindBy(how = How.ID, using = "ec_row_edit_email")
	public WebElement Emailtextbox;
	
	@FindBy(how = How.ID, using = "generate_codes_button")
	public WebElement generateAccCodeButton;
	
	@FindBy(how = How.ID, using = "generated_code_textarea")
	public WebElement accessCodeValue;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']/button[@type='button']/span[contains(text(),'OK')]")
	public WebElement okButton;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'alert_dialog_outer_')]/following::div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(.,'OK')]")
	public WebElement entlokButton;
	
	@FindBy(how = How.ID, using = "access_code_input")
	public WebElement accessCodeInput;
	
	@FindBy(how = How.XPATH, using = "//button[@id='access_code_check']")
	public WebElement checkCodebutton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='show_code_redeem_btn']/span[contains(text(),'Authorize Additional Students')]")
	public WebElement authAddStudents;
	
	@FindBy(how = How.XPATH, using = "//div/textarea[@id='code_redeem_ta']")
	public WebElement emailtextarea;
	
	@FindBy(how = How.ID, using = "school_type_select")
	public WebElement selectSchool;
	@FindBy(how = How.ID, using = "state_select")
	public WebElement selectState;	
	@FindBy(how = How.ID, using = "school_text_input")
	public WebElement enterSchoolName;	
	@FindBy(how = How.XPATH, using = "//button[@id='code_redeem_btn']/span[contains(text(),'Authorize Students')]")
	public WebElement authtudentsbutton;
	
	@FindBy(how = How.XPATH, using = "//div[@id='general_dialog_outer_1']//ul/li[contains(text(),'rights')]")
	public WebElement rightsElement;
	
	@FindBy(how = How.XPATH, using ="//table[@id='student_details_ent_table']/tbody/tr/td[@class[substring(.,string-length(.)-string-length('pert_nobr') +1)='pert_nobr']]")
	public List<WebElement> isbnEle;
	
	@FindBy(how = How.XPATH, using ="//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')]")
	public List<WebElement> pertCenterEle;
	
	@FindBy(how = How.XPATH, using ="//table[@id='entitlement_summary_table']/tbody/tr[not(contains(@class,'not_purchaseable'))]//td/div[@class='entitlement_summary_sortable_handle']")
	public List<WebElement> entitlementsummaryPrice;
	
	@FindBy(how = How.XPATH, using ="//table[@id='entitlement_summary_table']/tbody/tr[not(contains(@class,'not_purchaseable'))]//td/div[starts-with(@style,'display:inline-block;')]")
	public List<WebElement> entitlementsummaryisbnDetails;
	
	@FindBy(how = How.XPATH, using ="//tr[not(contains(@class,'not_purchaseable'))]/td/button[contains(@class,'entitlement_edit_row_button')]")
	public List<WebElement> entitlementEditButton;
	
	@FindBy(how = How.XPATH, using ="//tr/td[contains(.,'Canadian Price override for DLP:')]/following-sibling::td")
	public WebElement canadaunit;
	
	@FindBy(how = How.XPATH, using ="//tr/td[contains(.,'Canadian Price override for DLP:')]/following-sibling::td/input")
	public WebElement canadaPrice;
	
	@FindBy(how = How.ID, using ="edit_row_button_cancel")
	public WebElement cancelButton;
	
	@FindBy(how = How.ID, using ="edit_row_button_save")
	public WebElement saveButton;
	
	@FindBy(how = How.XPATH, using ="//table[contains(@id,'student_details_ent_table')]/tbody/tr/td[contains(@class,' pert_indent')]")
	public WebElement productCodeDescription;
	
	@FindBy(how = How.XPATH, using ="//table[@id='student_details_ent_table']/tbody/tr/td[@class[substring(.,string-length(.)-string-length('pert_center') +1)='pert_center']]")
	public List<WebElement> purchaseandPrice;
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using="details")
	public WebElement detailslink;
	
	@FindBy(how = How.XPATH, using ="//table[contains(@id,'student_details_ent_table')]/tbody/tr/td[contains(@class,'pert_nobr_center sorting')]")
	public WebElement dateValue;
		
	@FindBy(how = How.XPATH, using ="//div[contains(@id,'alert_dialog_outer_')]/b[contains(.,'Entitlement Details:')]/following::pre")
	public WebElement entlDetailsJson;
	
	ReadUIJsonFile readUserData = new ReadUIJsonFile();
	JsonObject obj = readUserData.readUIJson();

	public PCATPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	public PCATPage(String InstEmail, String InstPassword) {
		PCATPage.Inst_Email = InstEmail;
		PCATPage.Inst_Password = InstPassword;
	}

	
	// Navigate to PCAT URL

	@Step("Navigate to Pcat application,  Method: {method} ")
	public void navigatePCAT() {
		PropertiesFile.setPcatURL();
	}

	@Step("Navigate to Pcat application,  Method: {method} ")
	public void navigateAdmintool() {
		PropertiesFile.setAccessCodeAdminToolURL();
	}
	// Allure Steps Annotations and Methods
	@Step("Login to PCAT,  Method: {method} ")
	public void loginto_Pcat() {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login_dialog")));
		for(int i=0; i<oRadiobutton.size(); i++){
			String selectRbutton=oRadiobutton.get(i).getDomAttribute("id");
			if(selectRbutton.contains("register_login_choice_login")){
				oRadiobutton.get(i).click();
			}
			
		}
		String UserName = obj.getAsJsonObject("PCAT_LOGIN").get("Pcatloginemail").getAsString();
		//String UserName = "spatil@wwnorton.com";
		username_Pcat.clear();
		username_Pcat.sendKeys(UserName);
		password_Pcat.clear();
		password_Pcat.sendKeys(obj.getAsJsonObject("PCAT_LOGIN").get("Pcatloginpassword").getAsString());
		SignInButton_Pcat.click();
		try {
			ReusableMethods.checkPageIsReady(driver);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*wait.until(ExpectedConditions.visibilityOfElementLocated(By
				.linkText("NERT")));
		driver.findElement(By.linkText("NERT")).click();*/

	}
	
	@Step("Login to NCIA Admin tool,  Method: {method} ")
	public void logintoNCIAAdmin() {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(SignIn_Pcat));
		Actions PcatIn = new Actions(driver);
		PcatIn.moveToElement(SignIn_Pcat);
		PcatIn.click();
		PcatIn.perform();
	    String UserName = "spatil@wwnorton.com";
		username_Pcat.clear();
		username_Pcat.sendKeys(UserName);
		password_Pcat.clear();
		password_Pcat.sendKeys("TableTop123");
		SignInButton_Pcat.click();
		try {
			ReusableMethods.checkPageIsReady(driver);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

	@Step("Click the gearButton_Username link,  Method: {method} ")
	public void clickgearMenu() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(gearButton_Username));
		//ReusableMethods.scrollIntoView(driver, gearButton_Username);
		gearButton_Username.click();
	}

	@Step("Click theGo to NCIA Entitlements Setup Tool (NEST),  Method: {method} ")
	public void clickNCIAEntMenu() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(NciaEntitlementsMenu));
		NciaEntitlementsMenu.click();
	}
	
	@Step("Select the Discipline from the List box ,  Method: {method} ")
	public void selectDiscpline(String discipline) throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(selectDiscipline));
		Select dis = new Select(selectDiscipline);
		dis.selectByVisibleText(discipline);
	}
	
	@Step("Select the Product from the List box ,  Method: {method} ")
	public void selectProduct(String product) throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(selectProduct));
		Select dis = new Select(selectProduct);
		dis.selectByVisibleText(product);
	}
	
	@Step("Click the Generate Access Code for All ,  Method: {method} ")
	public void generateAccessCodebutton() throws InterruptedException{
		
		ReusableMethods.checkPageIsReady(driver);
		List<WebElement> generateAcccodeEle = driver.findElements(By.xpath("//table[@id='entitlement_summary_table']/tbody[@class='ui-sortable']/tr/td/button[starts-with(@class,'entitlement_access_codes_button')]"));
		for(int i=0; i<generateAcccodeEle.size(); i++){
		boolean isbuttonEnabled =generateAcccodeEle.get(i).isEnabled();
		if(isbuttonEnabled==true){
			generateAcccodeEle.get(i).click();
			break;
		}
		}
	}
	
	@Step("Click the Generate Access Code for Specific Product ,  Method: {method} ")
	public void generateAccessCodebasedTileName(String prodName) throws InterruptedException{
		
		ReusableMethods.checkPageIsReady(driver);
		List<WebElement> generateAcccodeEle = driver.findElements(By.xpath("//table[@id='entitlement_summary_table']/tbody/tr/td/div[contains(.,'"+prodName+"')]/following::button[@role='button']/span[starts-with(.,'Generate')]"));
		for(int i=0; i<generateAcccodeEle.size(); i++){
		boolean isbuttonEnabled =generateAcccodeEle.get(i).isEnabled();
		if(isbuttonEnabled==true){
			generateAcccodeEle.get(i).click();
			break;
		}
		}
	}
	
	@Step("Generate Access Code ,  Method: {method} ")
	public String getAccessCode(){
		String getAccessCode=null;
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		NotesTextbox.sendKeys("tests");
		Emailtextbox.clear();
		generateAccCodeButton.click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		getAccessCode = accessCodeValue.getText();
		okButton.click();
		
		return getAccessCode;
	}

	@Step("Click the Authorize an Instructor Account link,  Method: {method} ")
	public void authorize_Instructor_Account_link() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions
				.visibilityOf(Authorize_Instructor_Account));
		Authorize_Instructor_Account.click();
	}

	@Step("Enter an Authorize Instructor details,  Method: {method} ")
	public void authorize_Instructor_Account() throws Exception {
		String FirstName = "john";
		String LastName = "mercey";

		Authorize_Instructor_FirstName.sendKeys(FirstName);
		Authorize_Instructor_LastName.sendKeys(LastName);
		// String Auth_Inst_Email =
		// jsonobject.getAsJsonObject("Authorize_Instructor").get("Authorize_Instructor_Email").getAsString();
		String EmailID = FirstName + "_" + LastName
				+ GetRandomId.randomAlphaNumeric(3).toLowerCase()
				+ "@mailinator.com";
		Authorize_Instructor_Email.sendKeys(EmailID);
		Authorize_Instructor_Button.click();
	}

	@Step("get the Authorize instructor details,  Method: {method} ")
	public PCATPage authorize_Instructor_getText() throws Exception {
		WebDriverWait wait  = new WebDriverWait(driver, Duration.ofSeconds(10));	
		wait.until(ExpectedConditions.visibilityOf(Instructor_Email));
		Inst_Email = Instructor_Email.getText();
		boolean isPassword = driver.findElements(By.xpath("//div[@class='ui-dialog-content ui-widget-content']/b[2]")).size() >0;
		if(isPassword==true){
		Inst_Password = Instructor_Password.getText();
		return new PCATPage(Inst_Email, Inst_Password);
		}
		return new PCATPage(Inst_Email,null);

	}

	@Step("Enter an userNameon NCIA Entitlements Reporting Tool (NERT),  Method: {method} ")
	public void setUserName(String UserName) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(userNameTextbox));
		userNameTextbox.sendKeys(UserName);
	}

	@Step("User has unchecked the checkboxes  Include legacy “Registration Actions” records and  Exclude test accounts,  Method: {method} ")
	public void uncheck() throws InterruptedException {
		Thread.sleep(5000);
		boolean checked = searchlegacycheckbox.isSelected();
		if (checked == true) {
			searchlegacycheckbox.click();
		}
		boolean excludechecked = excludetestaccountscheckbox.isSelected();
		if (excludechecked == true) {
			excludetestaccountscheckbox.click();
		}
	}

	@Step("Click Go! button,  Method: {method} ")
	public void clickGo() {
		GoButton.click();
	}
	
	@Step("Click OK Button,  Method: {method} ")
	public void clickEntitlementOKbutton() {
		try {
			entlokButton.click();
		} catch (ElementClickInterceptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Step("Click OK Button,  Method: {method} ")
	public void clickOKbutton() {
		try {
			okButton.click();
		} catch (ElementClickInterceptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Step("Click UserName link from the row,  Method: {method} ")
	public void clickUsernamelink() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Usernamelink));
		Usernamelink.click();
	}

	@Step("User Role/NCIA Rights for  new account is set as ‘Student-1,  Method: {method} ")
	public String getuserRole() {
		String getroleText = null;
		boolean elementNciarights = driver
				.findElements(
						By.xpath("//div[@id='general_dialog_outer_1']//ul/li[contains(text(),'rights')]"))
				.size() > 0;
		if (elementNciarights == true) {
			getroleText = rightsElement.getText();
		}

		return getroleText;
	}

	@Step("Verify the access code on User details entitlement table,  Method: {method} ")
	public void getaccesscodeentTable(String accesscode) {
		String getaccesscode = null;
		boolean elementaccode = driver
				.findElements(
						By.xpath("//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')]"))
				.size() > 0;
		if (elementaccode == true) {
			for (int i = 0; i < pertCenterEle.size(); i++) {
				getaccesscode = pertCenterEle.get(i).getText();
			}
			if (getaccesscode.contains(accesscode)) {
				String acccode = getaccesscode.split("\\[")[0];
				System.out.println(acccode);
				Assert.assertEquals(acccode.trim(), accesscode.trim());
			}
		}

	}

	@Step("Verify Entitlements type is purchase is displayed,  Method: {method} ")
	public String entitlementpurchase() {
		String eType = null;
		WebElement elegettext = driver
				.findElement(By
						.xpath("//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')][contains(text(),'purchase')]"));
		eType = elegettext.getText();
		return eType;

	}
	
	@Step("Verify Entitlements type is Site_License is displayed,  Method: {method} ")
	public String entitlementsitelicence() {
		String eType = null;
		WebElement elegettext = driver
				.findElement(By
						.xpath("//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')][contains(text(),'site_license')]"));
		eType = elegettext.getText();
		return eType;

	}

	@Step("Verify Entitlements type is trail access is displayed,  Method: {method} ")
	public String entitlementtrailaccess() {
		String eType = null;
		WebElement elegettext = driver
				.findElement(By
						.xpath("//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')][contains(text(),'trial_access')]"));
		eType = elegettext.getText();
		return eType;

	}
	
	@Step("Verify Entitlements type is access_code_admin is displayed,  Method: {method} ")
	public String entitlementaccessCodeAdmin() {
		String eType = null;
		WebElement elegettext = driver
				.findElement(By
						.xpath("//table[@id='student_details_ent_table']/tbody/tr/td[contains(@class,' pert_center')][contains(text(),'access_code_admin')]"));
		eType = elegettext.getText();
		return eType;

	}

	@Step("close the user details window,  Method: {method} ")
	public void closeUserDetails() {
		closeUserDetailsection.click();
	}

	@Step("Signout PCAT,  Method: {method} ")
	public void SignOut_Pcat() throws InterruptedException {
	boolean isdisplayBlock = driver.findElements(By.xpath("//ul[@id='gear_menu'][contains(@style,'display: none;')]")).size()>0;
	if(isdisplayBlock==false) {
	PcatSignOut_link.click();
	} else {
		gearButton_Username.click();
		Thread.sleep(2000);
		PcatSignOut_link.click();
	}
	
	}
	//Admin Tool features
	
	@Step("Enter the access code in accessCode Text box,  Method: {method} ")
	public void inputAccesscode(String accessCode){
		accessCodeInput.click();
		accessCodeInput.sendKeys(accessCode);
	}
	
	@Step("click the Check Code Button,  Method: {method} ")
	public void clickCheckCodebutton(){
		checkCodebutton.click();
	}
	
	@Step("click Authorize Additional Students  Button,  Method: {method} ")
	public void clickAuthAddStudents(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(authAddStudents));
		authAddStudents.click();
	}
	
	@Step("Enter the Student informations,  Method: {method} ")
	public void enterStudentInfo(String email, String fName, String Lname){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(emailtextarea));
		emailtextarea.click();
		emailtextarea.sendKeys(email,",",Lname,",",fName);
	}
	
	@Step("Select the School name from the List box ,  Method: {method} ")
	public void selectSchoolName() {
		Select scName = new Select(selectSchool);
		scName.selectByValue("COLLEGE");
	}
	
	@Step("Select the state name from the List box ,  Method: {method} ")
	public void selectstateName() {
		Select stateName = new Select(selectState);
		stateName.selectByValue("USA_AL");
	}
	
	@Step("Enter the School Information ,  Method: {method} ")
	public void enterSchoolInfo() throws InterruptedException {
		enterSchoolName.click();
		WebElement schoolName = driver.findElement(By.xpath("//input[@id='school_text_input']"));
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].value='ab';", schoolName);
	//	enterSchoolName.sendKeys("Alabama");
		Thread.sleep(3000);
		boolean isListdisplayed = driver.findElements(By.xpath("//div[@id='school_text_div']/ul[starts-with(@id,'ui-id-')]/li")).size() >0;
		if(isListdisplayed==true){
			List<WebElement> schoolNameList = driver.findElements(By.xpath("//div[@id='school_text_div']/ul[starts-with(@id,'ui-id-')]/li"));
			for(int i=0; i<schoolNameList.size();){
				schoolNameList.get(0).click();
				break;
			}
		}
		enterSchoolName.click();
	}
	
	@Step("click Authorize Students Button ,  Method: {method} ")
	public void clickAuthStudent() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(authtudentsbutton));
		authtudentsbutton.click();
	}
	
	@Step("Capture the Price from the Entitlement/Purchase Configuration for Standard Product, Method: {method}")
	public List<String> capturePrice(){
		ArrayList<String> pricelist = new ArrayList<String>();
		for(int i=0; i<entitlementsummaryPrice.size(); i++) {
			pricelist.add(entitlementsummaryPrice.get(i).getText());
		}
		return pricelist;
	}
	
	@Step("Capture the ISBN details from the Entitlement/Purchase Configuration for Standard Product, Method: {method}")
	public List<String> captureisbndetails(){
		ArrayList<String> isbnlist = new ArrayList<String>();
		for(int i=0; i<entitlementsummaryisbnDetails.size(); i++) {
			isbnlist.add(entitlementsummaryisbnDetails.get(i).getText());
		}
		return isbnlist;
	}
	@Step("LogOut the PCAT Application,  Method: {method} ")
	public void logOutPCAT() throws InterruptedException {
		ReusableMethods.scrollIntoView(driver, gearButton_Username);
		gearButton_Username.click();
		Thread.sleep(2000);
		PcatSignOut_link.click();
		Thread.sleep(5000);
	}
	
	@Step("Click Edit Button and get the Canadain Price,  Method: {method} ")
	public List<String> captureCADPrice(){
		ArrayList<String> pricelist = new ArrayList<String>();
		for(int i=0; i<entitlementEditButton.size(); i++) {
			entitlementEditButton.get(i).click();
			String unit = canadaunit.getText().replaceAll(" ", "");
			String price = canadaPrice.getAttribute("value");
			StringBuilder newStr =new StringBuilder(unit);
			newStr.insert(1, price);
			pricelist.add(newStr.toString());
			cancelButton.click();
		}
		return pricelist;
	}
	
	public String originalCADPrice(){
		String price = null;
		for(int i=0; i<entitlementEditButton.size();) {
			entitlementEditButton.get(i).click();
			price = canadaPrice.getAttribute("value");
			cancelButton.click();
			break;
			 
		}
		return price;
	}
	
	@Step("User Overide the Canadain Price,  Method: {method} ")
	public List<String> OveridePriceCAD(String overRidePrice){
		ArrayList<String> pricelist = new ArrayList<String>();	
		for(int i=0; i<entitlementEditButton.size();) {
			entitlementEditButton.get(i).click();
			canadaPrice.clear();
			canadaPrice.sendKeys(overRidePrice);
			saveButton.click();
			break;
			
		}
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[not(contains(@class,'not_purchaseable'))]/td/button[contains(@class,'entitlement_edit_row_button')]")));		
		for(int i=0; i<entitlementEditButton.size(); ) {
			entitlementEditButton.get(i).click();
			String unit = canadaunit.getText().replaceAll(" ", "");
			overRidePrice =canadaPrice.getAttribute("value");	
			System.out.println("The OP "+overRidePrice );
			StringBuilder newStr =new StringBuilder(unit);
			newStr.insert(1, overRidePrice);
			pricelist.add(newStr.toString());
		    cancelButton.click();
		    break;
		}
		return pricelist;
	}
	
	@Step("get the Production Code Description from Entl Table,  Method: {method} ")
	public String getProdCodeDesc() {
		return productCodeDescription.getText();
	}
	
	@Step("Get the ISBN Details from Entl Table,  Method: {method} ")
	public String getISBN() {
		String isbnNumber=null;
	for(int i=0; i<isbnEle.size(); i++) {
		isbnNumber=  isbnEle.get(i).getAttribute("innerText");
		
	}
	return isbnNumber;
	}
	
	@Step("Get the Purchase Text  from Entl Table,  Method: {method} ")
	public List<String> getpurchasePriceData() {
	ArrayList<String> purchasePriceList = new ArrayList<String>();
	String pText=null;
	for(int i=0; i<purchaseandPrice.size(); i++) {
		pText=  purchaseandPrice.get(i).getAttribute("innerText");
	    if(pText.contains("details")){
	    	pText = pText.replace("[details]", "");
	    }
	    		
		purchasePriceList.add(pText);
	}
	return purchasePriceList;
	}
	
	@Step("Get the Date data from Entl Table,  Method: {method} ")
	public String getDateValue() {
		return dateValue.getText();
	}
	
	@Step("Click details link from Entl Table,  Method: {method} ")
	public void clickDetailslink() {
		detailslink.click();
	}
	
	@Step("Get the Entitlement details,  Method: {method} ")
	public String getEntlDetails() {
		return entlDetailsJson.getAttribute("innerHTML");
	}
}
