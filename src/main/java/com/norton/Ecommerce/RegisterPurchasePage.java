package com.norton.Ecommerce;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;

import Utilities.BaseDriver;
import Utilities.FakerNames;
import Utilities.LogUtil;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
import io.qameta.allure.Step;

public class RegisterPurchasePage {

	WebDriver driver;
	String ErrorMessage;
	String StudentEmailID;
	String StudentEmailID_InvalidCode;
	public String EmailID = "temp@temp.com";
	public String first_name = "temp";
	public String last_name = "temp1";
	///public String new_user_emailID = "tempnew@temp.com";
	WebDriverWait wait;

	@FindBy(how = How.XPATH, using = "//input[@id='name'][@class='login_dialog_text_input']")
	public WebElement FirstName_LastName;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Sign_in_or_Register;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']")
	public WebElement SignInButton;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement StudentEmail;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.ID, using = "password2")
	public WebElement Password2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='login_dialog_step_header'][contains(text(),'Purchase options:')]")
	public WebElement PurchaseoptionsPopUp;

	@FindBy(how = How.XPATH, using = "//input[@type='radio'][@id='register_purchase_choice_register'][@value='register']")
	public WebElement registrationradioOprtion;
	
	@FindBy(how = How.XPATH, using = "//label[@id='register_purchase_choice_register']")
	public WebElement registration_code;

	@FindBy(how = How.XPATH, using = "//label[@for='register_purchase_choice_register']")
	public WebElement registrationcodeText;

	@FindBy(how = How.XPATH, using = "//label[@for='register_purchase_choice_purchase']")
	public WebElement purchaseaccessText;

	@FindBy(how = How.XPATH, using = "//label[@for='register_purchase_choice_trial']")
	public WebElement trialaccessText;

	@FindBy(how = How.ID, using = "register_purchase_choice_trial")
	public WebElement register_purchase_choice_trial;

	@FindBy(how = How.ID, using = "register_purchase_choice_purchase")
	public WebElement purchaseOptions;

	@FindBy(how = How.ID, using = "login_dialog_reg_go_button")
	public WebElement ShowPurchasingOptions;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']/span[contains(.,'Get Free')]")
	public WebElement GetFreeItems;

	@FindBy(how = How.ID, using = "login_dialog_reg_go_button")
	public WebElement PurchaseRegisterAccess;

	@FindBy(how = How.ID, using = "login_dialog_reg_go_button")
	public WebElement CompleteYourPurchase;

	@FindBy(how = How.ID, using = "access_code")
	public WebElement access_code;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement register_My_code_Button;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']/span[@class='ui-button-text']/span[@id='login_dialog_reg_go_button_text'][contains(text(),'Confirm')]")
	public WebElement Confirm_Button;

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	public WebElement Checkbox;

	@FindBy(how = How.ID, using = "login_dialog_school_type_select")
	public WebElement school_dropdown;

	@FindBy(how = How.ID, using = "login_dialog_country_select")
	public WebElement Select_Country;

	@FindBy(how = How.ID, using = "login_dialog_state_select")
	public WebElement Select_State;

	@FindBy(how = How.ID, using = "login_dialog_school_text_input")
	public WebElement School_Institution;

	@FindBy(how = How.XPATH, using = "//ul[contains(@id,'ui-id-')]/li")
	public List<WebElement> School_Selection;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::button")
	public WebElement Continue_Button;

	@FindBy(how = How.ID, using = "login_dialog_error_msg")
	public WebElement Error_Msg;

	@FindBy(how = How.ID, using = "login_dialog_reg_go_button")
	public WebElement enterCreditCardInfo;

	@FindBy(how = How.ID, using = "purchase_options_button_inner")
	public WebElement purchaseOptionsButton;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement oKbutton;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_confirmation_go_button']")
	public WebElement Get_Started;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK, Sign Up for Trial Access')]")
	public WebElement OK_SignUpforTrialAccess_button;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement MaintenanceButton;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']")
	public WebElement Sign_Up_for_Trial_AccessButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_top_msg']/img[@id='login_dialog_close']")
	public WebElement loginDialogCloseButton;

	@FindBy(how = How.ID, using = "login_dialog_reg_startover_button")
	public WebElement BackUpButton;

	@FindBy(how = How.ID, using = "creditcard")
	public WebElement CreditCardNumber;

	@FindBy(how = How.ID, using = "email_confirm")
	public WebElement emailConfirm;

	@FindBy(how = How.ID, using = "expirationdate")
	public WebElement expdate;

	@FindBy(how = How.ID, using = "securitycode")
	public WebElement securityCode;

	@FindBy(how = How.ID, using = "address1")
	public WebElement address1;

	@FindBy(how = How.ID, using = "address2")
	public WebElement address2;

	@FindBy(how = How.ID, using = "city")
	public WebElement city;

	@FindBy(how = How.ID, using = "state")
	public WebElement state;

	@FindBy(how = How.ID, using = "postalcode")
	public WebElement postalcode;

	@FindBy(how = How.ID, using = "phonenumber")
	public WebElement phonenumber;

	@FindBy(how = How.ID, using = "login_dialog_seagull")
	public WebElement seagullButton;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Continue')]/ancestor::div[@id='sl_tos_checked_button']")
	public WebElement siteLContinue;

	@FindBy(how = How.XPATH, using = "//div[@id='sl_note_checked_button']//span[@id='login_dialog_reg_go_button_text']")
	public WebElement siteLContinuebutton;

	@FindBy(how = How.XPATH, using = "//span[contains(.,'Upgrade to Full Access')]/parent::button[@role='button']")
	public WebElement upGradeButton;

	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']/tbody/tr/td[@class='login_dialog_purchase_price_td']/span[@class='login_dialog_purchase_price_td_dollar']")
	public List<WebElement> purchaseOptionsDollar;

	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']/tbody/tr/td[@class='login_dialog_purchase_price_td']/span[@class='login_dialog_purchase_price_td_unit']/abbr[@title='Canadian dollar']")
	public List<WebElement> candainUnit;

	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']/tbody/tr/td[@class='login_dialog_purchase_price_td']/span[@class='login_dialog_purchase_price_td_price']")
	public List<WebElement> purchaseOptionsPrice;

	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']/tbody/tr/td[@class='login_dialog_purchase_description_td']")
	public List<WebElement> purchaseOptionsDescription;

	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']/tbody/tr/td/div[contains(@class,'login_dialog_purchase_already_purchased_div')]/ancestor::tr/td/div[contains(@class,'login_dialog_purchase_description_td_description')]")
	public List<WebElement> purchaseISBNDescription;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_startover_button']/span[@class='ui-button-text'][contains(text(),'Back Up')]")
	public WebElement backUpButton;

	@FindBy(how = How.XPATH, using = "//select[@id='login_dialog_state_select']/option")
	public List<WebElement> stateOptionList;

	@FindBy(how = How.ID, using = "login_dialog_purchase_price_dollars")
	public WebElement totalprice;

	@FindBy(how = How.XPATH, using = "//span[@id='login_dialog_purchase_price_usd']/abbr[@title='Canadian dollar']")
	public WebElement CanadianUnit;

	@FindBy(how = How.XPATH, using = "//span[@id='login_dialog_purchase_price_usd']/abbr[@title='United States dollar']")
	public WebElement UnitedDollarsUnit;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_reg_go_button']/span[@class='ui-button-text'][contains(.,'Secure Checkout through Canadian Distributor')]")
	public WebElement SecureCheckoutCanadianDistributorButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_inner']//table/tbody/tr/td[@class='login_dialog_purchase_option_checkbox_td']/input")
	public List<WebElement> checkboxes;

	@FindBy(how = How.XPATH, using = "//td[@class='login_dialog_purchase_price_td']/span[contains(.,'FREE')]/ancestor::tr/td[@class='login_dialog_purchase_option_checkbox_td']/input")
	public List<WebElement> checkboxesfree;

	@FindBy(how = How.XPATH, using = "//tr/td[@class='login_dialog_purchase_price_td']/span[contains(.,'FREE')]")
	public List<WebElement> freeText;

	@FindBy(how = How.XPATH, using = "//table[contains(@class,'login_dialog_purchase_tile')]/tbody/tr/td[contains(@class,'login_dialog_purchase_description_td')]/div[@class='login_dialog_purchase_description_td_description']")
	public List<WebElement> selectcheckboxOthers;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_inner']/div[@class='login_dialog_purchase_option_div login_dialog_purchase_option_div_already_purchased']")
	public List<WebElement> divtable;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_reg_buttons']/button[@role='button']/span[contains(.,'Cancel')]")
	private WebElement CancelButton;

	@FindBy(how = How.XPATH, using = "//button[@id='purchase_register_button'][contains(.,'Register a Code or Purchase Access')]")
	private WebElement RegisterCodePurchaseAccessButton;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_inner']//div/p[not(@style)]")
	private WebElement RegisterPurchaseNotAvailableCanada;

	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_inner']//div/p/a[@id='hlink_return']")
	private WebElement backlinkButton;

	@FindBy(how = How.XPATH, using = "//div[@id='canada_items_not_available_error_msg']")
	private WebElement errorDistributorDatabase;

	@FindBy(how = How.XPATH, using = "//div[@id='canada_items_not_available_error_msg']/a")
	private WebElement supportlink;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'general_dialog_outer')]")
	private WebElement dialogScroll;

	// SS EBook Checkbook
	@FindBy(how = How.XPATH, using = "//td[@class=\"login_dialog_purchase_option_checkbox_td\"]//label[@for=\"purchase_option_0\"]")
	public WebElement Checkbox_Ebook;

	// SS
	@FindBy(how = How.ID, using = "login_dialog_reg_go_button")
	public WebElement ReviewyourOrder;
	// SS
	@FindBy(how = How.XPATH, using = "//button[@class='activity_group_selector animations ')]")
	public WebElement SelectAnimation;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_order_conf_bottom_div']/div[contains(@id,'tax_div')]")
	public WebElement SalesTax;

	// SS
	@FindBy(how = How.ID, using = "//div[@id='login_dialog_order_conf_bottom_div']//span[contains(.,'$93.50')]")
	public WebElement TotalPriceUS;

	// SS
	@FindBy(how = How.ID, using = "//select[@id = 'state']")
	public WebElement BillingStateName;

	// SS
	@FindBy(how = How.XPATH, using = "//input[@id = 'postalcode']")
	public WebElement BillingPinCode;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@class = 'login_dialog_purchase_already_purchased_div']")
	public WebElement AlreadyPurchased;

	// SS
	@FindBy(how = How.XPATH, using = "//button[@type='button']//span[contains(.,'OK')]")
	public WebElement PurchaseCompletePopUp;

	// SS
	@FindBy(how = How.XPATH, using = "//td[@class='login_dialog_reg_table_cell_right']")
	public WebElement registration_code_getStarted;

	//
	@FindBy(how = How.XPATH, using = "//button[@type='button']//span[contains(.,'Sign Up')]")
	public WebElement sign_up_for_trial_popup;

	// SS - NOt working
	@FindBy(how = How.XPATH, using = "//div[@class='alt_text_message']//div[contains(.,'registered users')]")
	public WebElement ebook_unaccessible_errormessage;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@class=\"alt_text_message\"]//div[@class=\"alt_text_p\"]")
	public WebElement ebook_unaccessible_errormessage_new;
	// div[@class="alt_text_message"]//div[@class="alt_text_p"]

	// SS
	@FindBy(how = How.XPATH, using = "//table[@id = 'login_dialog_reg_table']//td[contains(.,'LYR')]")
	public WebElement good_to_go_regcode;

	// SS
	@FindBy(how = How.XPATH, using = "//table[@id = 'login_dialog_reg_table']//tr[1]//td[@class = 'login_dialog_reg_table_cell_right']")
	public WebElement gtg_reg_id;

	// SS
	@FindBy(how = How.XPATH, using = "//table[@id = 'login_dialog_reg_table']//tr[2]//td[@class = 'login_dialog_reg_table_cell_right']")
	public WebElement gtg_name;

	// SS
	@FindBy(how = How.XPATH, using = "//table[@id = 'login_dialog_reg_table']//tr[3]//td[@class = 'login_dialog_reg_table_cell_right']")
	public WebElement gtg_email;

	// Initializing Web Driver and PageFactory.
	public RegisterPurchasePage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();

	// Call ReadJsonobject method to read and set node values from Json testData
	// file.
	/*
	 * ReadUIJsonFile readJasonObject = new ReadUIJsonFile(); JsonObject jsonobject
	 * = readJasonObject.readUIJson();
	 */
	// Allure Steps Annotations and Methods
	@Step("Register, Purchase, or Sign Up for Trial Access,  Method: {method} ")
	public String register_Access_ValidCode(String accessCode) throws Exception {

		wait = new WebDriverWait(driver, Duration.ofSeconds(50));

		wait.until(ExpectedConditions.visibilityOf(FirstName_LastName));

		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(EmailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		oKbutton.click();
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}

		// registration_code.click();
		Thread.sleep(5000);
		access_code.sendKeys(accessCode);

		register_My_code_Button.click();
		return EmailID;
	}

	@Step("Register, Purchase, or Sign Up for Trial Access,  Method: {method} ")
	public String register_Access_InValidCode() throws Exception {

		wait = new WebDriverWait(driver, Duration.ofSeconds(50));

		wait.until(ExpectedConditions.visibilityOf(FirstName_LastName));

		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(EmailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		// StudentEmailID_InvalidCode=
		// jsonobject.getAsJsonObject("StudentRegistration_ExpiredCode").get("EmailID").getAsString();
		// registration_code.click();
		oKbutton.click();
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		Thread.sleep(5000);
		/*
		 * access_code.sendKeys(jsonobject
		 * .getAsJsonObject("StudentRegistration_ExpiredCode")
		 * .get("Expired_Registration_Code").getAsString());
		 */

		register_My_code_Button.click();

		return EmailID;
	}

	@Step("get the First Name and Last name from user creation page,  Method: {method} ")
	public String getFLName() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String flName = "return document.getElementById('name').value;";
		String name = js.executeScript(flName).toString();
		return name;
	}

	@Step("Before we complete your registration,  Method: {method} ")
	public void confirm_Registration(String studentEmailID) throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Confirm_Button));
		StudentEmail.sendKeys(studentEmailID);
		Confirm_Button.click();
	}

	@Step("Click the purchase Option button from DLP page,  Method: {method} ")
	public void clickpurchaseOptionButton() throws Exception {		
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("purchase_options_button_inner")));
		//ReusableMethods.scrollIntoView(driver, Sign_in_or_Register);
		ReusableMethods.clickElement(driver, purchaseOptionsButton);

	}

	@Step("Verify Purchase Options Button is displayed,  Method: {method} ")
	public boolean isPurchaseOptionButton() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("purchase_options_button_inner")));
		return purchaseOptionsButton.isDisplayed();
	}

	@Step("Select Purchase Option radio Button,  Method: {method} ")
	public void selectPurchaseOption() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='register_purchase_choice_purchase']")));
		purchaseaccessText.click();
	}
	
	@Step("Select Register Code  radio Button,  Method: {method} ")
	public void selectRegisterCodeOption() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='register_purchase_choice_register']")));
		registrationradioOprtion.click();
	}

	@Step("Please read our Terms of Use and Privacy Policy.,  Method: {method} ")
	public void term_privacypolicy_popup() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));

		// wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");

		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Thread.sleep(5000);
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(School_Institution));
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();
		Thread.sleep(2000);

	}

	@Step("Please read our Terms of Use and Privacy Policy SiteLicense user.,  Method: {method} ")
	public void siteLicensetermpolicy() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));

		// wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Thread.sleep(5000);
		siteLContinue.click();
		Thread.sleep(5000);
		boolean isdisplayed = driver
				.findElements(
						By.xpath("//div[@id='sl_note_checked_button']//span[@id='login_dialog_reg_go_button_text']"))
				.size() > 0;
		ReusableMethods.scrollToElement(driver,
				By.xpath("//div[@id='sl_note_checked_button']//span[@id='login_dialog_reg_go_button_text']"));
		if (isdisplayed == true) {
			siteLContinuebutton.click();
		}
	}

	@Step("Please read our Terms of Use and Privacy Policy for purchasing options.,  Method: {method} ")
	public void termprivacypolicypopupPurchaseOptions() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']")));
		ReusableMethods.clickElement(driver, Checkbox);
		Thread.sleep(3000);
		school_dropdown.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");
		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Select_State.click();
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(School_Institution));
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();
		Thread.sleep(2000);

	}
	
	@Step("Verify Getting Started Pop up is displayed, Method: {method} ")
	public boolean  isgetStarted_popup() throws Exception {
		ReusableMethods.checkPageIsReady(driver);
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Get_Started));
		return Get_Started.isDisplayed();

	}

	@Step("You’re good to go! pop up, Method: {method} ")
	public void getStarted_popup() throws Exception {
		ReusableMethods.checkPageIsReady(driver);
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Get_Started));
		Get_Started.click();

	}

	@Step("Create a New Trial Access user, Method: {method} ")
	public String createTrialAccessNewUser() throws InterruptedException {
		ReusableMethods.WaitElementClickable(driver, By.id("gear_button"));
		Sign_in_or_Register.click();
		SignInButton.click();
		Thread.sleep(5000);
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String EmailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(EmailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		boolean isOKButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOKButton == true) {
			oKbutton.click();
		}
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		StudentEmail.sendKeys(EmailID);
		Confirm_Button.click();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Checkbox));
		ReusableMethods.clickElement(driver, Checkbox);
		Thread.sleep(3000);
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");
		Thread.sleep(3000);
		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Thread.sleep(3000);
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(School_Institution));
		School_Institution.click();
		Thread.sleep(3000);
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();

		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Get_Started));
		Get_Started.click();
		return EmailID;

	}

	@Step("User Enters the purchase option details, Method: {method} ")
	public void purchaseOptiondetails(String EmailID) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(StudentEmail));
		StudentEmail.click();
		StudentEmail.sendKeys(EmailID);
		Thread.sleep(2000);
		Confirm_Button.click();

		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
		Select drop = new Select(school_dropdown);
		drop.selectByValue("HIGH_SCHOOL");
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		School_Institution.click();
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();

	}

	public void clickMaintenanceButton() {
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
	}

	@Step("Verify Already Purchase Text on Purchase Table,  Method: {method} ")
	public void VerifyPurchaseData() throws Exception {
		// List<WebElement> divtable=
		// driver.findElements(By.xpath("//div[@id='login_dialog_inner']/div"));
		int divtablecount = divtable.size();
		for (int i = 0; i < divtablecount; i++) {
			List<WebElement> colVals = divtable.get(i).findElements(By.tagName("td"));
			java.util.Iterator<WebElement> a = colVals.iterator();
			// System.out.println(colNum);
			WebElement val = null;
			while (a.hasNext()) {
				val = a.next();
				val.getText();

			}
			String alreadyPurchased = val.getText().replace("\n", " ");
			if (val.getText().equalsIgnoreCase("ALREADY PURCHASED"));
			LogUtil.log("ALREADY PURCHASED text is displayed");
			Assert.assertEquals("ALREADY PURCHASED", alreadyPurchased.toString().trim());
		}
	}

	@Step("get the ISBN Name from Purchase Table,  Method: {method} ")
	public String getISBNName() throws Exception {
		String isbnName = null;
		for (int i = 0; i < purchaseISBNDescription.size();) {
			isbnName = purchaseISBNDescription.get(0).getText();
			break;
		}
		return isbnName;
	}

	@Step("get the Purchased Name from Purchase Table,  Method: {method} ")
	public List<String> getPurchasedName() throws Exception {
		String isbnName = null;
		ArrayList<String> pList = new ArrayList<>();
		for (int i = 0; i < purchaseISBNDescription.size(); i++) {
			isbnName = purchaseISBNDescription.get(i).getText();
			pList.add(isbnName);
		}
		return pList;
	}

	@Step("Select the Check box from Purchase Table,  Method: {method} ")
	public void selectCheckbox() throws Exception {
		Thread.sleep(5000);
		for (int i = 0; i < checkboxes.size();) {
			checkboxes.get(i).click();
			break;
		}
	}

	@Step("Select all the Check box from Purchase Table,  Method: {method} ")
	public void selectallCheckbox() throws Exception {
		for (int i = 0; i < checkboxes.size(); i++) {
			boolean checked = checkboxes.get(i).isSelected();
			if (checked == false) {
				clickOKButton();
				try {
					ReusableMethods.clickElement(driver, checkboxes.get(i));
				} catch (ElementClickInterceptedException e) {
					// TODO Auto-generated catch block
					ReusableMethods.clickElement(driver, checkboxes.get(i));
				}
				Thread.sleep(3000);
			}

		}
	}

	@Step("Select the Check box from Purchase Table other than eBook,  Method: {method} ")
	public String selectOtherCheckbox() throws Exception {
		String descName = null;
		Thread.sleep(3000);
		int count = selectcheckboxOthers.size();
		for (int i = 0; i < count; i++) {
			descName = selectcheckboxOthers.get(i).getAttribute("innerText");
			if (!descName.contains("Ebook") && !descName.contains("Smartwork")
					&& !descName.contains("Student Solutions")) {
				checkboxes.get(i).click();
				boolean okButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
						.size() > 0;
				if (okButton == true) {
					oKbutton.click();
					continue;
				} else {
					break;
				}

			}
		}
		return descName;
	}
	@Step("Select the Check box from Purchase Table other than eBook,  Method: {method} ")
	public String selectCheckboxBasedonName(String tileName) throws Exception {
		String descName = null;
		Thread.sleep(3000);
		int count = selectcheckboxOthers.size();
		for (int i = 0; i < count; i++) {
			descName = selectcheckboxOthers.get(i).getAttribute("innerText");
			if (descName.contains(tileName)) {
				checkboxes.get(i).click();
				boolean okButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
						.size() > 0;
				if (okButton == true) {
					oKbutton.click();
					continue;
				} 
                break;
			}
		}
		return descName;
	}
	@Step("Select the Invalid Entitlement Check box from Purchase Table,  Method: {method} ")
	public String selectInvalidISBNCheckbox(String isbnName) throws Exception {
		String descName = null;
		Thread.sleep(3000);
		int count = selectcheckboxOthers.size();
		for (int i = 0; i < count; i++) {
			descName = selectcheckboxOthers.get(i).getAttribute("innerText");
			if (descName.contains(isbnName)) {
				checkboxes.get(i).click();
				boolean okButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
						.size() > 0;
				if (okButton == true) {
					oKbutton.click();
					continue;
				} else {
					break;
				}

			}
		}
		return descName;
	}

	@Step("Verify Check box is Checked,  Method: {method} ")
	public void isCheckBoxChecked() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("document.querySelector(\"input[type='checkbox']:checked\")");

		// document.querySelector("input[type='checkbox']:checked").checked
	}

	@Step("Select the Check box from Purchase Table which shows the FREE Option,  Method: {method} ")
	public void selectCheckboxFREEOption() throws Exception {
		Thread.sleep(5000);
		for (int i = 0; i < checkboxesfree.size(); i++) {
			boolean isEnabled = checkboxesfree.get(i).isEnabled();
			if (isEnabled == true) {
				checkboxesfree.get(i).click();
			}

		}

	}

	@Step("Click Enter Credit Card Button,  Method: {method} ")
	public void creditcardButton() throws Exception {
		ReusableMethods.scrollIntoView(driver, enterCreditCardInfo);
		enterCreditCardInfo.click();
	}

	@Step("Click Review Order Button,  Method: {method} ")
	public void reviewOrderbutton() throws Exception {
		enterCreditCardInfo.click();
	}

	@Step("user provide a Billing Information from Credit Card Section,  Method: {method} ")
	public void billingInfo() throws InterruptedException {
		address1.sendKeys("935  Poplar Avenue");
		address2.sendKeys("1501  Columbia Boulevard");
		city.sendKeys("Baltimore");
		Select stateinfo = new Select(state);
		stateinfo.selectByValue("AL");
		Thread.sleep(4000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='';", postalcode);
		postalcode.sendKeys("35005");
		phonenumber.clear();
		phonenumber.sendKeys("410-435-7279");
	}

	@Step("Click Complete Purchaseing Button,  Method: {method} ")
	public void completePurchase() {
		CompleteYourPurchase.click();
	}

	public String createUser() throws InterruptedException {
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//input[@id='name'][@class='login_dialog_text_input']")));
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		String emailID = FirstName + "_" + LastName + "@maildrop.cc";
		StudentEmail.clear();
		StudentEmail.sendKeys(emailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);

		try {
			boolean okButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
					.size() > 0;
			if (okButton == true) {
				oKbutton.click();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			boolean okButtonExist = driver
					.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]")).size() > 0;
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		Thread.sleep(5000);
		return emailID;
	}
	
	

	@Step(" Verify Error Message,  Method: {method} ")
	public String getErrorMsg() {
		return Error_Msg.getText();

	}

	@Step(" Double Click Seagull button on Credit card Page,  Method: {method} ")
	public void doubleClickSeagull() {
		Actions action = new Actions(driver);
		action.doubleClick(seagullButton).build().perform();

	}

	@Step("Click Upgrade to Full Access Button,  Method: {method} ")
	public void clickUpGradeFullAccessbutton() {
		upGradeButton.click();

	}

	@Step("Click Register a Code or Purchase Access Button,  Method: {method} ")
	public void clickRegisterCodePurchaseAccessButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(RegisterCodePurchaseAccessButton));
		ReusableMethods.clickElement(driver, RegisterCodePurchaseAccessButton);
	}

	@Step("Verify Upgrade to Full Access Button is displayed,  Method: {method} ")
	public boolean isUpGradeFullAccessbutton() throws InterruptedException {
		boolean isUpgradeButton = driver
				.findElements(By.xpath(
						"//button[@id='purchase_register_button'][contains(.,'Register a Code or Purchase Access')]"))
				.size() != 0;
		return isUpgradeButton;

	}

	@Step("Click OK Button to Full Access Button,  Method: {method} ")
	public void clickOKButton() {
		boolean isOK = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]")).size() > 0;
		if (isOK == true) {
			oKbutton.click();
		}
	}

	@Step("Capture the Price from the Purchase options:, Method: {method}")
	public List<String> capturePurchaseoptionsPrice() {
		String pricetext, pricedollarsign, priceUnit;
		boolean isCAD = driver.findElements(By.xpath(
				"//table[@class='login_dialog_purchase_tile']/tbody/tr/td[@class='login_dialog_purchase_price_td']/span[@class='login_dialog_purchase_price_td_unit']/abbr[@title='Canadian dollar']"))
				.size() > 0;
		ArrayList<String> pricelist = new ArrayList<String>();
		if (isCAD == true) {
			for (int j = 0, i = 0, k = 0; j < purchaseOptionsDollar.size() && i < purchaseOptionsPrice.size()
					&& k < candainUnit.size(); j++, i++, k++) {
				pricedollarsign = purchaseOptionsDollar.get(j).getText();
				pricetext = purchaseOptionsPrice.get(i).getText();
				priceUnit = candainUnit.get(k).getText();
				BigDecimal priceValue = new BigDecimal(pricetext);
				pricelist.add(pricedollarsign + priceValue.stripTrailingZeros().toPlainString() + priceUnit);
			}
		} else {
			for (int j = 0, i = 0; j < purchaseOptionsDollar.size() && i < purchaseOptionsPrice.size(); j++, i++) {
				pricedollarsign = purchaseOptionsDollar.get(j).getText();
				pricetext = purchaseOptionsPrice.get(i).getText();
				pricelist.add(pricedollarsign + pricetext);
			}
		}
		return pricelist;
	}

	@Step("Capture the Description from the Purchase options:, Method: {method}")
	public List<String> capturePurchaseoptionsDesc() {
		ArrayList<String> pricelist = new ArrayList<String>();
		for (int i = 0; i < purchaseOptionsDescription.size(); i++) {
			pricelist.add(purchaseOptionsDescription.get(i).getText());
		}
		return pricelist;
	}

	@Step("Click Purchase or Register for Access from the Purchase options:, Method: {method}")
	public void clickRegisterAccessButton() {
		ReusableMethods.clickElement(driver, PurchaseRegisterAccess);
	}

	@Step("Click Show Purchasing button from Login Dialog page:, Method: {method}")
	public void clickShowPurchaseButton() {
		ShowPurchasingOptions.click();
	}

	@Step("Please read our Terms of Use and Privacy Policy SiteLicense user.,  Method: {method} ")
	public void selectCheckboxTermofUse() throws Exception {
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		Checkbox.click();
	}

	@Step("Verify School or institution details default selected values,  Method: {method} ")
	public String schoolDropdownDefault() {
		Select se = new Select(school_dropdown);
		WebElement firstSelectedele = se.getFirstSelectedOption();
		String firstSelected = firstSelectedele.getText();
		return firstSelected;
	}

	@Step("Verify School or institution details default selected values,  Method: {method} ")
	public String countrySelectedDefault() {
		Select se = new Select(Select_Country);
		WebElement firstSelectedele = se.getFirstSelectedOption();
		String firstSelected = firstSelectedele.getText();
		return firstSelected;
	}

	@Step("Verify School or institution options values,  Method: {method} ")
	public List<String> schoolDropdownlist() {
		Select se = new Select(school_dropdown);
		ArrayList<String> schoolName = new ArrayList<String>();
		List<WebElement> allOptionsElement = se.getOptions();
		for (WebElement optionEle : allOptionsElement) {
			schoolName.add(optionEle.getText().toString());

		}
		return schoolName;

	}

	@Step("Verify School or institution options values,  Method: {method} ")
	public List<String> countryDropdownlist() {
		Select se = new Select(Select_Country);
		ArrayList<String> countryName = new ArrayList<String>();
		List<WebElement> allOptionsElement = se.getOptions();
		for (WebElement optionEle : allOptionsElement) {
			countryName.add(optionEle.getText().toString());
		}
		return countryName;

	}

	@Step("Verify User Selects the schoolName {0},countyName {1} from Dropdown and Verify State dropdown and School Institute is displayed,  Method: {method} ")
	public void selectCountryDropDown(String schoolName, String countyName, String stateName, String instName)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		Select schoolNameOption = new Select(school_dropdown);
		schoolNameOption.selectByValue(schoolName);
		Select country = new Select(Select_Country);
		country.selectByValue(countyName);
		Thread.sleep(2000);
		try {
			Select_State.click();
		} catch (ElementClickInterceptedException e) {
			// TODO Auto-generated catch block
			e.getMessage();
		}
		selectState(stateName);
		wait.until(ExpectedConditions.elementToBeClickable(School_Institution));
		School_Institution.click();
		School_Institution.sendKeys(instName);
		seagullButton.click();
	}

	@Step("Verify User Selects Not a Student Dropdown option and Verify State dropdown and School Institute is displayed are Not displayed,  Method: {method} ")
	public boolean selectNotaStudent(String schoolName, String countyName) {
		Select schoolNameOption = new Select(school_dropdown);
		schoolNameOption.selectByValue(schoolName);
		Select country = new Select(Select_Country);
		country.selectByValue(countyName);
		boolean isState = driver.findElements(By.id("login_dialog_state_select")).size() > 0;
		return isState;
	}

	@Step("Verify the Total Price,  Method: {method} ")
	public boolean isTotalPrice() {
		String totalPrice = totalprice.getText();
		String unit = CanadianUnit.getText();
		String totalP = totalPrice + "" + unit;
		return totalP != null;
	}

	@Step("Verify the Total Price,  Method: {method} ")
	public String getTotalPrice() {
		String totalPrice = totalprice.getText();
		String unit = CanadianUnit.getText();
		String totalP = totalPrice + "" + unit;
		return totalP;
	}

	@Step("Verify the Total Price for USA,  Method: {method} ")
	public String getUSATotalPrice() {
		String totalPrice = totalprice.getText();
		String unit = UnitedDollarsUnit.getText();
		String totalP = totalPrice + " " + unit;
		return totalP;
	}

	@Step("Verify the Secure Checkout through Canadian Distributor Button,  Method: {method} ")
	public boolean issecureButton() {
		return SecureCheckoutCanadianDistributorButton.isDisplayed();
	}

	@Step("Click the Secure Checkout through Canadian Distributor Button,  Method: {method} ")
	public void clickSecureCheckOutButton() {
		ReusableMethods.scrollIntoView(driver, SecureCheckoutCanadianDistributorButton);
		ReusableMethods.clickElement(driver, SecureCheckoutCanadianDistributorButton);

	}

	@Step("Click the Get Free Items Button,  Method: {method} ")
	public void clickGetFreeItemsButton() {
		ReusableMethods.scrollIntoView(driver, GetFreeItems);
		ReusableMethods.clickElement(driver, GetFreeItems);

	}

	public void selectState(String stateName) {
		String stateValue = null;
		for (int i = 0; i < stateOptionList.size(); i++) {
			try {
				stateValue = stateOptionList.get(i).getAttribute("value");
			} catch (StaleElementReferenceException e1) {
				// TODO Auto-generated catch block
				stateValue = stateOptionList.get(i).getAttribute("value");
			}
			if (stateValue.equals(stateName)) {
				stateOptionList.get(i).click();
			}
		}

	}

	@Step("Create a Purchase user and Logout the User,  Method: {method} ")
	public String createTrialUser() throws Exception {
		String emailID = null;
		try {
			emailID = createUser();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean isOKButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOKButton == true) {
			oKbutton.click();
		}
		register_purchase_choice_trial.click();
		Sign_Up_for_Trial_AccessButton.click();
		Thread.sleep(2000);
		OK_SignUpforTrialAccess_button.click();
		confirm_Registration(emailID);
		termprivacypolicypopupPurchaseOptions();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Get_Started));
		Get_Started.click();
		return emailID;
	}

	@Step("User clicks the Close Button from Purchase Option Table,  Method: {method} ")
	public void clickCloseButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(loginDialogCloseButton));
		ReusableMethods.clickElement(driver, loginDialogCloseButton);
	}

	@Step("Verify This title is not available for purchase in Canada.,  Method: {method} ")
	public String getErrorentlCanada() {
		return RegisterPurchaseNotAvailableCanada.getText();
	}
	
	@Step("Verify Purchase Options Pop Up is displayed.,  Method: {method} ")
	public boolean isPurchaseOption() {
		return PurchaseoptionsPopUp.isDisplayed();
	}

	@Step("User Click the Back Button,  Method: {method} ")
	public void clickBackButton() {
		backlinkButton.click();
	}

	@Step("Verify The selected items were not found in distributor's database.You may request help from our support team.,  Method: {method} ")
	public String getdistrDatabaseError() {
		ReusableMethods.scrollIntoView(driver, dialogScroll);
		return errorDistributorDatabase.getAttribute("innerText");
	}

	@Step("Click request help from our support team. link.,  Method: {method} ")
	public void clicksupportTeamLink() {
		ReusableMethods.scrollIntoView(driver, dialogScroll);
		ReusableMethods.scrollIntoView(driver, dialogScroll);
		supportlink.click();

	}

	@Step("Click Check boxes which has Free text.,  Method: {method} ")
	public void clickFreeTextCheckboxes() {
		for (int i = 0; i < freeText.size(); i++) {
			freeText.get(i).getText();
		}

	}

	// SS
	@Step("Click Complete Purchaseing Button,  Method: {method} ")
	public void clickCompletePurchase() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(CompleteYourPurchase));
		CompleteYourPurchase.click();
	}

	// SS
	@Step("Click Complete Purchaseing Button,  Method: {method} ")
	public void clickContinueButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Continue_Button));
		Continue_Button.click();
	}

	// SS
	@Step("Click Back Button Button,  Method: {method} ")
	public void clickBackUpButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(BackUpButton));
		BackUpButton.click();
	}

	// SS
	@Step("Click Checkbox_Ebook, Method: {method} ")
	public void clickCheckbox_Ebook() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Checkbox_Ebook));
		Checkbox_Ebook.click();
	}

	// SS
	@Step("Click Complete Purchaseing Button,  Method: {method} ")
	public void click_PurchaseCompletePopUp() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(PurchaseCompletePopUp));
		PurchaseCompletePopUp.click();
	}

	// SS
	@Step("Verify Payment,  Method: {method} ")
	public void verifyPayment() throws Exception {
		creditcardButton();
		doubleClickSeagull();
		Thread.sleep(3000);
		CreditCardNumber.clear();
		String CCNumber = jsonobject.getAsJsonObject("CreditCard").get("CCNumber").getAsString();
		CreditCardNumber.sendKeys(CCNumber);
		expdate.clear();
		// String CreditCardExpiryDate = PropertiesFile.getCCexpirydate();
		String[] str = ReusableMethods.getUSASysDate();
		int month = Integer.parseInt(str[0]);
		int year = Integer.parseInt(str[1])+2;
		String expDatevalue =month + "/" +year ;
		expdate.sendKeys(expDatevalue);
		ReusableMethods.clickTABkey(driver);
		securityCode.sendKeys("123");
		billingInfo();
		reviewyourOrder();
		Thread.sleep(5000);

	}

	// SS
	@Step("Verify Sales Tax and Total Price for Chem6,  Method: {method} ")
	public void verifySalexTaxTotalPrice() throws Exception {
		String captured_sales_tax = captureSalesTax();
		String substring_sales_tax = captured_sales_tax.substring(14);
		String expected_sales_tax = PropertiesFile.getSalesTax();
		Assert.assertEquals(substring_sales_tax, expected_sales_tax.trim());
		String captured_total_price = captureTotalPrice();
		String expected_total_price = PropertiesFile.getTotalPrice().trim();
		Assert.assertEquals(captured_total_price, expected_total_price);
	}

	// SS
	@Step("Verify Total Price of Printed Ebook - Chem6,  Method: {method} ")
	public void verifyTotalPricePrinted() {
		String totalprice_printedtextbook = captureTotalPricePrinted();
		String totalPrice_prop = PropertiesFile.getTotalPricePrinted();
		Assert.assertEquals(totalprice_printedtextbook, totalPrice_prop);
	}

	// SS
	@Step("Click on Review your order button, Method: {method}")
	public void reviewyourOrder() {
		ReviewyourOrder.click();
	}

	@Step("Select Sales Tax, Method: {method}")
	public String captureSalesTax() {
		String ST = SalesTax.getAttribute("innerText");
		return ST;
	}

	// SS
	@Step("Select Animation Slide")
	public void selectAnimantionSlide() {
		SelectAnimation.click();
	}

	// SS
	@Step("Select Total Price")
	public String captureTotalPricePrinted() {
		String TP_PrintedTextBook = totalprice.getAttribute("innerText");
		return TP_PrintedTextBook;
	}

	// SS
	@Step("Select Total Price")
	public String captureTotalPrice() {
		String TotalPriceUS = totalprice.getAttribute("innerText");
		return TotalPriceUS;
	}

	// SS
	@Step("Editing Billing Address")
	public void editBillingAddress() throws InterruptedException {
		// state.click();
		Select statename = new Select(state);
		statename.selectByValue("AL");
		Thread.sleep(2000);
		BillingPinCode.clear();
		BillingPinCode.sendKeys("35005");

	}

	// SS
	@Step("Click Confirm Button,  Method: {method} ")
	public void click_confirmButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
		wait.until(ExpectedConditions.elementToBeClickable(Confirm_Button));
		Confirm_Button.click();
	}

	// SS
	@Step("Click registration code Button,  Method: {method} ")
	public void click_registration_code() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(registration_code));
		registration_code.click();
	}

	// SS
	@Step("Click Register my code Button,  Method: {method} ")
	public void click_register_My_code_Button() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(register_My_code_Button));
		register_My_code_Button.click();
	}

	// @Step("Register, Purchase, or Sign Up for Trial Access, Method: {method} ")
	public String register_Access_InValidCode(String accessCode) throws Exception {

		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(FirstName_LastName));

		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		EmailID = FirstName + "_" + LastName + "@mailinator.com";
		first_name = FirstName;
		last_name = LastName;

		StudentEmail.clear();
		StudentEmail.sendKeys(EmailID.toLowerCase());
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		access_code.sendKeys(accessCode);
		register_My_code_Button.click();

		Thread.sleep(5000);
		StudentEmail.sendKeys(EmailID.toLowerCase());
		Thread.sleep(5000);
		Confirm_Button.click();
		Thread.sleep(2000);
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOf(Checkbox));
		ReusableMethods.clickElement(driver, Checkbox);
		Thread.sleep(5000);
		Select drop = new Select(school_dropdown);
		school_dropdown.click();
		drop.selectByValue("HIGH_SCHOOL");
		Thread.sleep(3000);
		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Thread.sleep(3000);
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(3000);
		School_Institution.click();
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();

		return EmailID;
	}

	// SS
	@Step("Create a New Purchase Access, Method: {method} ")
	public String createPurchaseAccessNewUser() throws InterruptedException {
		String newuseremailID;
		ReusableMethods.WaitElementClickable(driver, By.id("gear_button"));
		Sign_in_or_Register.click();
		SignInButton.click();
		Thread.sleep(5000);
		String FirstName = FakerNames.getStudentFName();
		String LastName = FakerNames.getStudentFName();
		FirstName_LastName.clear();
		Thread.sleep(3000);
		FirstName_LastName.sendKeys(FirstName + " " + LastName);
		newuseremailID = FirstName + "_" + LastName + "@mailinator.com";
		StudentEmail.clear();
		StudentEmail.sendKeys(newuseremailID);
		String studentPassword = "Tabletop1";
		Password.sendKeys(studentPassword);
		Password2.sendKeys(studentPassword);
		//System.out.println(new_user_emailID);
		boolean isOKButton = driver.findElements(By.xpath("//button[@type='button']/span[contains(text(),'OK')]"))
				.size() > 0;
		if (isOKButton == true) {
			oKbutton.click();
		}
		try {
			boolean okButtonExist = MaintenanceButton.isDisplayed();
			if (okButtonExist == true) {
				MaintenanceButton.click();
			}
		} catch (Exception e) {

			e.getMessage();
		}
		purchaseaccessText.click();
		try {
			selectPurchaseOption();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clickShowPurchaseButton();
		Thread.sleep(2000);
		StudentEmail.sendKeys(newuseremailID);
		Thread.sleep(2000);
		Confirm_Button.click();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Checkbox));
		ReusableMethods.clickElement(driver, Checkbox);
		Thread.sleep(5000);
		Select drop = new Select(school_dropdown);
		school_dropdown.click();
		drop.selectByValue("HIGH_SCHOOL");
		Thread.sleep(3000);
		Select country = new Select(Select_Country);
		country.selectByValue("USA");
		Thread.sleep(3000);
		Select state = new Select(Select_State);
		state.selectByValue("USA_AZ");
		Thread.sleep(5000);
		School_Institution.click();
		School_Institution.sendKeys("A");
		Thread.sleep(5000);
		String schoolName = null;
		for (int i = 0; i < School_Selection.size();) {
			schoolName = School_Selection.get(0).getText();
			break;
		}
		School_Institution.clear();
		Thread.sleep(2000);
		School_Institution.sendKeys(schoolName);
		Thread.sleep(2000);
		Continue_Button.click();
		return newuseremailID;
	}

	// SS
	@Step("Click Sign Register to Sign in or Sign Up,  Method: {method} ")
	public void signInRegister() {
		Sign_in_or_Register.click();
	}

}
