package com.norton.Ecommerce;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;

import Utilities.BaseDriver;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;
import io.qameta.allure.Step;

public class HelpDeskTool {
	WebDriver driver;
	

	@FindBy(how = How.XPATH, using = "//input[@class='textfield__input login-email-input']")
	public WebElement Email;

	@FindBy(how = How.XPATH, using = "//input[@class='textfield__input login-password-input']")
	public WebElement Password;

	@FindBy(how = How.XPATH, using = "//button[@class='button button--solid login-btn']")
	public WebElement Login;

	@FindBy(how = How.XPATH, using = "//div[@class='input-box']/input[@id='search-input']")
	public WebElement SearchInputText;

	@FindBy(how = How.XPATH, using = "//button[@class='search-btn']")
	public WebElement SearchButton;

	@FindBy(how = How.XPATH, using = "//td[@class='formatedEmail']/span/a")
	public WebElement emailLink;

	@FindBy(how = How.XPATH, using = "//button/span[@class='button__text'][contains(text(),'Reset Password')]")
	public WebElement resetPasswordButton;

	@FindBy(how = How.XPATH, using = "//div/label[@id='UserRole'][contains(text(),'User Role')]/following::div[contains(text(),'Student-')]")
	public WebElement UserRoleHdtool;
	
	@FindBy(how = How.XPATH, using = "//div/label[@id='UserRole'][contains(text(),'User Role')]/following::div[contains(text(),'Email')]")
	public WebElement UserEmailHdtool;

	@FindBy(how = How.XPATH, using = "//div/label[@id='UserRole'][contains(text(),'User Role')]/following::div[contains(text(),'Teaching Assistant-')]")
	public WebElement UserTARoleHdtool;

	@FindBy(how = How.XPATH, using = "//div/label[@id='UserRole'][contains(text(),'User Role')]/following::div[contains(text(),'Scoring Teaching Assistant-')]")
	public WebElement UserSTARoleHdtool;

	@FindBy(how = How.XPATH, using = "//div[@class='react-bootstrap-table']/table/tbody/tr/td/span[contains(text(),'access_code_admin')]")
	public WebElement accesscodeadmin;

	@FindBy(how = How.XPATH, using = "//div/label[@id='UserRole'][contains(text(),'User Role')]/following::div[contains(text(),'Instructor-')]")
	public WebElement UserInstRoleHdtool;

	@FindBy(how = How.XPATH, using = "//div[@class='user-holder']/button[@id='user-items-button']/div[@class='user-email']")
	public WebElement userEmailButton;

	@FindBy(how = How.XPATH, using = "//div[@class='user-dropdown open']/ul/li/button[contains(text(),'Sign Out')]")
	public WebElement signOutbutton;
	
	@FindBy(how = How.XPATH, using ="//th[@aria-label='Product Code/Description sort asc']/following::tbody/tr/td/span")
	public List<WebElement> ProductCodeDesc;
	
	@FindBy(how = How.XPATH, using ="//tbody/tr/td[contains(.,'purchase')]/preceding-sibling::td")
	public List<WebElement> isbnNumber;
	
	@FindBy(how = How.XPATH, using ="//tbody/tr/td[contains(.,'purchase')]/descendant-or-self::td/span")
	public WebElement purchaseEle;
	
	@FindBy(how = How.XPATH, using ="//th[@aria-label='Product Code/Description sort asc']/following::tbody/tr/td[@style]/div[contains(.,'$')]")
	public WebElement entlmentprice;
		
	@FindBy(how = How.XPATH, using ="//div[@class='react-bootstrap-table']/table/tbody/tr/td[@class='expand-cell']/following::td")
	public List<WebElement> orderHistory;
	
	@FindBy(how = How.XPATH, using ="//div/label[@id='CustomerID'][contains(text(),'Email')]/following::div[contains(@class,'user-content')][contains(text(),'@')]")
	public WebElement emailID;
	
	public HelpDeskTool() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	ReadUIJsonFile readUserData = new ReadUIJsonFile();
	JsonObject jsonobject = readUserData.readUIJson();

	@Step("Navigate to HD application,  Method: {method} ")
	public void navigateHDTool() {
		PropertiesFile.setHDToolURL();
	}

	// Allure Steps Annotations and Methods
	@Step("HD Tool Enter login Credentials,  Method: {method} ")
	public void hdtoolLogin() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(Email));
		ReusableMethods.checkPageIsReady(driver);
		String AccountEmailID = jsonobject.getAsJsonObject("HDTOOL_LOGIN").get("HDloginemail").getAsString();
		String AccountPassword = jsonobject.getAsJsonObject("HDTOOL_LOGIN").get("HDloginpassword").getAsString();
		Email.sendKeys(AccountEmailID);
		Password.sendKeys(AccountPassword);
		Login.click();
	}

	@Step("Select the Email option from HelpDesk tool,  Method: {method} ")
	public void selectEmailOption() throws InterruptedException {
		ReusableMethods.checkPageIsReady(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("dropdown1")));
		Select categoryList = new Select(driver.findElement(By.name("Category")));
		categoryList.selectByVisibleText("Email");
	}

	@Step("Select the Email option from HelpDesk tool,  Method: {method} ")
	public void searchEmail(String Email) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(SearchButton));
		SearchInputText.sendKeys(Email);
		SearchButton.click();
	}

	@Step("Click on the Email link from HelpDesk tool,  Method: {method} ")
	public void clickEmaillink() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(emailLink));
		emailLink.click();
	}

	@Step("Click Reset password button from HelpDesk tool,  Method: {method} ")
	public void clickResetPassword() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(resetPasswordButton));
		resetPasswordButton.click();
	}

	@Step("get the User Role from HelpDesk tool,  Method: {method} ")
	public String getUserRole() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(UserRoleHdtool));
		return UserRoleHdtool.getText();

	}
	
	@Step("get the User Email from HelpDesk tool,  Method: {method} ")
	public String getUserEmailID() {		
		return emailID.getText().trim();

	}
	
	@Step("get the User Email from HelpDesk tool,  Method: {method} ")
	public String getUserEmail() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(UserEmailHdtool));
		return UserEmailHdtool.getText();

	}

	@Step("get the User Role for Teaching Assitant from HelpDesk tool,  Method: {method} ")
	public String getTAUserRole() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(UserTARoleHdtool));
		return UserTARoleHdtool.getText();

	}

	@Step("get the User Role for Instructor  from HelpDesk tool,  Method: {method} ")
	public String getInstructorUserRole() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(UserInstRoleHdtool));
		return UserInstRoleHdtool.getText();

	}

	@Step("get the User Role for Scoring Teaching Assitant from HelpDesk tool,  Method: {method} ")
	public String getSTAUserRole() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(UserSTARoleHdtool));
		return UserSTARoleHdtool.getText();

	}

	@Step("get the access code admin type  in HelpDesk tool,  Method: {method} ")
	public String getaccesscodeAdmin() {
		ReusableMethods.scrollIntoView(driver, accesscodeadmin);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(accesscodeadmin));
		return accesscodeadmin.getText();

	}
	
	
	@Step("get the Prodct code Description on HelpDesk tool,  Method: {method} ")
	public List<String> getProductCodeDesc() {
		ReusableMethods.scrollIntoView(driver, ProductCodeDesc.get(0));
		ArrayList<String> prodCodeList = new ArrayList<>();
	    for(int i=0; i<ProductCodeDesc.size(); i++) {
	    	prodCodeList.add(ProductCodeDesc.get(i).getText());
	    }
		return prodCodeList;

	}
	
	@Step("Get the ISBN NUmber on HelpDesk tool,  Method: {method} ")
	public String getISBNNumber() {
		ReusableMethods.scrollIntoView(driver, ProductCodeDesc.get(0));
	    String isbn;
	  	isbn =isbnNumber.get(0).getText();
	  	return isbn;

	}
	
	@Step("Get the Purchase Text on HelpDesk tool,  Method: {method} ")
	public String getpurchaseText() {
		ReusableMethods.scrollIntoView(driver, ProductCodeDesc.get(0));
	    return purchaseEle.getText();

	}
	
	@Step("Get the Entitlements table Price on HelpDesk tool,  Method: {method} ")
	public String getEntlPrice() {
		ReusableMethods.scrollIntoView(driver, ProductCodeDesc.get(0));
	    return entlmentprice.getText();

	}
	
	@Step("Get the Order History on HelpDesk tool,  Method: {method} ")
	public List<String> getOrderHistory() {
		ArrayList<String> orderHistoryList = new ArrayList<>();
		ReusableMethods.scrollIntoView(driver, orderHistory.get(0));
		for(int i=0; i<orderHistory.size(); i++) {
			String orderHistorytext= orderHistory.get(i).getText();
			if(orderHistorytext.contains("-")) {
				continue;
			}
			orderHistoryList.add(orderHistorytext);
		}
		return orderHistoryList;

	}


	@Step("get the purchase type  in HelpDesk tool,  Method: {method} ")
	public String gettype(String EntTitleType) {
		String getTypeText = null;
		WebElement entitelementsRow = driver
				.findElement(By.xpath("//div[@class='row']/div[contains(.,'Entitlements')]"));
		ReusableMethods.scrollIntoView(driver, entitelementsRow);
		List<WebElement> typecol = driver
				.findElements(By.xpath(("//div[@class='react-bootstrap-table']/table/tbody/tr/td/span[contains(text(),'"
						+ EntTitleType + "')]")));
		for (int i = 0; i < typecol.size(); i++) {
			getTypeText = typecol.get(i).getText();
		}
		return getTypeText;

	}

	@Step("Sign Out HelpDesk tool,  Method: {method} ")
	public void signOutHDtool() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(userEmailButton));
		ReusableMethods.scrollIntoView(driver, userEmailButton);
		userEmailButton.click();
		Thread.sleep(2000);
		signOutbutton.click();
	}
}
