package com.norton.Ecommerce;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.qameta.allure.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonObject;

import Utilities.BaseDriver;
import Utilities.FakerNames;
import Utilities.PropertiesFile;
import Utilities.ReadUIJsonFile;
import Utilities.ReusableMethods;

public class Canadiandistributor {
	
	WebDriver driver;
	ReadUIJsonFile readJsonObject = new ReadUIJsonFile();
	JsonObject jsonobject = readJsonObject.readUIJson();
	
	
	@FindBy(how = How.XPATH, using = "//div[@title='Fulfillment by Login Canada']")
	public WebElement loginCanadaTitle;
	
	@FindBy(how = How.LINK_TEXT, using = "< Back to W. W. Norton")
	public WebElement backtoNortonDLP;
	
	@FindBy(how = How.XPATH, using = "//div[@class='redirect_blurb']/span[@class='redirect_text']")
	public WebElement redirectText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='info_cont']/div[@class='info_d']/table[@class='account_table']/tbody/tr")
	public List<WebElement> nortonAccountdetails;
	
	@FindBy(how = How.XPATH, using = "//table[@class='order_table']/tbody/tr")
	public List<WebElement> itemSummary;
	
	@FindBy(how = How.XPATH, using = "//table[@class='order_table']/tfoot/tr/th/span")
	public List<WebElement> itemtotalSummary;
	
	@FindBy(how = How.XPATH, using ="//input[@alt='Check out with PayPal']")
	public WebElement payPalButton;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'cancelUrl')]/a[@id='cancelLink']")
	public WebElement cancelLink;
	
	@FindBy(how = How.XPATH, using ="//input[@id='email'][@placeholder='Email or mobile number']")
	public WebElement emailMobileNumberTextbox;
	
	@FindBy(how = How.ID, using ="btnNext")
	public WebElement NextButton;
	
	@FindBy(how = How.ID, using ="password")
	public WebElement password;
	
	@FindBy(how = How.ID, using ="btnLogin")
	public WebElement loginButton;
	
	@FindBy(how = How.XPATH, using ="//button[@id='payment-submit-btn']")
	public WebElement paymentSubmitButton;
	
	@FindBy(how = How.ID, using ="emailSubTagLine")
	public WebElement emailSubTag;
	
	@FindBy(how = How.ID, using ="headerText")
	public WebElement headerText;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@data-testid,'header-container']/img[@alt='PayPal']")
	public WebElement payPaltext;
	
	@FindBy(how = How.XPATH, using ="//div[@data-testid='first-fis'] | //div[@id='paymentMethod']/div[@class='focusable singleFi editable']")
	public WebElement payPalPaymentcardDetails;
	
	@FindBy(how = How.XPATH, using ="//div[@class='info_h']/h2[contains(.,'BILLING INFO')]/following::div/table[@class='account_table']/tbody/tr")
	public List<WebElement> billingInfo;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/thead/following-sibling::tbody/tr/td[not(contains(.,'GST'))][not(contains(@class,'ra'))]")
	public WebElement ItemName;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr/td[not(contains(.,'GST'))]/following-sibling::td/span[@class='item_price']")
	public WebElement itemPricePayPal;
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr/td[contains(.,'GST')]/following-sibling::td/span[@class='item_price']")
	public  WebElement itemGSTPricePayPal;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr//td/span[@class='item_currency']")
	public WebElement itemCurrencyPayPal;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr//td[contains(.,'GST')]")
	public WebElement itemgstPayPal;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tfoot/tr/th[contains(.,'Total')]//th/span[@class='item_price']")
	public WebElement totalItemPrice;
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tfoot/tr/th[contains(.,'Total')]//th/span[@class='item_currency']")
	public WebElement totalItemcurrency;
	
	@FindBy(how = How.XPATH, using ="//button[@type='submit'][@class='order_submit_button']")
	public WebElement submitOrderButton;
	
	@FindBy(how = How.XPATH, using ="//button[@class='order_cancel_button']")
	public WebElement cancelOrderButton;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'order_complete')]/h1[contains(@class,'order_complete_header')]")
	public WebElement orderCompleteHeader;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'order_complete')]/div[contains(@class,'order_complete_invoice')]/span")
	public List<WebElement> orderCompleteInvoice;
	
	@FindBy(how = How.XPATH, using ="//button[@type='submit'][@class='order_finished_button']")
	public WebElement exitandReturntoNorton;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'order_complete')]/div[contains(@class,'order_complete_blurb')]")
	public WebElement orderCompleteblurb;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'headerDefault')]/h1/p[contains(.,'W. W. Norton Help')]")
	public WebElement helpText;
	
	@FindBy(how = How.XPATH, using ="//div[contains(@class,'headerDefault')]/h1/p[contains(.,'Returns')]")
	public WebElement returnText;
	
	@FindBy(how = How.XPATH, using ="//button[@type='button'][@id='createAccount']")
	public WebElement guestUserButton;
	
	@FindBy(how = How.ID, using ="cardNumber")
	public WebElement cardNumbertxtBox;
	
	@FindBy(how = How.ID, using ="cardExpiry")
	public WebElement cardexpiraytxtBox;
	
	@FindBy(how = How.ID, using ="cardCvv")
	public WebElement cardCvvtxtBox;
	
	@FindBy(how = How.ID, using ="firstName")
	public WebElement firstNametxtBox;
	
	@FindBy(how = How.ID, using ="lastName")
	public WebElement lastNametxtBox;
	
	@FindBy(how = How.ID, using ="billingLine1")
	public WebElement billingLine1Textbox;
		
	@FindBy(how = How.ID, using ="billingCity")
	public WebElement billingcityTextbox;
	
	@FindBy(how = How.ID, using ="billingState")
	public WebElement billingprovlistbox;
	
	@FindBy(how = How.ID, using ="billingPostalCode")
	public WebElement billingpostCodetxtBox;
	
	@FindBy(how = How.ID, using ="phoneType")
	public WebElement phoneTypeListBox;
	
	@FindBy(how = How.ID, using ="phone")
	public WebElement phonetxtBox;
	
	@FindBy(how = How.ID, using ="email")
	public WebElement emailtxtBox;		
	@FindBy(how = How.XPATH, using ="//input[@id='onboardOptionGuest']")
	public WebElement guestradioButton;	
		
	@FindBy(how = How.XPATH, using ="//button[@type='submit'][contains(.,'Continue')]")
	public WebElement continueButton;	
	@FindBy(how = How.XPATH, using ="//label[@for='guestAgreeToTerms']")
	public WebElement guestAgreeToTermschkBox;	
	
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr/td[contains(.,'free')]/following-sibling::td")
	public WebElement freePrice;
		
	@FindBy(how = How.XPATH, using ="//table[@class='order_table']/tbody/tr/td[not(contains(.,'free'))]/following-sibling::td")
	public List<WebElement> eachItemsPrice;
	
	@FindBy(how = How.XPATH, using ="//div[@class='buttons reviewButton']/input[@id='confirmButtonTop']")
	public WebElement continueConfirmButton;
	
	
	public Canadiandistributor() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	@Step("Enter Canadaian URL with appending last 3 digits as 000,  Method: {method} ")
	public void appendURL(String newURL) {
		PropertiesFile.setCanNewURL(newURL);
	}
	
	@Step("Fulfillment by Login Canada,  Method: {method} ")
	public String loginCanadatitle(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
		wait.until(ExpectedConditions.attributeContains(By.xpath("//div[@title='Fulfillment by Login Canada']"), "innerText", "Fulfillment by Login Canada"));
		return loginCanadaTitle.getAttribute("innerText");
	}
	
	@Step("Youve been redirected to Norton’s Canadian distributor, Login Canada.,  Method: {method} ")
	public String getCanadadisText(){
		return redirectText.getText();
	}
	
	@Step("click Back to Norton link.,  Method: {method} ")
	public void clickNortonlink(){
		backtoNortonDLP.click();
	}
	
	@Step("Verify Order Information Norton Account details.,  Method: {method} ")
	public ArrayList<String> getNortonAccountDetails(){
		String nameEmail;
		ArrayList<String> accountData = new ArrayList<String>();
		for(int i=1; i<nortonAccountdetails.size(); i=i+2) {
			nameEmail =nortonAccountdetails.get(i).getText();
			accountData.add(nameEmail);
		}
		return accountData;
	}
	
	@Step("Verify ORDER SUMMARY details.,  Method: {method} ")
	public ArrayList<String> getorderSummaryDetails(){
		String itemValue;
		ArrayList<String> accountData = new ArrayList<String>();
		for(int i=0; i<itemSummary.size(); i++) {
			itemValue =itemSummary.get(i).getText();
			accountData.add(itemValue);
		}
		return accountData;
	}
	
	@Step("Verify Item Total details.,  Method: {method} ")
	public ArrayList<String> itemTotal(){
		String itemValue;
		ArrayList<String> accountData = new ArrayList<String>();
		for(int i=0; i<itemtotalSummary.size(); i++) {
			itemValue =itemtotalSummary.get(i).getText();
			accountData.add(itemValue);
		}
		return accountData;
	}
	
	@Step("Click PayPal Button.,  Method: {method} ")
	public void clickPayPal(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(payPalButton));
		payPalButton.click();
	}
	
	@Step("Click Cancel and return to Login Canada (Test Store),  Method: {method} ")
	public void clickCancelLink(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(cancelLink));		
		ReusableMethods.clickElement(driver, cancelLink);
	}
	
	@Step("Enter PayPal User details and Click Next Button,  Method: {method} ")
	public void setPayPalUser(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.visibilityOf(emailMobileNumberTextbox));
		String paypalUserName = jsonobject.getAsJsonObject("PayPalUser").get("UserName").getAsString();
		String paypalPassword = jsonobject.getAsJsonObject("PayPalUser").get("Password").getAsString();
		emailMobileNumberTextbox.sendKeys(paypalUserName);
		NextButton.click();
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(paypalPassword);
		loginButton.click();
	}
	@Step("Verify Card Details on PayPal Card Details Page,  Method: {method} ")
	public String getCardinfo(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
		wait.until(ExpectedConditions.visibilityOf(payPalPaymentcardDetails));
		return payPalPaymentcardDetails.getText();
	}
	
	@Step("User Click the Continue Button from PayPal Card Details Page,  Method: {method} ")
	public void clickContinueButton() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.visibilityOf(paymentSubmitButton));
		boolean ispaymentSubmit = driver.findElements(By.xpath("//button[@id='payment-submit-btn']")).size() !=0;
		if(ispaymentSubmit==true) {
		try {
			paymentSubmitButton.click();
		} catch (StaleElementReferenceException e) {
			// TODO Auto-generated catch block
			boolean isconfirmContinue = driver.findElements(By.xpath("//div[@class='buttons reviewButton']/input[@id='confirmButtonTop']")).size() !=0;
			if(isconfirmContinue==true) {
				continueConfirmButton.click();
			}
		}
		}
		Thread.sleep(3000);
		
		
	}
	
	@Step("get the Billing info data,  Method: {method} ")
	public HashMap<String, String> getBillingInfo(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(submitOrderButton));
		//ArrayList<String> billinglist = new ArrayList<>();
		HashMap<String, String>  billinginfoMap = new HashMap<String, String>();
		int count =billingInfo.size();
		for(int i=1; i<=count; i=i+2){
			WebElement billingLabelEle = driver.findElement(By.xpath("//div[@class='info_h']/h2[contains(.,'BILLING INFO')]/following::div/table[@class='account_table']/tbody/tr["+i+"]/th"));
			String billingLabel =billingLabelEle.getText();
			WebElement billingValueEle =driver.findElement(By.xpath("//div[@class='info_h']/h2[contains(.,'BILLING INFO')]/following::div/table[@class='account_table']/tbody/tr["+(i+1)+"]/td"));
			String billingvalue =billingValueEle.getText();
			if(!billingvalue.equals(null)) {
			billinginfoMap.put(billingLabel,billingvalue);
			}
			
		}
		return billinginfoMap;
		
	}
	
	@Step("Verify Book Info on Order Table section,  Method: {method} ")
	public String getBookName(){
		return ItemName.getText();
	}
	
	@Step("Verify Item Price Order Table section,  Method: {method} ")
	public String getItemPrice(){
		return itemPricePayPal.getText();
	}
	
	@Step("Verify Free Item Price Order Table section,  Method: {method} ")
	public String getFreeItemPrice(){
		return freePrice.getAttribute("innerText");
	}
	
	@Step("get the each item price on order summary tabel,  Method: {method} ")
	public ArrayList<String> geteachItemPrice(){
		String totalprice;
		ArrayList<String> pricelist = new ArrayList<String>();
		for(int i=0; i<eachItemsPrice.size(); i++) {
			totalprice = eachItemsPrice.get(i).getAttribute("innerText");
			pricelist.add(totalprice);
		}
		return pricelist;
	}
	
	@Step("Verify Item GST Price Order Table section,  Method: {method} ")
	public String getGSTItemPrice(){
		return itemGSTPricePayPal.getText();
	}
	@Step("Verify Item Price Order Table section,  Method: {method} ")
	public String getItemCurrency(){
		return itemCurrencyPayPal.getText();
	}
	
	@Step("Click the Submit Order Button,  Method: {method} ")
	public void clickSubmitOrder(){
		submitOrderButton.click();
	}
	
	@Step("Order Completed Page is displayed,  Method: {method} ")
	public String getOrderCompleteText(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(orderCompleteHeader));
		return orderCompleteHeader.getText();
	}
	
	@Step("Order Complete Invoice Details,  Method: {method} ")
	public List<String> getOrderCompleteInvoice(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(orderCompleteHeader));
		ArrayList<String> orderInvoice = new ArrayList<>();
		for(WebElement ele : orderCompleteInvoice){
			orderInvoice.add(ele.getText());
		}
		return orderInvoice;
	}
	
	@Step("Order Complete Email Notification text,  Method: {method} ")
	public String getOrderCompleteEmailText(){
		return orderCompleteblurb.getText();
	}
	
	@Step("Click Exit and Return to Norton Button,  Method: {method} ")
	public void clickExitBacktoNortonButton(){
		ReusableMethods.scrollIntoView(driver, exitandReturntoNorton);
		exitandReturntoNorton.submit();
	}
	
	@Step("Click HelpDesk & Return Policy link from Order Complete Page,  Method: {method} ")
	public void clickLink(String linkName){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='norton_services']")));
		driver.findElement(By.linkText(linkName)).click();
	}
	//Website Page 
	@Step("Verify WW Norton Help Page is displayed,  Method: {method} ")
	public String helpPage(){	
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'headerDefault')]")));
		return helpText.getText();
	}
	
	
	@Step("Verify WW Norton Help Page is displayed,  Method: {method} ")
	public String returnPolicy(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'headerDefault')]")));
		return returnText.getText();
	}
	
	@Step("Click the Cancel Order Button,  Method: {method} ")
	public void clickCancelOrder(){
		ReusableMethods.scrollToBottom(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='order_cancel_button']")));
		ReusableMethods.clickElement(driver, cancelOrderButton);
	}
	
	@Step("Click the Pay with a credit or Visa Debit card Button,  Method: {method} ")
	public void clickCreateAccountGuestUser(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(guestUserButton));
		//ReusableMethods.clickElement(driver, guestUserButton);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector(\"#createAccount\").click()");
	}
	
	@Step("Enter a Billing Information for Guest user, Method: {method} ")
	public void setBillingInfoGuestUser(){
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(cardNumbertxtBox));
		ReusableMethods.clickElement(driver, cardNumbertxtBox);
		cardNumbertxtBox.sendKeys("4111111111111111");
		String[] getCurrentYear = ReusableMethods.getSysDate(); 
		int year =Integer.parseInt(getCurrentYear[1]);
		int updateYear = year+2;
		String newYear = String.valueOf(updateYear);
		cardexpiraytxtBox.sendKeys(getCurrentYear[0]+" "+newYear);
		cardCvvtxtBox.sendKeys("123");
		String fName=FakerNames.getStudentFName();
		String lName =FakerNames.getStudentLName();
		firstNametxtBox.sendKeys(fName);
		lastNametxtBox.sendKeys(lName);
		billingLine1Textbox.sendKeys("Zohar Gallery, 284 Forest Hill Rd");
		billingcityTextbox.sendKeys("Sherbrooke");
		Select proSel = new Select(billingprovlistbox);
		proSel.selectByValue("QC");
		billingpostCodetxtBox.sendKeys("J1H 1R6");
		//contact Info
		Select phoneType = new Select(phoneTypeListBox);
		phoneType.selectByValue("MOBILE");
		phonetxtBox.sendKeys("4378861038");
		emailtxtBox.sendKeys(fName+"-"+lName+"@maildrop.cc");
		ReusableMethods.scrollIntoViewByWindowHeight(driver, guestradioButton);
		ReusableMethods.clickElement(driver, guestradioButton);
		//Confirm Check box 
		ReusableMethods.scrollIntoView(driver, guestAgreeToTermschkBox);
		
		try {
			guestAgreeToTermschkBox.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		continueButton.click();
	}
}
