package com.norton.Ecommerce;

import io.qameta.allure.Step;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.BaseDriver;
import Utilities.PropertiesFile;
import Utilities.ReusableMethods;

public class NCIA {
	WebDriver driver;
	public RegisterPurchasePage RP;
	
	@FindBy(how = How.XPATH, using = "//button[@id='login_button']/span")
	public
	WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	public WebElement email;

	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	WebElement password;

	@FindBy(how = How.XPATH, using = "//*[@id=\"login_dialog_button\"]")
	public WebElement signInButton;

	@FindBy(how = How.XPATH, using = "//button[@id='login_dialog_button']/span[@class='ui-button-text'][contains(.,'Register, Purchase, or Sign Up for Trial Access')]")
	public WebElement RegisterPurchaseSignUpButton;

	@FindBy(how = How.XPATH, using = "//span[@id='gear_button_username']")
	public WebElement userName;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']/span[@id='gear_button_username']")
	public WebElement Signin_or_Register;

	@FindBy(how = How.CLASS_NAME, using = "ui-progressbar-overlay")
	public WebElement Overlay;

	@FindBy(how = How.ID, using = "activity_group_selectors")
	public WebElement agsElements;

	@FindBy(how = How.XPATH, using = "//button[@type='button']/span[contains(text(),'OK')]")
	public WebElement okButton;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")
	public WebElement eBookTile;

	@FindBy(how = How.XPATH, using = "//table[@id='product_info_table']/tbody/tr/td[@id='product_info_td']/h2[@id='product_title']")
	public WebElement productTitle;

	@FindBy(how = How.XPATH, using = "//table[@id='product_info_table']/tbody/tr/td[@id='product_info_td']/div[@id='product_authors_wrapper']/div[@id='product_authors']")
	public WebElement productAuthors;

	@FindBy(how = How.XPATH, using = "//button[@id='gear_button']//span[@id='gear_button_username']")
	public WebElement gear_button_username;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']/a/b[contains(.,'Sign Out')]")
	public WebElement SignOut_link;

	@FindBy(how = How.ID, using = "name")
	public WebElement FirstName_LastName;

	@FindBy(how = How.ID, using = "register_login_choice_register")
	public WebElement NoradioOption;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	public WebElement StudentEmail;

	@FindBy(how = How.XPATH, using = "//input[@type='password']")
	public WebElement Password;

	@FindBy(how = How.ID, using = "password2")
	public WebElement Password2;

	@FindBy(how = How.XPATH, using = "//div[@id='activity_group_selectors']/button[starts-with(@class,'activity_group_selector')]/div[contains(@class,'activity_group_selector_name')] | //span[@id='instructor_activity_group_extras']/button[@data-activity_group='getting_started']/div[contains(@class,'activity_group_selector_name')]")
	public List<WebElement> dlpTileList;

	@FindBy(how = How.XPATH, using = "//div[contains(@id,'return_to_resources_button')]")
	public WebElement digitalResourceBackButton;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'alert_dialog_outer_')]/p")
	public WebElement alertDialogTrialAccessmessage;
	
	@FindBy(how = How.XPATH, using = "//div[@id='activity_list_table_wrapper']/table/tbody/tr/td[@class=' title_td']/a[1]")
	public List<WebElement> assignmentlinks;

	// SS
	@FindBy(how = How.XPATH, using = "//td[@class=\" title_td\"]//a[contains(.,\"Tools of Science\")]")
	public WebElement Chapter;

	// SS
	@FindBy(how = How.XPATH, using = "//td[@class = ' title_td']//a[contains(., 'Science')]")
	public WebElement inqui_chapter;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@id='top_panel']//span[@class='icon icon-cogwheel']")
	public WebElement SettingsButton;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@class='item_group']/button[@id='printupsell_menu_item']")
	public WebElement PrintCopyLink;

	// SS
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonset']//span[contains(.,'Show')]")
	public WebElement ShowPrintPurchase;

	// SS
	@FindBy(how = How.XPATH, using = "//table[@class='login_dialog_purchase_tile']//td[@class='login_dialog_purchase_option_checkbox_td']")
	public WebElement PrintPurchaseCheckbox;

	// SS - Animation Chapter 1
	@FindBy(how = How.XPATH, using = "//li[@id='chemtours_ch_01']//div[@role='button']//div[@class='cn__title_plus_sub']//div[contains(.,'Chapter')]")
	public WebElement animation_chapter_link;

	// SS - Animation Chapter Chem Tour
	@FindBy(how = How.XPATH, using = "//a[@id=\"indexhtml_btn\"]//span[contains(.,\"Open ChemTour\")]")
	public WebElement animation_chapter_chem_tour;

	// SS - Animation Device Launch
	@FindBy(how = How.XPATH, using = "//button[@id=\"deviceLaunch\"]")
	public WebElement animation_device_launch;

	// SS
	@FindBy(how = How.XPATH, using = "//td[@class = \" title_td\"]//a[contains(.,\"Bio\")]")
	public WebElement ebook_psychsc6;

	//
	@FindBy(how = How.XPATH, using = "//div[@id='login_dialog_error_msg']")
	public WebElement get_inquis_err;

	// SS
	@FindBy(how = How.XPATH, using = "//button[@id = 'playPauseBtn']")
	public WebElement play_pause;
	
	//SS
	@FindBy(how = How.XPATH, using = "//div[@id='activity_group_selectors']//div[contains(.,'Animations')]")
	public WebElement animationTile;

	// Initializing Web Driver and PageFactory.
	public NCIA() {
		this.driver = BaseDriver.getDriver();
		this.RP = new RegisterPurchasePage();
		PageFactory.initElements(driver, this);
	}

	@Step("Click Sign Register Button,  Method: {method} ")
	public void clickSignIn() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		ReusableMethods.checkPageIsReady(driver);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(Signin_or_Register));
			Signin_or_Register.click();
		}

		catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username']")));

	}
	
	@Step("Verify Login Dialog is displayed ,  Method: {method} ")
	public boolean loginDialog() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.domPropertyToBe(signInButton, "id", "login_dialog_button"));
		boolean isDialog = driver.findElements(By.xpath("//button[@id='login_dialog_button']")).size() >0;
		if(isDialog==true) {
		email.isDisplayed();
		signInButton.isDisplayed();		
		}
		return isDialog;
	}
	@Step("Login to NCIA application,  Method: {method} ")
	public void loginNCIA(String userName, String Password) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		boolean isSigninRegister = driver.findElements(By.xpath("//button[@id='gear_button']/span[@id='gear_button_username']")).size() > 0;
		if(isSigninRegister==true) {
		clickSignIn();
		}
		email.clear();
		email.sendKeys(userName);
		// Assert.assertTrue(true,"The UserName is displayed as " +userName);
		// saveTextLog("The UserName is displayed as " ,userName);
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(Password);

		wait.until(ExpectedConditions.visibilityOf(signInButton));
		signInButton.click();
		wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.invisibilityOf(Overlay));

		try {
			boolean okButtonExist = okButton.isDisplayed();

			if (okButtonExist == true) {
				okButton.click();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}

	@Step("Click the eBook Tile,  Method: {method} ")
	public void clickEbookTile() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				"//div[contains(text(),'Ebook')]/preceding-sibling::div[@class='activity_group_selector_button activity_group_selector_button_width activity_group_selector_button_height']")));
		eBookTile.click();
	}

	@Step("Click the Tile based on the User Input,  Method: {method} ")
	public void clickTile(int index) throws InterruptedException {
		List<WebElement> dlpList = driver.findElements(
				By.xpath("//div[@class='activity_group_selector_name activity_group_selector_button_width']"));
		for (int i = 0; i < dlpList.size(); i++) {
			if (i == index) {
				dlpList.get(i).click();
			}
		}
	}
	
	@Step("Get the Tile Name from DLP Page,  Method: {method} ")
	public List<String> getTileName() throws InterruptedException {
		ArrayList<String> tileNameList = new ArrayList<>();
		String tileName=null;
		List<WebElement> dlpList = driver.findElements(
				By.xpath("//div[@class='activity_group_selector_name activity_group_selector_button_width']"));
		for (int i = 0; i < dlpList.size(); i++) {
			tileName =dlpList.get(i).getText();
			tileNameList.add(tileName);
		}
		return tileNameList;
	}
	
	@Step("Click the Chapter Links from Demo Page and when user is NOT Logged in ,  Method: {method} ")
	public void clickChapters(int link) throws InterruptedException {
	List<WebElement> chapterlinklist = driver.findElements(
				By.xpath("//table[contains(@class,'demo_activity_list_table')]/tbody/tr/td/a"));
		for (int i = 0; i < chapterlinklist.size(); i++) {
			if(i==link) {
				chapterlinklist.get(i).click();
			break;
			}
		}
		
	}

	@Step("Click the chapter Link ,Method: {method} ")
	public void clickeBookChapterTitle(String assignmentName) {
		List<WebElement> assignmentlinks = driver.findElements(
				By.xpath("//div[@id='activity_list_table_wrapper']/table/tbody/tr/td[@class=' title_td']/a[1]"));
		for (int i = 0; i < assignmentlinks.size(); i++) {
			String assignmentgetText = assignmentlinks.get(i).getText();
			if (assignmentgetText.equalsIgnoreCase(assignmentName)) {
				assignmentlinks.get(i).click();
				break;
			}
		}

	}

	@Step("Click the chapter Link based on the link value ,Method: {method} ")
	public String clickeBookChapterTitle(int linkValue) {
		String chapterTitle = null;
		for (int i = 0; i < assignmentlinks.size(); i++) {
			if (i == linkValue) {
				chapterTitle = assignmentlinks.get(i).getText();
				assignmentlinks.get(i).click();
				break;
			}
		}
		return chapterTitle;

	}

	@Step("get the Product Title from DLP page,Method: {method} ")
	public String getProductTitle() {
		String product_Title = productTitle.getText().trim();
		String[] gettitle = product_Title.split("\\r?\\n");
		return gettitle[0];
	}

	@Step("get the Product Editon from DLP page,Method: {method} ")
	public String getProductEditon() {
		String product_Edition = productTitle.getText().trim();
		String[] getEdition = product_Edition.split("\\r?\\n");
		boolean isSubTitle = driver.findElements(By.id("product_subtitle")).size() > 0;
		if (isSubTitle == true) {
			return getEdition[2];
		} else {
			return getEdition[1];
		}
	}

	@Step("get the Product Authors from DLP page,Method: {method} ")
	public String getProductAuthors() {
		String product_Authors = productAuthors.getText().trim();

		// remove data displayed in ();
		if (product_Authors.matches(".*\\(.+?\\)")) {
			product_Authors = product_Authors.replaceAll("\\(.+?\\)", "").trim();

		}
		return product_Authors;
	}

	@Step("get the Product Authors from DLP page,Method: {method} ")
	public List<String> getProductAuthors1() {
		ArrayList<String> pAuthors = new ArrayList<String>();
		String product_Authors = productAuthors.getText().trim();

		// remove data displayed in ();
		if (product_Authors.matches(".*\\(.+?\\)")) {
			product_Authors = product_Authors.replaceAll("\\(.+?\\)", "").trim();
			pAuthors.add(product_Authors);
		}

		return pAuthors;
	}

	@Step("SignOut the NCIA User,Method: {method} ")
	public void UserSignOut() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("gear_button")));
		boolean isUserName = driver.findElements(By.xpath("//button[@id='gear_button']")).size() > 0;
		if (isUserName == true) {
			ReusableMethods.clickElement(driver, gear_button_username);
			Thread.sleep(3000);
			SignOut_link.click();
		}

	}

	@Step("Click Register, Purchase, or Sign Up for Trial Access button, Method: {method} ")
	public void clickPurchaseSignUpRegisterButton() throws InterruptedException {
		boolean isChecked = driver
				.findElements(By.xpath("//input[@type='radio'][@id='register_login_choice_register'][@checked]"))
				.size() > 0;
		if (isChecked == false) {
			NoradioOption.click();
		}
		RegisterPurchaseSignUpButton.click();
	}

	@Step("Navigate to NCIA DLP application,  Method: {method} ")
	public void navigateNCIADLP(String dlpName) throws InterruptedException {
		if(dlpName.isEmpty()) {
		try {
			PropertiesFile.setURL("");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			PropertiesFile.setURL(dlpName);
		}
		
	}

	@Step("Verify User Name,  Method: {method} ")
	public String verify_Username() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='gear_button_username']")));
		String expectedUserName = userName.getText();
		return expectedUserName;

	}

	@Step("Verify Sign in Page,  Method: {method} ")
	public boolean isSigninPopUp() {
		return RegisterPurchaseSignUpButton.isDisplayed();
	}

	@Step("Click on the Tile {tileName},  Method: {method} ")
	public void clickDLPTile(String tileName) {
		String pcodeName;
		ReusableMethods.scrollIntoView(driver, agsElements);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[contains(@class,'activity_group_selector_name')]")));
		boolean isDLPTitle = driver.findElements(By.xpath("//div[contains(@class,'activity_group_selector_name')]"))
				.size() > 0;
		if (isDLPTitle == true) {

			for (WebElement prodName : dlpTileList) {
				pcodeName = prodName.getAttribute("innerText");
				if (pcodeName.startsWith(tileName)) {
					prodName.click();
					break;
				} else if (pcodeName.endsWith(tileName)) {
					prodName.click();
					break;
				} else if (tileName.contains(pcodeName)) {
					prodName.click();
					break;
				}
			}

		}
	}

	@Step("Click on the Digital resource Back Button,  Method: {method} ")
	public void clickdigitalResourceBackButton() {
		ReusableMethods.clickElement(driver, digitalResourceBackButton);
	}
	
	@Step("Verify Trail Access Pop Up message,  Method: {method} ")
	public String getTrialAccessPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(alertDialogTrialAccessmessage));
		return alertDialogTrialAccessmessage.getAttribute("innerText");
	}
	
	@Step("Verify Trail Access Pop Up message isNOT Displayed,  Method: {method} ")
	public boolean isTrialAccessPopUp() {		
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOf(alertDialogTrialAccessmessage));
		boolean isalertPopup= driver.findElements(By.xpath("//div[contains(@id,'alert_dialog_outer_')]/p")).size() >0;
		if(isalertPopup==true);		
		return alertDialogTrialAccessmessage.isDisplayed();
		
	}

	@Step("Verify DLP Tile {tileName} page is displayed,  Method: {method} ")
	public boolean isDLPTilesPage() {
		ReusableMethods.scrollIntoView(driver, agsElements);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[contains(@class,'activity_group_selector_name')]")));
		boolean isDLPTile = driver.findElements(By.xpath("//div[contains(@class,'activity_group_selector_name')]"))
				.size() > 0;
		return isDLPTile;
	}

	@Step("get the list of DLP Name from the NCIA page ,Method: {method} ")
	public List<String> getProductCodeNames() {
		ArrayList<String> nameList = new ArrayList<String>();
		String pcodeName;
		List<WebElement> productNamelist = driver.findElements(By.xpath(
				"//div[@id='activity_group_selectors']/button[starts-with(@class,'activity_group_selector')]/div[contains(@class,'activity_group_selector_name')] | //span[@id='instructor_activity_group_extras']/button[@data-activity_group='getting_started']/div[contains(@class,'activity_group_selector_name')]"));
		for (WebElement pcodeNameEle : productNamelist) {
			pcodeName = pcodeNameEle.getText();
			nameList.add(pcodeName);
		}
		return nameList;
	}

	// SS Methods
	@Step("Verify Upgrade to Full Access Button is displayed,  Method: {method} ")
	public boolean isUpGradeFullAccessbutton() throws InterruptedException {
		boolean isUpgradeButton = driver
				.findElements(By.xpath(
						"//button[@id='purchase_register_button'][contains(.,'Register a Code or Purchase Access')]"))
				.size() != 0;
		return isUpgradeButton;
	}

	// SS
	@Step("VerifyEbook is accessible if purchased-   Method: {method} ")
	public void verifyEbookAccessible() throws InterruptedException {
		Thread.sleep(2000);
		clickEbookTile();
		Thread.sleep(5000);
		Chapter.click();
		Thread.sleep(5000);
		ReusableMethods.switchToNewWindow(2, driver);
		driver.switchTo().frame("section_iframe");
		ReusableMethods.checkPageIsReady(driver);
		Thread.sleep(5000);
		boolean isebook_errortext = driver
				.findElements(By.xpath("//div[@class='alt_text_message']//div[@class='alt_text_p']")).size() > 0;
		if (isebook_errortext == true) {
			String error_message_ebook_chapter = RP.ebook_unaccessible_errormessage_new.getAttribute("innerText");
			String expected_ebook_errmsg = "This section of the e-book is only available to registered users.";
			Assert.assertTrue(error_message_ebook_chapter.contains(expected_ebook_errmsg));
		}
//			//ReusableMethods.switchToNewWindow(1, driver);
//			clickReturnArrow();
//			Thread.sleep(2000);
	}

	// SS
	@Step("VerifyEbook is accessible if purchased-   Method: {method} ")
	public void verifyInquisitiveAccessible() throws InterruptedException {
		clickDLPTile("InQuizitive");
		Thread.sleep(2000);
		inqui_chapter.click();
		Thread.sleep(2000);
		ReusableMethods.switchToNewWindow(2, driver);
		ReusableMethods.checkPageIsReady(driver);
		// boolean isebook_errortext =
		// driver.findElements(By.xpath("//div[@class='alt_text_message']//div[@class='alt_text_p']")).size()
		// != 0;
		boolean isebook_errortext1 = driver
				.findElements(By.xpath("//td[@class = ' title_td']//a[contains(., 'Science')]")).size() != 0;
		if (isebook_errortext1 == true) {
			String error_message_inqui_chapter = get_inquis_err.getAttribute("innerText");
			String expected_inqui_errormsg = "you are not yet entitled to access this activity.";
			Assert.assertTrue(error_message_inqui_chapter.contains(expected_inqui_errormsg));
		} else
			System.out.println("Hurray you are in Inquisitive Tile");
		ReusableMethods.switchToNewWindow(1, driver);
		clickdigitalResourceBackButton();
		Thread.sleep(2000);
	}

	// ss
	@Step("Click the Animation Tile,  Method: {method} ")
	public void clickAnimationTile() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[@id='activity_group_selectors']//div[contains(.,'Animations')]")));
		animationTile.click();
	}

	/*
	 * // ss
	 * 
	 * @Step("Return to DLP main page") public void clickReturnArrow() throws
	 * InterruptedException { WebDriverWait wait = new WebDriverWait(driver,
	 * Duration.ofSeconds(50));
	 * wait.until(ExpectedConditions.elementToBeClickable(By.
	 * xpath("//div[@id = 'return_to_resources_button']")));
	 * returntoresource.click(); }
	 */

	// SS
	@Step("Clicking on Setting button")
	public void clickSettingsButton() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(SettingsButton));
		SettingsButton.click();
	}

	// SS
	@Step("Clicking on PrintCopyLink button")
	public void clickPrintCopyLink() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(PrintCopyLink));
		PrintCopyLink.click();
	}

	// SS
	@Step("Clicking on Show Print Purchase button")
	public void clickShowPrintPurchase() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(ShowPrintPurchase));
		ShowPrintPurchase.click();
	}

	// SS
	@Step("Clicking on Print Purchase Checkbox")
	public void clickPrintPurchaseCheckbox() {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(50));
		wait.until(ExpectedConditions.elementToBeClickable(PrintPurchaseCheckbox));
		PrintPurchaseCheckbox.click();
	}
	
	//SS
		@Step("User is able to access Animation")
		public void accessNciaAnimation() throws InterruptedException{
		clickAnimationTile();
		Thread.sleep(5000);
		driver.switchTo().frame("cta_85466");
		Thread.sleep(2000);
		//ReusableMethods.scrollIntoView(driver, ncia.animation_chapter_link);
		animation_chapter_link.click();
		Thread.sleep(2000);
		animation_chapter_chem_tour.click();
		Thread.sleep(2000);
		ReusableMethods.switchToNewWindow(2, driver);
		Thread.sleep(2000);
		ReusableMethods.clickElement(driver, animation_device_launch);
		//animation_device_launch.click();
		Thread.sleep(5000);
		ReusableMethods.clickElement(driver, play_pause);
		//play_pause.click();
		Thread.sleep(5000);
		driver.close();
		ReusableMethods.switchToNewWindow(1, driver);
		Thread.sleep(2000);
		clickdigitalResourceBackButton();
		}

}
