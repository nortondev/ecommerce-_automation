package com.norton.Ecommerce;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import io.qameta.allure.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.BaseDriver;
import Utilities.PropertiesFile;

public class MailDrop {
	WebDriver driver;
	

	@FindBy(how = How.XPATH, using = "//input[@type='text'][@placeholder='view-this-inbox']")
	public WebElement viewInboxTextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='nav-container']//button//*[name()='svg']/following-sibling::span[contains(.,'View Inbox')]")
	public WebElement viewInboxButton;

	@FindBy(how = How.XPATH, using = "//div[@class='messagelist-container']//div[@class='messagelist-subject']/span")
	public List<WebElement> emailslist;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Reload')]")
	public WebElement reloadButton;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'fade-container')]//div[contains(@class,'messagelist-subject')]")
	public List<WebElement> emailsSubject;
	
	@FindBy(how = How.XPATH, using = "//html/body/p/span[starts-with(.,'Billing Address:')]/following-sibling::span")
	public List<WebElement> BillingAddress;
	
	@FindBy(how = How.XPATH, using = "//button[@class='close-button']")
	public WebElement closeButton;

	public MailDrop() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	// Navigate to PCAT URL

	@Step("Navigate to Mail Drop  application,  Method: {method} ")
	public void navigateMailDrop() {
		PropertiesFile.setmailDropURL();
	}

	@Step("Enter User name in Inbox and Click View Inbox button,  Method: {method} ")
	public void clickViewInboxbutton(String uName) {
		viewInboxTextbox.sendKeys(uName);
		viewInboxButton.click();
	}

	@Step("Click ReloadButton,  Method: {method} ")
	public void clickReloadButton() throws InterruptedException {	
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(60));
		wait.until(ExpectedConditions.elementToBeClickable(reloadButton));
		reloadButton.click();		
		
	}

	@Step("Verify Content on Email Body {0},  Method: {method} ")
	public String getOrderConfirmationContents(String content) {		
		WebElement contentElement = driver.findElement(By
				.xpath("//html/body/p/span[starts-with(.,'" + content + "')]"));
		return contentElement.getAttribute("innerText");
	}
	
	@Step("Get the Order # from the Email,  Method: {method} ")
	public String getOrderNumber(String content) {		
		WebElement contentElement = driver.findElement(By
				.xpath("//html/body/p//span[starts-with(.,'" + content + "')]"));
		return contentElement.getAttribute("innerText");
	}
	
	@Step("Verify Billing Address,  Method: {method} ")
	public String getBillingAddress() {	
		String billingAd;
		ArrayList<String> addList = new ArrayList<>();
		for(int b=0; b<BillingAddress.size(); b++) {
			billingAd = BillingAddress.get(b).getText();
			if(billingAd.startsWith("Please note:")) {
				break;
			}
			addList.add(billingAd);
		}
		return addList.toString();
	}
	@Step("Click Close Button from Email Section,  Method: {method} ")
	public void clickClose() {	
		closeButton.click();
		
	}
}
